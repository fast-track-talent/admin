<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Http\Traits\Interview\ContactTrait;
use App\Http\Traits\Interview\PersonalDataTrait;
use App\Http\Traits\Interview\SalaryExpectationTrait;
use App\Http\Traits\Interview\StudiesTrait;
use App\Http\Traits\Interview\SkillsTrait;
use App\Http\Traits\Interview\WorkExperienceTrait;
use App\Http\Traits\Interview\ExtraDataTrait;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Application;
use App\Models\CountryState;
use App\Models\Country;
use App\Models\Language;
use App\Models\LanguageLevel;
use App\Models\SalaryExpectation;
use App\Models\PositionRequest;
use App\Models\CompanyActivity;
use App\Models\Degree;
use App\Models\Client;
use App\Models\Position;
use App\Models\StudyType;
use App\Models\Skill;
use Hashids\Hashids;
use App\Models\Person;
use App\Models\TalentComment;
use App\Models\ExperienceRange;
use App\Models\AgeRange;
use App\Models\Nomination;
use Illuminate\Support\Arr;

// RESOURCES
use App\Http\Resources\ApplicationsIndexResource;
use App\Http\Resources\ApplicationsListResource;
use App\Http\Resources\SalaryExpectationsResource;
use App\Http\Resources\SkillsResource;
use App\Http\Resources\PersonDataResource;
use App\Http\Resources\Interview\CountriesResource;
use App\Http\Resources\Interview\StatesResource;
use App\Http\Resources\Interview\LanguagesResource;
use App\Http\Resources\Interview\LanguageLevelsResource;
use App\Http\Resources\Interview\StudyTypesResource;
use App\Http\Resources\Interview\StepFourResource;
use App\Http\Resources\Interview\StepFiveResource;
use App\Http\Resources\Interview\UserLanguagesResource;
use App\Http\Resources\Profiles\CompanyActivitiesResource;
use App\Http\Resources\Profiles\PositionsResource;
use App\Http\Resources\Profiles\DegreesResource;

class ApplicationController extends Controller
{
    use ContactTrait,
        PersonalDataTrait,
        SalaryExpectationTrait,
        StudiesTrait,
        WorkExperienceTrait,
        SkillsTrait,
        ExtraDataTrait;

    public function index(Request $request) {
        try {
            $companyActivities = CompanyActivity::orderBy('activity')->get();
            $positions = Position::orderBy('position')->get();
            $salary_expectations = SalaryExpectation::all();
            $countries = Country::orderBy('name')->get();
            $studyTypes = StudyType::orderBy('name')->get();
            $degrees = Degree::orderBy('degree')->get();
            $languages = Language::orderBy('lang')->get();
            $languageLevels = LanguageLevel::orderBy('level')->get();
            $skills = Skill::orderBy('skill')->get();
            $experienceRanges = ExperienceRange::all();
            $ageRanges = AgeRange::all();
            $applications = Application::with('talent', 'talent.personData')->get();
            $arr = [];
            $userIdHash = new Hashids('assistant-user', 20);
            foreach($applications as $app){
                $aux = (object)[];
                $aux->id = $userIdHash->encode($app->talent->id);
                $aux->firstnames = null;
                $aux->lastnames = null;
                $aux->gender = null;
                if($app->talent->personData){
                    $aux->firstnames = $app->talent->personData->firstnames ;
                    $aux->lastnames = $app->talent->personData->lastnames ;
                    $aux->gender = $app->talent->personData->gender;
                }
                if($app->talent){
                    $aux->email = $app->talent->email;
                }
                $aux->reputation = $app->reputation;
                $aux->finished = $app->finished;
                $aux->curriculum = $app->curriculum;
                $arr[] = $aux;
            }
            
            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];

            return Inertia::render('application/', [
                'sessionUser' => $sessionUser,
                'companyActivities' => $companyActivities,
                'positions' => $positions,
                'salaryExpectations' => $salary_expectations,
                'countries' => $countries,
                'studyTypes' => $studyTypes,
                'degrees' => $degrees,
                'languages' => $languages,
                'languageLevels' => $languageLevels,
                'skills' => $skills,
                'experienceRanges' => $experienceRanges,
                'ageRanges' => $ageRanges,
                'applications' => $arr
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function getStates($code)
    {
        try {
            $hashids = new Hashids('countries-talents-assistant', 20);
            $decoded = $hashids->decode($code);
            if (count($decoded) === 0) {
                return response()->json([
                    'validCountry' => false
                ], 200);
            }
            $states = CountryState::where('country', $decoded[0])->orderBy('name')->get();
            return response()->json([
                "states" => StatesResource::collection($states)
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }


    public function getApplications(Request $request) {
        try {
            $filters = $request->filters;
            $query = DB::table('users');
            $query->join('people', 'users.id', '=', 'people.user_id');
            $query->join('applications', 'applications.user_id', '=', 'users.id');

            // Position, COmpany Activity or Experience range filters.
            if (
                array_key_exists('position', $filters) ||
                array_key_exists('companyActivity', $filters) ||
                array_key_exists('expRange', $filters)
            ) {
                $query->join('experiences', 'experiences.user_id', '=', 'users.id');
            }

            // Studies filter (Degrees).
            if (array_key_exists('studiesFilters', $filters)) $query->join('user_studies', 'user_studies.user_id', '=', 'users.id');
            
            // Languages filter (Spoken languages).
            if (array_key_exists('languagesFilters', $filters)) $query->join('user_langs', 'user_langs.user_id', '=', 'users.id');
            
            // Skills filter.
            if (array_key_exists('chosenSkills', $filters)) $query->join('user_skills', 'user_skills.user_id', '=', 'users.id');

            // Fields to return.
            $query->select(
                'users.id',
                'people.firstnames',
                'people.lastnames',
                'people.gender',
                'users.email',
                'applications.reputation',
                'applications.finished',
                'applications.curriculum'
            );

            // Where clauses with filters.
            $query->where(function ($query) use ($request, $filters) {

                // Get only talent user type.
                $query->where('users.user_type_id', 3);

                // Gender filter query.
                if (array_key_exists('gender', $filters)) {
                    if ($filters['gender'] === 'NULL') {
                        $query->whereNull('people.gender');
                    } else {
                        $query->where('people.gender', $filters['gender']);
                    }
                }

                // Company Activity filter query.
                if (array_key_exists('companyActivity', $filters)) $query->where('experiences.company_activity_id', $filters['companyActivity']);

                // Position filter query.
                if (array_key_exists('position', $filters)) $query->where('experiences.position', $filters['position']);

                // Salary Expectation filter query.
                if (array_key_exists('salaryExpectation', $filters)) $query->where('applications.salary_expect_id', $filters['salaryExpectation']);

                // Country filter query.
                if (array_key_exists('country', $filters) && !array_key_exists('state', $filters)) $query->where('people.country', $filters['country']);

                // Country & State filter query.
                if (array_key_exists('country', $filters) && array_key_exists('state', $filters)) $query->where([
                    ['people.country', $filters['country']],
                    ['people.state_id', $filters['state']]
                ]);

                // Study types filter query.
                if (array_key_exists('studiesFilters', $filters)) {
                    $studyTypes = [];
                    $degrees = [];
                    for ($i = 0; $i < count($filters['studiesFilters']); $i++) { 
                        $st = $filters['studiesFilters'][$i]['studyType'];
                        $d = $filters['studiesFilters'][$i]['degree'];
                        if ($st !== null) array_push($studyTypes, $st);
                        if ($d !== null) array_push($degrees, $d);
                    }
                    if (count($studyTypes) > 0) $query->whereIn('user_studies.study_type_id', $studyTypes);
                    if (count($degrees) > 0) $query->whereIn('user_studies.degree_id', $degrees);
                }

                // Languages filter query.
                if (array_key_exists('languagesFilters', $filters)) {
                    for ($i = 0; $i < count($filters['languagesFilters']); $i++) {
                        if ($i === 0) {
                            $query->where([
                                ['user_langs.lang_id', $filters['languagesFilters'][$i]['langId']],
                                ['user_langs.level_id', $filters['languagesFilters'][$i]['levelId']],
                            ]);
                        } else {
                            $query->orWhere([
                                ['user_langs.lang_id', $filters['languagesFilters'][$i]['langId']],
                                ['user_langs.level_id', $filters['languagesFilters'][$i]['levelId']],
                            ]);
                        }
                    }
                }

                if (array_key_exists('chosenSkills', $filters)) {
                    $skills = [];
                    for ($i = 0; $i < count($filters['chosenSkills']); $i++) { 
                        $cs = $filters['chosenSkills'][$i];
                        if ($cs !== null) array_push($skills, $cs);
                    }
                    if (count($skills) > 0) $query->whereIn('user_skills.skill_id', $skills);
                }

                // Age Range query.
                if (array_key_exists('ageRange', $filters)) {
                    $ageFilters = $filters['ageRange'];
                    if ($ageFilters['id'] === 6) {
                        $minAge = $ageFilters['min'];
                        $minDate = Carbon::today()->subYears($minAge);
                        $query->whereDate('people.birthdate', '<=', $minDate);
                    } else {
                        $minAge = $ageFilters['min'];
                        $maxAge = $ageFilters['max'];
                        $minDate = Carbon::today()->subYears($maxAge+1); // make sure to use Carbon\Carbon in the class
                        $maxDate = Carbon::today()->subYears($minAge)->endOfDay();
                        $query->whereBetween('people.birthdate', [$minDate, $maxDate]);
                    }
                }

                // Experience Range in position filter query.
                if (array_key_exists('expRange', $filters)) {
                    $expFilters = $filters['expRange'];
                    if ($expFilters['id'] === 1) {
                        $max = $expFilters['max'];
                        $query->whereRaw("IF(`experiences`.`to` IS NULL, TIMESTAMPDIFF(year, `experiences`.`from` , NOW()), TIMESTAMPDIFF(year, `experiences`.`from`, `experiences`.`to`)) <= $max");
                    } else if ($expFilters['id'] === 4) {
                        $min = $expFilters['min'];
                        $query->whereRaw("IF(`experiences`.`to` IS NULL, TIMESTAMPDIFF(year, `experiences`.`from` , NOW()), TIMESTAMPDIFF(year, `experiences`.`from`, `experiences`.`to`)) >= $min");
                    } else {
                        $max = $expFilters['max'];
                        $min = $expFilters['min'];
                        $query->whereRaw("IF(`experiences`.`to` IS NULL, TIMESTAMPDIFF(year, `experiences`.`from` , NOW()), TIMESTAMPDIFF(year, `experiences`.`from`, `experiences`.`to`)) BETWEEN $min AND $max");
                    }
                }
            });
            $query->orderBy('users.id','asc');
            $query->groupBy(
                'users.id',
                'people.firstnames',
                'people.lastnames',
                'people.birthdate',
                'people.gender',
                'users.email',
                'applications.reputation',
                'applications.finished',
                'applications.curriculum',
            );
            $result =  $query->get();
            $userIdHash = new Hashids('assistant-user', 20);
            foreach($result as $res){
                $res->id = $userIdHash->encode($res->id);
            }

            return response()->json([
                'data' => $result
            ]);
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

    public function showDetail(Request $request, $id) {
        try {

            $hashids = new Hashids('assistant-user', 20);
            //$idUser = $id;
            $idUser = $hashids->decode($id);
            $userDetails = User::with(
                'country',
                'personData', 'personData.countryInfo', 'personData.state',
                'application',
                'application.salary_expectation',
                'application.comments',
                'application.comments.user',
                'application.comments.user.personData',
                'studies', 'studies.degree', 'studies.studyType', 'studies.country',
                'languages', 'languages.language', 'languages.level',
                'skills',
                'experiences',
                'nominations',
                'nominations.client',
                'nominations.position',
                'nominations.salaryExpectation',
            )->find($idUser[0]);
            $aux = $userDetails->replicate();
            unset($aux->id);
            $aux->user_id = (string) $id;

            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];
            $result = [
                'sessionUser' => $sessionUser,
                'talent' => $aux,
            ];

            return Inertia::render('application/detail',$result);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function addTalent(Request $request, $id=null) {
        try {
            $userDetails = null;
            $aux = (object)[];
            $states = [];
            $result = null;

            if($id){
                $userIdHash = new Hashids('assistant-user', 20);
                $id = $userIdHash->decode($id);
                $userDetails = User::with(
                    'country',
                    'personData', 'personData.countryInfo', 'personData.state',
                    'application',
                    'application.salary_expectation',
                    'application.comments',
                    'application.comments.user',
                    'application.comments.user.personData',
                    'studies', 'studies.degree', 'studies.studyType', 'studies.country',
                    'languages', 'languages.language', 'languages.level',
                    'experiences',
                    'skills',
                    'nominations',
                    'nominations.client',
                    'nominations.position',
                    'nominations.salaryExpectation',
                )->find($id[0]);
                $aux->email = $userDetails->email;
                $aux->phone_number = $userDetails->phone_number;
                $aux->country = new CountriesResource($userDetails->country);
                $personDataIdHash = new Hashids('assistant-person', 20);
                $stateHash = new Hashids('states-in-client', 20);

                $aux->id = $userIdHash->encode($userDetails->id);
                if($userDetails->personData){
                    $aux->personData = new PersonDataResource($userDetails->personData);
                    //$aux->personData = $userDetails->personData;
                }

                if($aux->personData->country){
                    $states = CountryState::where('country', $aux->personData->country)->orderBy('name')->get();
                    $states = StatesResource::collection($states);
                }

                if($userDetails->application){
                    $aux->application = $userDetails->application;
                    //$aux->application->salary_expectation = new SalaryExpectationsResource($userDetails->salary_expectation);
                    $salaryExpectation = new SalaryExpectationsResource($userDetails->application->salary_expectation);
                    unset($aux->application->salary_expectation);
                    unset($aux->application->salary_expectation_id);
                    $aux->application->salary_expectation = $salaryExpectation;
                }

                if($userDetails->studies){
                    // $aux->studies = $userDetails->studies;
                    // $countriesHash = new Hashids('countries-talents-assistant', 20);
                    // $studiesHash = new Hashids('step-four-data-in-client', 20);
                    // foreach($aux->studies as $study){
                    //     $study->id = $studiesHash->encode($study->id);
                    //     $study->degree->id = $studiesHash->encode($study->degree->id);
                    //     $study->studyType->id = $studiesHash->encode($study->studyType->id);
                    //     $study->country->id = $countriesHash->encode($study->country->id);
                    // }
                    $aux->studies = StepFourResource::collection($userDetails->studies);
                }

                if($userDetails->languages){
                    //$aux->languages = $userDetails->languages;
                    // $studiesHash = new Hashids('step-four-data-in-client', 20);
                    // foreach($aux->languages as $language){
                    //     $language->id = $studiesHash->encode($language->id);
                    //     $language->language->id = $studiesHash->encode($language->language->id);
                    //     $language->level->id = $studiesHash->encode($language->level->id);
                    // }
                    $aux->languages = UserLanguagesResource::collection($userDetails->languages);
                }

                if($userDetails->experiences){
                    // $aux->experiences = $userDetails->experiences;
                    // $workHash = new Hashids('step-five-data-in-client', 20);
                    // foreach($aux->experiences as $experience){
                    //     $experience->id = $workHash->encode($experience->id);
                    // }
                    $aux->experiences = StepFiveResource::collection($userDetails->experiences);
                }

                if($userDetails->skills){
                    $aux->skills = SkillsResource::collection($userDetails->skills);
                }

                if($userDetails->nominations){
                    $nominations = $userDetails->nominations;
                    foreach($nominations as $nom){
                        $client = $nom->client->replicate();
                        $clientHash = new Hashids('assistant-company', 20);
                        $clientId = $clientHash->encode($client->id);
                        unset($client->id);
                        $client->id = $clientId;
                        unset($nom->client);
                        $nom->client = $client;

                        $position = $nom->position->replicate();
                        $positionHash = new Hashids('positions-in-client', 20);
                        $positionId = $positionHash->encode($position->id);
                        unset($position->id);
                        $position->id = $positionId;
                        $nom->position = $position;

                        $salaryExpectation = $nom->salaryExpectation->replicate();
                        $salaryHash = new Hashids('salaries-exp-in-client', 20);
                        $salaryId = $salaryHash->encode($salaryExpectation->id);
                        unset($salaryExpectation->id);
                        $salaryExpectation->id = $salaryId;
                        $nom->salaryExpectation = $salaryExpectation;
                    }
                }
            }
            
            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];
            
            $companyActivities = CompanyActivitiesResource::collection(CompanyActivity::orderBy('activity')->get());
            $positions = PositionsResource::collection(Position::orderBy('position')->get());
            $salaryExpectations = SalaryExpectationsResource::collection(SalaryExpectation::all());
            $countries = CountriesResource::collection(Country::orderBy('name')->get());
            $studyTypes = StudyTypesResource::collection(StudyType::orderBy('name')->get());
            $degrees = DegreesResource::collection(Degree::orderBy('degree')->get());
            $languages = LanguagesResource::collection(Language::orderBy('lang')->get());
            $languageLevels = LanguageLevelsResource::collection(LanguageLevel::orderBy('level')->get());
            $skills = SkillsResource::collection(Skill::orderBy('skill')->get());

            $result = [
                'sessionUser' => $sessionUser,
                'talent' => $aux,
                'countries' => $countries,
                'salaryExpectations' => $salaryExpectations,
                'studyTypes' => $studyTypes,
                'degrees' => $degrees,
                'languages' => $languages,
                'languageLevels' => $languageLevels,
                'skills' => $skills,
                'companyActivities' => $companyActivities,
                'positions' => $positions,
            ];
            if($states){
                $result['states'] = $states;
            }
            return Inertia::render('application/edit',$result);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function updateReputation(Request $request, $id) {
        try {
            $app = Application::find($id);
            $app->reputation = $request->value;
            $app->save();
            return response()->json([
                'success' => true
            ], 200);
            return response()->json($app);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function searchClients($value) {
        try {
            $param = "%{$value}%";
            $clients = Client::where('name', 'like', $param)->get();

            return response()->json([
                'success' => true,
                'clients' => $clients
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function searchPositionRequests($id) {
        try {
            $posReqs = PositionRequest::with(['position', 'salaryExpectation'])->where([
                ['client_id', $id],
                ['finished', 1],
            ])->get();

            return response()->json([
                'success' => true,
                'positionRequests' => $posReqs
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function nominateTalent(Request $request) {
        try {
            DB::beginTransaction();
            $userId = $request->userId;
            $posReqIds = $request->posReqIds;
            for ($i = 0; $i < count($posReqIds); $i++) { 
                Nomination::create([
                    'position_request_id' => $posReqIds[$i],
                    'user_id' => $userId,
                ]);
            }
            DB::commit();
    
            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function saveComment(Request $request) {
        try {
            DB::beginTransaction();
            if ($request->commentId) {
                $comment = TalentComment::find($request->commentId);
            } else {
                $comment = new TalentComment;
            }
            $comment->user_id = $request->userId;
            $comment->application_id = $request->applicationId;
            $comment->comment = $request->comment;
            $comment->save();

            DB::commit();
    
            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function deleteComment($id) {
        try {
            DB::beginTransaction();
            $comment = TalentComment::find($id);
            $comment->delete();
            DB::commit();
    
            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }


    // OLD METHODS

    public function storeTalent(Request $request)
    {
        try {
            $obj = $request->data;
            $obj = json_decode (json_encode ($request->data), FALSE);
            $array = (array)json_decode($request->data);
            $object = (object)[];
            foreach ($array as $key => $value) {
                $object->{$key} = $value;
            }
            $obj = $object->data;
            $contactForm = $obj->contact;
            $user_id = null;
            $stepContactResult = null;
            if($contactForm ){
                $stepContactResult = (!$obj->user_id) ? $this->registerUserTrait($contactForm) : $this->updateUserTrait($contactForm, $obj->user_id);
            }

            $user_id = ($obj->user_id) ? $obj->user_id : $stepContactResult['interviewContact']['id'];
            $stepPersonalDataResult = null;
            $personalDataForm = (object) $obj->personData;
            $personalDataForm->userId = $user_id;
            
            if($personalDataForm){
                $personalDataForm->countryCode = $personalDataForm->country;
                $stepPersonalDataResult = (!$obj->person_id) ? $this->registerPersonDataTrait($personalDataForm) : $this->updatePersonDataTrait($personalDataForm, $obj->person_id);
            }
            
            $stepSalaryExpectationResult = null;
            $salaryExpectationForm = (object) $obj->salaryExpectation;
            if($salaryExpectationForm){
                $stepSalaryExpectationResult = $this->saveApplicationSalaryTrait($salaryExpectationForm, $user_id);
            }

            $stepStudiesResult = null;
            $studiesForm = (object) $obj->studies;
            if(count($studiesForm->studies) > 0){
                $languages = (array) $studiesForm->languages;
                $studies = (array) $studiesForm->studies;
                $arr = [];
                foreach($languages as $lang){
                    $aux = (array) $lang;
                    $aux['levelId'] = $aux['level'];
                    $arr[]= $aux;
                }
                $studiesForm->languages = $arr;
                $arr= [];
                foreach($studies as $study){
                    $aux = (array) $study;
                    $arr[] = $aux;
                }
                $studiesForm->studies = $arr;
                $studiesForm->userId = $user_id;
                $stepStudiesResult = $this->saveStudiesTrait($studiesForm);
            }

            $stepWorkExperienceResult = null;
            $workExperienceForm = (object) $obj->workExperience;
            if(count($workExperienceForm->experiences) > 0){
                $experiences = $workExperienceForm->experiences;
                $arr= [];
                foreach($experiences as $experience){
                    $aux = (array)$experience;
                    $aux['from'] = $aux['from']->value;
                    $aux['to'] = $aux['to']->value;
                    $arr[] = $aux;
                }
                $workExperienceForm->experiences = $arr;
                $workExperienceForm->userId = $user_id;
                $stepWorkExperienceResult = $this->syncExperiencesTrait($workExperienceForm);
            }

            $stepSkillsResult = null;
            $skillsForm = (object) $obj->skills;
            if(count($skillsForm->skillsToAdd) > 0 || count($skillsForm->skillsToDelete)){

                $skillsForm->userId = $user_id;                
                $stepSkillsResult = $this->saveUserSkillsTrait($skillsForm);
            }

            $stepExtraDataResult = null;
            $extraDataForm = (object) $obj->extraData;
            $extraDataForm->file  = $request->file('file');
            if($extraDataForm){
                $stepExtraDataResult = $this->syncExtrasTrait($extraDataForm, $user_id);
            }

            $hashids = new Hashids('assistant-user', 20);
            $usr_ = $hashids->decode($user_id);
            $appId = User::find($usr_[0])->application->id;
            $app = Application::find($appId);
            $app->finished = 1;
            $app->save();
            
            $result = [
                "userId" => $user_id,
                "contactResult" => $stepContactResult,
                "personalDataResult" => $stepPersonalDataResult,
                "salaryExpectationResult" => $stepSalaryExpectationResult,
                "studiesResult" => $stepStudiesResult,
                "workExperienceResult" => $stepWorkExperienceResult,
                "skillsResult" => $stepSkillsResult,
                'extraDataResult' => $stepExtraDataResult
            ];
            return response()->json($result,200);
        }catch (\Throwable $th) {
            throw $th;
        }

    }

    public function validEmail(Request $request) {
        try {
            if($request->id){
                
                $hashids = new Hashids('assistant-user', 20);
                //pregunto si existe un user con este hash
                $userId = $hashids->decode($request->id);
                if (!$userId = $hashids->decode($request->id)) throw new ModelNotFoundException;

                if(User::where('id', $userId)->where('email', $request->email)->exists()){
                    return response()->json(true, 200);
                }
                
            }
            
            // verificando el email
            $user = User::where('email', $request->email)->exists();
            if($user){
                return response()->json(false,200);
            } else {
                return response()->json(true, 200);
            }

        } catch (\Throwable $th) {
            return response()->json(false,200);
        }
       
    }
    public function saveExtras(Request $req)
    {
        $stepSixResult = $this->syncExtrasTrait($req, $req->user_id);

        return redirect()->route('application');
    }

    public function upload(Request $req)
    {
        $userIdHash = new Hashids('assistant-user', 20);
        $decodedUserId = $userIdHash->decode($req->user_id);
        $user = User::find($decodedUserId[0]);

        if ($req->hasFile('file')) {
            $file = $req->file('file');
            $ext = $file->extension();
            $path = '/storage/users/'.$decodedUserId[0];
            if ($file->move(public_path($path), "avatar.$ext")) {

                // $application = Application::find($user->application->id);
                $user->avatar = "$path/avatar.$ext";
                $user->save();

               return response()->json($req->user_id,200);

            }
        }

        return response()->json($req->user_id,200);
    }

    // public function form(Request $request)
    // {
    //     return Inertia::render('application/store');
    // }

    public function deleteApplication($id)
    {
      $app = Application::find($id);
      if ($app != null) {
        $app->delete();

        return redirect('/application', 303);;
       }
    }
  
    public function getAll()
    {
        $app = Application::has('personData')->with('personData')->get();

        return response()->json($app);
    }

    public function getDownload($id)
    {   
        try {
            $userIdHash = new Hashids('assistant-user', 20);
            $decodedUserId = $userIdHash->decode($id);
            $user = User::with('application')->find($decodedUserId[0]);
            if($file = $user->application->curriculum){
                $name= $user->personData->firstnames.'_'.$user->personData->lastnames.'_cv';
                if(str_contains($file, '.pdf')){
                    return response()->file($file);
                }
                return response()->download($file);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'este usuario no tiene curriculum'
                ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
