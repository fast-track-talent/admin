<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Client;
use Inertia\Inertia;
use Hashids\Hashids;
use App\Models\User;
use App\Http\Traits\Profiles\IdentificationTrait;
use App\Http\Traits\Profiles\ContactTrait;
use App\Http\Traits\Profiles\IndustrySectorTrait;
use App\Http\Traits\Profiles\ProductsTrait;    
use App\Http\Resources\Interview\CountriesResource;
use App\Http\Resources\Profiles\CompanyActivitiesResource;
use App\Http\Resources\Profiles\CompanyActivitiesWithPivotIdResource;
use App\Http\Resources\ClientListingResource;
use App\Http\Resources\ClientResource;
// use App\Http\Resources\ProductsResource;
use App\Models\CompanyActivity;
use App\Models\Country;
use App\Models\ClientComment;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{

    use IdentificationTrait;
    use ContactTrait;
    use IndustrySectorTrait;
    use ProductsTrait;   

    public function index(Request $request)
    {   
        try {
            $clients =  Client::with('contact', 'contact.country')->orderBy('name')->get();
            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];
            return Inertia::render('client/index', [
                'sessionUser' => $sessionUser,
                'clients' => ClientListingResource::collection($clients),    
            ]);
        }catch (\Throwable $th) {
            throw $th;
        }
    }

    public function showDetail(Request $request, $id) {
        try {
            $hashids = new Hashids('assistant-company', 20);
            $decodedId = $hashids->decode($id);
            $client = client::with([
                'contact',
                'contact.country',
                'contact.personData', 
                'positionRequests',
                'activities',
                'products',
                'comments',
                'comments.user',
                'comments.user.personData'
            ])->find($decodedId[0]);
            
            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];
            return Inertia::render('client/details',[
                'sessionUser' => $sessionUser,
                'client' => $client,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function saveComment(Request $request) {
        try {
            DB::beginTransaction();
            if ($request->commentId) {
                $clientComment = ClientComment::find($request->commentId);
            } else {
                $clientComment = new ClientComment;
            }
            $clientComment->user_id = $request->userId;
            $clientComment->client_id = $request->clientId;
            $clientComment->comment = $request->comment;
            $clientComment->save();

            DB::commit();
    
            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function deleteComment($id) {
        try {
            DB::beginTransaction();
            $clientComment = ClientComment::find($id);
            $clientComment->delete();
            DB::commit();
    
            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }



    public function getAll(Request $request)
    {
      $data = Client::all();
      return response()->json($data);
    }

    public function get(Request $request)
    {

        $data = Client::with('activities', 'applications', 'contact')
        ->where(function ($query) use ($request) {

            if($request->activity)
            {
                $hashids = new Hashids('step-five-data-in-client', 20);
                $hashiActivity = $hashids->decode($request->activity);
                $query->where('activity_company', $hashiActivity);
            }
            if($request->search)
            {
                $query->where("name", "LIKE", "%{$request->search}%");
            }
        });

        if($request->status == 1)
        {
            $data = $data->has('applications');
        }
        
        if($request->status == 2)
        {
            $data = $data->doesntHave('applications');
        }


        if ($request->perPage > 0)
        {
            $data = $data->paginate($request->perPage);
        }
        else
        {
            $data = $data->get();
        }
        return response()->json($data);
    }

    public function addClient($id=null, Request $request){
        try {
            $countries = CountriesResource::collection(Country::all());
            $companyActivities = CompanyActivitiesResource::collection(CompanyActivity::all());
            $client = null;
            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];

            if($id){
                $hashids = new Hashids('assistant-company', 20);
                $decodedId = $hashids->decode($id);
                $client = new ClientResource(Client::with('contact',  'contact.personData', 'contact.country', 'activities', 'products')->find($decodedId[0]));
                
            }

            return Inertia::render('client/store', [
                'sessionUser' => $sessionUser,
                'countries'=> $countries,
                'activities' => $companyActivities,
                'client' => $client        
            ]);

        } catch(\Throwable $th){
            throw $th;
        }
    }



    public function newClient(){
        try {
            $countries = CountriesResource::collection(Country::all());
            $companyActivities = CompanyActivitiesResource::collection(CompanyActivity::all());
            return Inertia::render('client/new', [
                'sessionUser' => $sessionUser,
                'countries'=> $countries,
                'companyActivities' => $companyActivities        
            ]);

        } catch(\Throwable $th){
            throw $th;
        }
    }

    public function getClientInfo($id){
        try {
            $client = Client::find($id)->with('contact', 'contact.country', 'activities', 'products');
            return response()->json(['clients' => $clients, 'client' => $result]);

        } catch(\Throwable $th){
            throw $th;
        }
    }

    
    public function storeClient(Request $request)
    {
        try{
            $result = [];
            if($request->addClient){
                //paso cliente
                if($request->addClient['name'] &&  
                $request->addClient['numberId'] && 
                $request->addClient['description']){
                    $clientOne = new Request([
                        'name'   => $request->addClient['name'],
                        'numberId' => $request->addClient['numberId'],
                        'description' =>$request->addClient['description']
                    ]);
                    $result['stepOne'] = $this->saveClientTrait($clientOne);
                } else {
                    return response()->json([
                        'error' => true,
                        'message' => 'Debes llenar todos campos del cliente.'
                    ]);
                }


                //paso contacto 
                if( $result['stepOne']['identification']['id'] &&
                $request->addClient['firstNames'] &&
                $request->addClient['lastNames'] &&
                $request->addClient['countryCode'] &&
                $request->addClient['telephone'] &&
                $request->addClient['email']
                ){
                    $clientTwo = new Request([
                        'clientId'   => $result['stepOne']['identification']['id'],
                        'userId' => null,
                        'firstnames' => $request->addClient['firstNames'],
                        'lastnames' => $request->addClient['lastNames'],
                        'countryCode' => $request->addClient['countryCode'],
                        'telephone' => $request->addClient['telephone'],
                        'email' => $request->addClient['email']
                    ]);
    
                    $result['stepTwo'] = $this->saveClientContactTrait($clientTwo);
                }else {
                    return response()->json([
                        'error' => true,
                        'message' => 'Debes llenar todos campos del contacto del cliente.'
                    ]);
                }

                // paso actividades
               if( $result['stepOne']['identification']['id'] &&
               $request->addClient['activities']){
                   //actividades con id vacio para crear cliente
                   $clientThree = new Request([
                       'clientId' => $result['stepOne']['identification']['id'],
                       'activities' => $request->addClient['activities']
                   ]);
   
                   $result['stepThree'] = $this->saveClientActivitiesTrait($clientThree);
                } else {
                    return response()->json([
                        'error' => true,
                        'message' => 'Debes ingresar almenos una actividad.'
                    ]);
                }

                //paso productos
                if( $result['stepOne']['identification']['id'] &&
                $request->addClient['products']){
                    //productos con id vacio para crear cliente
                    $clientFour = new Request([
                        'clientId' => $result['stepOne']['identification']['id'],
                        'products' => $request->addClient['products']
                    ]);
                    $result['stepFour'] = $this->saveClientProductsTrait($clientFour);
                } else {
                    return response()->json([
                        'error' => true,
                        'message' => 'Debes ingresar al menos un producto.'
                    ]);
                }
            }

            if($request->editClient){

                //paso cliente
                if($request->editClient['name'] &&
                $request->editClient['numberId'] &&
                $request->editClient['description']){
                    $clientOne = new Request([
                        'name'   => $request->editClient['name'],
                        'numberId' => $request->editClient['numberId'],
                        'description' =>$request->editClient['description']
                    ]);
                    $result['stepOne'] = $this->updateClientTrait($clientOne, $request->editClient['clientId']);
                }

                $hashids = new Hashids('assistant-company', 20);
                $decodedId = $hashids->decode($request->editClient['clientId']);
                $client = Client::with('contact', 'products', 'activities')->find($decodedId[0]);
                
                //paso contacto
                if($request->editClient['email']&&
                $request->editClient['firstNames']&&
                $request->editClient['lastNames']&&
                $request->editClient['countryCode']&&
                $request->editClient['telephone']&&
                $request->editClient['email']){
                    if($client->contact){
                        $clientTwo = new Request([
                            'clientId'   =>  $request->editClient['clientId'],
                            'userId' => $hashids->encode($client->contact->id),
                            'personId'=> $hashids->encode($client->contact->personData->id),
                            'firstnames' => $request->editClient['firstNames'],
                            'lastnames' => $request->editClient['lastNames'],
                            'countryCode' => $request->editClient['countryCode'],
                            'telephone' => $request->editClient['telephone'],
                            'email' => $request->editClient['email']
                        ]);
                        $result['stepTwo'] = $this->saveClientContactTrait($clientTwo);
                    } else {
                        $clientTwo = new Request([
                            'clientId'   =>  $request->editClient['clientId'],
                            'userId' => null,
                            'firstnames' => $request->editClient['firstNames'],
                            'lastnames' => $request->editClient['lastNames'],
                            'countryCode' => $request->editClient['countryCode'],
                            'telephone' => $request->editClient['telephone'],
                            'email' => $request->editClient['email']
                        ]);
                        $result['stepTwo'] = $this->saveClientContactTrait($clientTwo);
                    }

                } else {
                    return response()->json([
                        'error' => true,
                        'message' => 'Debes llenar todos campos del contacto del cliente.'
                    ]);
                }

                // paso actividades
                if($request->editClient['activities']){
                    //actividades con id vacio para crear cliente
                    $clientThree = new Request([
                        'clientId' => $request->editClient['clientId'],
                        'activities' => $request->editClient['activities']
                    ]);
                    $result['stepThree'] = $this->saveClientActivitiesTrait($clientThree);
                }else {
                    return response()->json([
                        'error' => true,
                        'message' => 'Debes agregar al menos una actividad.'
                    ]);
                }

                //paso productos
                if($request->editClient['products']){
                    //productos con id vacio para crear cliente
                    $clientFour = new Request([
                        'clientId' => $request->editClient['clientId'],
                        'products' => $request->editClient['products']
                    ]);
                    $result['stepFour'] = $this->saveClientProductsTrait($clientFour);
                }else {
                    return response()->json([
                        'error' => true,
                        'message' => 'Debes agregar al menos un producto.'
                    ]);
                }

            }
            $clients =  Client::with('contact', 'contact.country')->orderBy('name')->get();
            return response()->json(['clients' => $clients, 'client' => $result]);
            
        }catch (\Throwable $th) {
            throw $th;
        }

    }

      public function save(Request $request)
    {
        try {

            $clientOneForm = (object) $request;
            $clientOneResult = (!$clientOneForm->contact_id) ? $this->saveClientTrait($clientOneForm) : $this->saveClientTrait($clientOneForm->contact_id);
            $contact_id = $clientOneResult['stepOne']['id'];
            $client = Client::firstOrNew([
                'id' => $request->id
            ]);
            $client->number_id = $clientOneForm->numberId;
            $client->contact_id = $request->contact_id;
            $client->fill($clientOneForm->all());
           
            return response()->json($client,200);
        }catch (\Throwable $th) {
            throw $th;
        }
    }

    public function deleteClient($id)
    {
        $client = Client::find($id);
        $client->delete();

        return Inertia::render('client/index');
    }

}
