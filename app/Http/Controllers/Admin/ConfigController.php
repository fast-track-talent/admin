<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Application;
use Inertia\Inertia;
use Carbon\Carbon;
use App\Models\CountryState;
use App\Models\StudyType;
use App\Models\Degree;
use App\Models\Position;
use App\Models\Country;
use App\Models\CompanyActivity;
use App\Models\Skill;
use App\Models\Language;
use Hashids\Hashids;


class ConfigController extends Controller
{
    // Countries module methods
    public function countries(Request $request) {
        try {
            $countries = Country::orderBy('name')->get();
            $deletedCountries = Country::onlyTrashed()->orderBy('name')->get();
            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];
            return Inertia::render('config/countries', [
                'sessionUser' => $sessionUser,
                'countries' => $countries,
                'deletedCountries' => $deletedCountries,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function saveCountry(Request $request) {
        try {
            $country = Country::withTrashed()->firstOrNew(['id' => $request->id]);
            $country->fill($request->all());
            $country->save();
            $countries = Country::orderBy('name')->get();
            $deletedCountries = Country::onlyTrashed()->orderBy('name')->get();
            return response()->json([
                'countries' => $countries,
                'deletedCountries' => $deletedCountries,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function deleteCountry($id) {
        try {
            $country = Country::find($id);
            $country->delete();

            $countries = Country::orderBy('name')->get();
            $deletedCountries = Country::onlyTrashed()->orderBy('name')->get();
            return response()->json([
                'countries' => $countries,
                'deletedCountries' => $deletedCountries,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function restoreCountry(Request $request) {
        try {
            $country = Country::withTrashed()->find($request->id);
            $country->restore();

            $countries = Country::orderBy('name')->get();
            $deletedCountries = Country::onlyTrashed()->orderBy('name')->get();
            return response()->json([
                'countries' => $countries,
                'deletedCountries' => $deletedCountries,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    // Countries module methods

    // States module methods
    public function states(Request $request) {
        try {
            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];
            $countries = Country::orderBy('name')->get();
            return Inertia::render('config/states', [
                'sessionUser' => $sessionUser,
                'countries' => $countries,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function getStates($id) {
        try {
            $states = CountryState::where('country', $id)->orderBy('name')->get();
            $deletedStates = CountryState::onlyTrashed()->where('country', $id)->orderBy('name')->get();
            return response()->json([
                'states' => $states,
                'deletedStates' => $deletedStates,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function saveState(Request $request) {
        try {
            $state = CountryState::withTrashed()->firstOrNew([
                'id' => $request->id
            ]);
            $state->fill($request->all());
            $state->save();

            $states = CountryState::where('country', $state->country)->orderBy('name')->get();
            $deletedStates = CountryState::onlyTrashed()->where('country', $state->country)->orderBy('name')->get();
            return response()->json([
                'states' => $states,
                'deletedStates' => $deletedStates,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function deleteState($id) {
        try {
            $state = CountryState::find($id);
            $state->delete();
            $states = CountryState::where('country', $state->country)->orderBy('name')->get();
            $deletedStates = CountryState::onlyTrashed()->where('country', $state->country)->orderBy('name')->get();
            return response()->json([
                'states' => $states,
                'deletedStates' => $deletedStates,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function restoreState(Request $request) {
        try {
            $state = CountryState::withTrashed()->find($request->id);
            $state->restore();
            $states = CountryState::where('country', $state->country)->orderBy('name')->get();
            $deletedStates = CountryState::onlyTrashed()->where('country', $state->country)->orderBy('name')->get();
            return response()->json([
                'states' => $states,
                'deletedStates' => $deletedStates,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }

    }
    // States module methods

    // Study types module methods
    public function studyTypes(Request $request) {
        try {
            $studyTypes = StudyType::orderBy('name')->get();
            $deletedStudyTypes = StudyType::onlyTrashed()->orderBy('name')->get();
            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];
            return Inertia::render('config/study', [
                'sessionUser' => $sessionUser,
                'studyTypes' => $studyTypes,
                'deletedStudyTypes' => $deletedStudyTypes,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function saveStudyType(Request $request) {
        try {
            $study = StudyType::withTrashed()->firstOrNew([
                'id' => $request->id
            ]);
            $study->fill($request->all());
            $study->save();

            $studyTypes = StudyType::orderBy('name')->get();
            $deletedStudyTypes = StudyType::onlyTrashed()->orderBy('name')->get();
    
            return response()->json([
                'studyTypes' => $studyTypes,
                'deletedStudyTypes' => $deletedStudyTypes,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function deleteStudy($id) {
        try {    
            $studyType = StudyType::find($id);
            $studyType->delete();
            $studyTypes = StudyType::orderBy('name')->get();
            $deletedStudyTypes = StudyType::onlyTrashed()->orderBy('name')->get();
            return response()->json([
                'studyTypes' => $studyTypes,
                'deletedStudyTypes' => $deletedStudyTypes,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function restoreStudy(Request $request) {
        try {    
            $studyType = StudyType::withTrashed()->find($request->id);
            $studyType->restore();
            $studyTypes = StudyType::orderBy('name')->get();
            $deletedStudyTypes = StudyType::onlyTrashed()->orderBy('name')->get();
            return response()->json([
                'studyTypes' => $studyTypes,
                'deletedStudyTypes' => $deletedStudyTypes,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    // Study types module methods

    // Degrees module methods
    public function degree(Request $request) {
        try {
            $studyTypes = StudyType::orderBy('name')->get();
            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];
            return Inertia::render('config/degree', [
                'sessionUser' => $sessionUser,
                'studyTypes' => $studyTypes,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function getDegrees($id) {   
        try {
            $degrees = Degree::where('study_type_id', $id)->orderBy('degree')->get();
            $deletedDegrees = Degree::onlyTrashed()->where('study_type_id', $id)->orderBy('degree')->get();
            return response()->json([
                'degrees' => $degrees,
                'deletedDegrees' => $deletedDegrees,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function saveDegree(Request $request) {
        try {
            $degree = Degree::withTrashed()->firstOrNew([
                'id' => $request->id
            ]);
            $degree->study_type_id = $request->study_type_id;
            $degree->degree = $request->degree;
            $degree->save();

            $degrees = Degree::where('study_type_id', $request->study_type_id)->orderBy('degree')->get();
            $deletedDegrees = Degree::onlyTrashed()->where('study_type_id', $request->study_type_id)->orderBy('degree')->get();
            return response()->json([
                'degrees' => $degrees,
                'deletedDegrees' => $deletedDegrees,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
        $degree = Degree::firstOrNew([
            'id' => $request->id
        ]);
        $degree->fill($request->all());
        $degree->save();

        return response()->json($degree);
    }

    public function deleteDegree($id){
        try {
            $degree = Degree::find($id);
            $degree->delete();
            $degrees = Degree::where('study_type_id', $degree->study_type_id)->orderBy('degree')->get();
            $deletedDegrees = Degree::onlyTrashed()->where('study_type_id', $degree->study_type_id)->orderBy('degree')->get();
            return response()->json([
                'degrees' => $degrees,
                'deletedDegrees' => $deletedDegrees,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function restoreDegree(Request $request){
        try {
            $degree = Degree::withTrashed()->find($request->id);
            $degree->restore();
            $degrees = Degree::where('study_type_id', $degree->study_type_id)->orderBy('degree')->get();
            $deletedDegrees = Degree::onlyTrashed()->where('study_type_id', $degree->study_type_id)->orderBy('degree')->get();
            return response()->json([
                'degrees' => $degrees,
                'deletedDegrees' => $deletedDegrees,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    // Degrees module methods

    // Positions module methods
    public function position(Request $request) {
        try {
            $positions = Position::orderBy('position')->get();
            $deletedPositions = Position::onlyTrashed()->orderBy('position')->get();
            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];
            return Inertia::render('config/position', [
                'sessionUser' => $sessionUser,
                'positions' => $positions,
                'deletedPositions' => $deletedPositions,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function savePosition(Request $request) {
        try {
            $position = Position::withTrashed()->firstOrNew([
                'id' => $request->id
            ]);
            $position->fill($request->all());
            $position->save();
            $positions = Position::orderBy('position')->get();
            $deletedPositions = Position::onlyTrashed()->orderBy('position')->get();
    
            return response()->json([
                'positions' => $positions,
                'deletedPositions' => $deletedPositions,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function deletePosition($id) {
        try {
            $position = Position::find($id);
            $position->delete();
            $positions = Position::orderBy('position')->get();
            $deletedPositions = Position::onlyTrashed()->orderBy('position')->get();
            return response()->json([
                'positions' => $positions,
                'deletedPositions' => $deletedPositions,
            ]);

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function restorePosition(Request $request){
        try {
            $position = Position::withTrashed()->find($request->id);
            $position->restore();
            $positions = Position::orderBy('position')->get();
            $deletedPositions = Position::onlyTrashed()->orderBy('position')->get();
            return response()->json([
                'positions' => $positions,
                'deletedPositions' => $deletedPositions,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    // Positions module Methods

    // Company activities module methods
    public function companyActivity(Request $request) {
        try {
            $companyActivities = CompanyActivity::orderBy('activity')->get();
            $deletedCompanyActivities = CompanyActivity::onlyTrashed()->orderBy('activity')->get();
            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];

            return Inertia::render('config/company', [
                'sessionUser' => $sessionUser,
                'companyActivities' => $companyActivities,
                'deletedCompanyActivities' => $deletedCompanyActivities,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function saveCompanyActivity(Request $request) {
        try {
            $companyActivity = CompanyActivity::withTrashed()->firstOrNew([
                'id' => $request->id
            ]);
            $companyActivity->fill($request->all());
            $companyActivity->save();
            $companyActivities = CompanyActivity::orderBy('activity')->get();
            $deletedCompanyActivities = CompanyActivity::onlyTrashed()->orderBy('activity')->get();
    
            return response()->json([
                'companyActivities' => $companyActivities,
                'deletedCompanyActivities' => $deletedCompanyActivities,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function deleteCompanyActivity($id) {
        try {
            $companyActivity = CompanyActivity::find($id);
            $companyActivity->delete();
            $companyActivities = CompanyActivity::orderBy('activity')->get();
            $deletedCompanyActivities = CompanyActivity::onlyTrashed()->orderBy('activity')->get();
            return response()->json([
                'companyActivities' => $companyActivities,
                'deletedCompanyActivities' => $deletedCompanyActivities,
            ]);

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function restoreCompanyActivity(Request $request) {
        try {
            $companyActivity = CompanyActivity::withTrashed()->find($request->id);
            $companyActivity->restore();
            $companyActivities = CompanyActivity::orderBy('activity')->get();
            $deletedCompanyActivities = CompanyActivity::onlyTrashed()->orderBy('activity')->get();
            return response()->json([
                'companyActivities' => $companyActivities,
                'deletedCompanyActivities' => $deletedCompanyActivities,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    // Company activities module methods

    // Skills module methods
    public function skills(Request $request) {
        try {
            $skills = Skill::orderBy('skill')->get();
            $deletedSkills = Skill::onlyTrashed()->orderBy('skill')->get();
            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];
            return Inertia::render('config/skills', [
                'sessionUser' => $sessionUser,
                'skills' => $skills,
                'deletedSkills' => $deletedSkills,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function saveSkill(Request $request) {
        try {
            $skill = Skill::withTrashed()->firstOrNew(['id' => $request->id]);
            $skill->fill($request->all());
            $skill->save();
            $skills = Skill::orderBy('skill')->get();
            $deletedSkills = Skill::onlyTrashed()->orderBy('skill')->get();
            return response()->json([
                'skills' => $skills,
                'deletedSkills' => $deletedSkills,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function deleteSkill($id) {
        try {
            $skill = Skill::find($id);
            $skill->delete();

            $skills = Skill::orderBy('skill')->get();
            $deletedSkills = Skill::onlyTrashed()->orderBy('skill')->get();
            return response()->json([
                'skills' => $skills,
                'deletedSkills' => $deletedSkills,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function restoreSkill(Request $request) {
        try {
            $skill = Skill::withTrashed()->find($request->id);
            $skill->restore();

            $skills = Skill::orderBy('skill')->get();
            $deletedSkills = Skill::onlyTrashed()->orderBy('skill')->get();
            return response()->json([
                'skills' => $skills,
                'deletedSkills' => $deletedSkills,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    // Skills module methods

    // Skills module methods
    public function languages(Request $request) {
        try {
            $languages = Language::orderBy('lang')->get();
            $deletedLanguages = Language::onlyTrashed()->orderBy('lang')->get();
            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];
            return Inertia::render('config/languages', [
                'sessionUser' => $sessionUser,
                'languages' => $languages,
                'deletedLanguages' => $deletedLanguages,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function saveLanguage(Request $request) {
        try {
            $language = Language::withTrashed()->firstOrNew(['id' => $request->id]);
            $language->fill($request->all());
            $language->save();
            $languages = Language::orderBy('lang')->get();
            $deletedLanguages = Language::onlyTrashed()->orderBy('lang')->get();
            return response()->json([
                'languages' => $languages,
                'deletedLanguages' => $deletedLanguages,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function deleteLanguage($id) {
        try {
            $language = Language::find($id);
            $language->delete();

            $languages = Language::orderBy('lang')->get();
            $deletedLanguages = Language::onlyTrashed()->orderBy('lang')->get();
            return response()->json([
                'languages' => $languages,
                'deletedLanguages' => $deletedLanguages,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function restoreLanguage(Request $request) {
        try {
            $language = Language::withTrashed()->find($request->id);
            $language->restore();

            $languages = Language::orderBy('lang')->get();
            $deletedLanguages = Language::onlyTrashed()->orderBy('lang')->get();
            return response()->json([
                'languages' => $languages,
                'deletedLanguages' => $deletedLanguages,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    // Skills module methods
}