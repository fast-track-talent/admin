<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Application;
use Inertia\Inertia;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $subtractc24Hours = Carbon::now()
            ->setTimezone('America/Caracas')
            ->subDays(1)
            ->toDateTimeString();

        $startLast24Hours = Application::where('created_at', '>', $subtractc24Hours)
        ->count();

        $finishedLast24Hours = Application::where('created_at', '>', $subtractc24Hours)
        ->where('finished',true)
        ->count();

        $women = Application::leftJoin('people', 'people.user_id', '=', 'applications.user_id')
        ->where('people.gender','F')
        ->count();

        $men = Application::leftJoin('people', 'people.user_id', '=', 'applications.user_id')
        ->where('people.gender','M')
        ->count();

        $undefined_gender = Application::leftJoin('people', 'people.user_id', '=', 'applications.user_id')
        ->where('people.gender','U')
        ->count();

        $unfinished = Application::where('finished',false)
        ->count();

        $response = [
          [
            'value' => (string)$startLast24Hours,
            'icon'  => 'mdi-clock-outline',
            'title' => 'Iniciadas las últimas 24 horas',
            'color' => 'blue'
          ],
          [
            'value' => (string)$finishedLast24Hours,
            'icon'  => 'mdi-clock-check-outline',
            'title' => 'Finalizadas las últimas 24 horas',
            'color' => 'green accent-4'
          ],
          [
            'value' => (string)$women,
            'icon'  => 'mdi-gender-female',
            'title' => 'Mujeres',
            'color' => 'pink accent-3'
          ],
          [
            'value' => (string)$men,
            'icon'  => 'mdi-gender-male',
            'title' => 'Hombres',
            'color' => 'indigo darken-4'
          ],
          [
            'value' => (string)$undefined_gender,
            'icon'  => 'mdi-gender-male-female',
            'title' => 'Género indefinido',
            'color' => 'blue-grey'
          ],
          [
            'value' => (string)$unfinished,
            'icon'  => 'mdi-alert-circle-outline',
            'title' => 'Iniciadas sin finalizar',
            'color' => 'orange darken-3'
          ]
        ];

        $sessionUser = [
          'id' => $request->user()->id,
          'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
          'email' => $request->user()->email,
        ];

        return Inertia::render('Dashboard',[
          'data' => $response,
          'sessionUser' => $sessionUser
        ]);
    }
}
