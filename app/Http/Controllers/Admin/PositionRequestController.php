<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Http\Resources\Profiles\PositionRequestResource;
use App\Http\Resources\TalentToNominateResource;
use App\Models\PositionRequest;
use App\Models\CompanyActivity;
use App\Models\Position;
use App\Models\SalaryExpectation;
use App\Models\Country;
use App\Models\StudyType;
use App\Models\Degree;
use App\Models\Language;
use App\Models\LanguageLevel;
use App\Models\Skill;
use App\Models\ExperienceRange;
use App\Models\AgeRange;
use App\Models\User;
use App\Models\Nomination;
use App\Models\RequestComment;

class PositionRequestController extends Controller
{

    public function index(Request $request) {
        try {
            $companyActivities = CompanyActivity::orderBy('activity')->get();
            $positions = Position::orderBy('position')->get();
            $salary_expectations = SalaryExpectation::all();
            $countries = Country::orderBy('name')->get();
            $studyTypes = StudyType::orderBy('name')->get();
            $degrees = Degree::orderBy('degree')->get();
            $languages = Language::orderBy('lang')->get();
            $languageLevels = LanguageLevel::orderBy('level')->get();
            $skills = Skill::orderBy('skill')->get();
            $experienceRanges = ExperienceRange::all();
            $ageRanges = AgeRange::all();

            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames. ' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];

            $positionRequests = PositionRequest::with(['position', 'country_rel'])->get();

            return Inertia::render('position_requests/', [
                'sessionUser' => $sessionUser,
                'positionRequests' => PositionRequestResource::collection($positionRequests),
                'companyActivities' => $companyActivities,
                'positions' => $positions,
                'salaryExpectations' => $salary_expectations,
                'countries' => $countries,
                'studyTypes' => $studyTypes,
                'degrees' => $degrees,
                'languages' => $languages,
                'languageLevels' => $languageLevels,
                'skills' => $skills,
                'experienceRanges' => $experienceRanges,
                'ageRanges' => $ageRanges,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function getStates($countryId) {
        try {
            $states = CountryState::where('country', $countryId)->orderBy('name', 'asc')->get();
            return response()->json([
                'error' => false,
                'states' => $states
            ], 200);
        } catch(ModelNotFoundException $mnfe) {
            return response()->json([
                'error' => true,
                'message' => 'No se ha encontrado el país'
            ], 200);
        } catch (Exception $ex) {
            //throw $th;
        }
    }

    public function filterPositionRequests(Request $request) {
        try {
            $filters = $request->filters;
            $query = DB::table('users');
            $query->join('people', 'users.id', '=', 'people.user_id');
            $query->join('applications', 'applications.user_id', '=', 'users.id');

            if (
                array_key_exists('position', $filters) ||
                array_key_exists('companyActivity', $filters) ||
                array_key_exists('expRange', $filters)
            ) {
                $query->join('experiences', 'experiences.user_id', '=', 'users.id');
            }
            if (array_key_exists('studiesFilters', $filters)) $query->join('user_studies', 'user_studies.user_id', '=', 'users.id');
            if (array_key_exists('chosenLangs', $filters)) $query->join('user_langs', 'user_langs.user_id', '=', 'users.id');
            // if (array_key_exists('chosenSkills', $filters)) $query->join('user_skills', 'user_skills.user_id', '=', 'users.id');

            $query->select(
                'users.id',
                'people.firstnames',
                'people.lastnames',
                'people.gender',
                'users.email',
                'applications.reputation',
                'applications.finished',
            );
            // $query->selectRaw("TIMESTAMPDIFF(YEAR, DATE(people.birthdate), current_date) AS age");
            $query->where(function ($query) use ($request, $filters) {
                $query->where('users.user_type_id', 3);

                if (array_key_exists('gender', $filters)) {
                    if ($filters['gender'] === 'NULL') {
                        $query->whereNull('people.gender');
                    } else {
                        $query->where('people.gender', $filters['gender']);
                    }
                }

                if (array_key_exists('companyActivity', $filters)) $query->where('experiences.company_activity_id', $filters['companyActivity']);

                if (array_key_exists('position', $filters)) $query->where('experiences.position', $filters['position']);

                if (array_key_exists('salaryExpectation', $filters)) $query->where('applications.salary_expect_id', $filters['salaryExpectation']);

                if (array_key_exists('country', $filters)) $query->where('people.country', $filters['country']);

                if (array_key_exists('studiesFilters', $filters)) {
                    $studyTypes = [];
                    $degrees = [];
                    for ($i = 0; $i < count($filters['studiesFilters']); $i++) { 
                        $st = $filters['studiesFilters'][$i]['studyType'];
                        $d = $filters['studiesFilters'][$i]['degree'];
                        if ($st !== null) array_push($studyTypes, $st);
                        if ($d !== null) array_push($degrees, $d);
                    }
                    if (count($studyTypes) > 0) $query->whereIn('user_studies.study_type_id', $studyTypes);
                    if (count($degrees) > 0) $query->whereIn('user_studies.degree_id', $degrees);
                }

                if (array_key_exists('chosenLangs', $filters)) {
                    $langs = [];
                    for ($i = 0; $i < count($filters['chosenLangs']); $i++) { 
                        $cl = $filters['chosenLangs'][$i];
                        if ($cl !== null) array_push($langs, $cl);
                    }
                    if (count($langs) > 0) $query->whereIn('user_langs.lang_id', $langs);
                }

                /* if (array_key_exists('chosenSkills', $filters)) {
                    $skills = [];
                    for ($i = 0; $i < count($filters['chosenSkills']); $i++) { 
                        $cs = $filters['chosenSkills'][$i];
                        if ($cs !== null) array_push($skills, $cs);
                    }
                    if (count($skills) > 0) $query->whereIn('user_langs.lang_id', $skills);
                } */

                if (array_key_exists('ageRange', $filters)) {
                    $ageFilters = $filters['ageRange'];
                    if ($ageFilters['id'] === 6) {
                        $minAge = $ageFilters['min'];
                        $minDate = Carbon::today()->subYears($minAge);
                        $query->whereDate('people.birthdate', '<=', $minDate);
                    } else {
                        $minAge = $ageFilters['min'];
                        $maxAge = $ageFilters['max'];
                        $minDate = Carbon::today()->subYears($maxAge+1); // make sure to use Carbon\Carbon in the class
                        $maxDate = Carbon::today()->subYears($minAge)->endOfDay();
                        $query->whereBetween('people.birthdate', [$minDate, $maxDate]);
                    }
                }

                if (array_key_exists('expRange', $filters)) {
                    $expFilters = $filters['expRange'];
                    if ($expFilters['id'] === 1) {
                        $max = $expFilters['max'];
                        $query->whereRaw("IF(`experiences`.`to` IS NULL, TIMESTAMPDIFF(year, `experiences`.`from` , NOW()), TIMESTAMPDIFF(year, `experiences`.`from`, `experiences`.`to`)) <= $max");
                    } else if ($expFilters['id'] === 4) {
                        $min = $expFilters['min'];
                        $query->whereRaw("IF(`experiences`.`to` IS NULL, TIMESTAMPDIFF(year, `experiences`.`from` , NOW()), TIMESTAMPDIFF(year, `experiences`.`from`, `experiences`.`to`)) >= $min");
                    } else {
                        $max = $expFilters['max'];
                        $min = $expFilters['min'];
                        $query->whereRaw("IF(`experiences`.`to` IS NULL, TIMESTAMPDIFF(year, `experiences`.`from` , NOW()), TIMESTAMPDIFF(year, `experiences`.`from`, `experiences`.`to`)) BETWEEN $min AND $max");
                    }
                }
            });
            $query->orderBy('people.firstnames','asc');
            $query->groupBy(
                'users.id',
                'people.firstnames',
                'people.lastnames',
                'people.birthdate',
                'people.gender',
                'users.email',
                'applications.reputation',
                'applications.finished',
            );
            return response()->json([
                'data' => $query->get()
            ]);
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

    public function showDetail(Request $request, $id) {
        try {
            $positionRequest = PositionRequest::with([
                'client',
                'client.contact',
                'client.contact.personData',
                'client.contact.country',
                'comments',
                'comments.user',
                'comments.user.personData',
                'position',
                'salaryExpectation',
                'country_rel',
                'state',
                'candidateDetails',
                'candidateDetails.ageRange',
                'candidateDetails.expRange',  'studies',
                'studies.studyType',
                'posReqLangs.language',
                'posReqLangs.level',
                'skills',
                'posReqNominations',
                'posReqNominations.talent',
                'posReqNominations.talent.country',
                'posReqNominations.talent.personData'
            ])->find($id);
            $sessionUser = [
                'id' => $request->user()->id,
                'fullname' => $request->user()->personData->firstnames.' '. $request->user()->personData->lastnames,
                'email' => $request->user()->email,
            ];
            return Inertia::render('position_requests/detail',[
                'sessionUser' => $sessionUser,
                'positionRequest' => $positionRequest,
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function searchTalents(Request $request) {
        try {
            $param = "%{$request->value}%";
            $talents = User::with(['personData', 'personData.country', 'personData.state', 'application'])->where('email', 'like', $param)->where('user_type_id', '=', 3)->get();

            return response()->json([
                'success' => true,
                'talents' => TalentToNominateResource::collection($talents)
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function nominateTalents(Request $request) {
        try {
            DB::beginTransaction();
            $posReqId = $request->posReqId;
            $userIds = $request->userIds;
            for ($i = 0; $i < count($userIds); $i++) { 
                Nomination::create([
                    'position_request_id' => $posReqId,
                    'user_id' => $userIds[$i],
                ]);
            }
            DB::commit();
    
            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function saveComment(Request $request) {
        try {
            DB::beginTransaction();
            if ($request->commentId) {
                $requestComment = RequestComment::find($request->commentId);
            } else {
                $requestComment = new RequestComment;
            }
            $requestComment->user_id = $request->userId;
            $requestComment->request_id = $request->requestId;
            $requestComment->comment = $request->comment;
            $requestComment->save();

            DB::commit();
    
            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function deleteComment($id) {
        try {
            DB::beginTransaction();
            $requestComment = RequestComment::find($id);
            $requestComment->delete();
            DB::commit();
    
            return response()->json([
                'success' => true,
            ]);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }
}
