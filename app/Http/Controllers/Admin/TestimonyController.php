<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Testimony;
use App\Models\Person;
use Inertia\Inertia;
use Illuminate\Support\Facades\DB;

class TestimonyController extends Controller
{
    public function index(Request $request)
    {
        return Inertia::render('testimony/index');
    }
    public function storeTestimony(Request $request)
    {

      $testimony = Testimony::firstOrNew([
        'id' => $request->id
        ]);
        $testimony->fill($request->all());
        $testimony->save();

       return response()->json($testimony);
    }
    public function get(Request $request)
    {
      
        $data = Testimony::select(
            'testimonies.id',
            'testimonies.description',
            'testimonies.type',
            'testimonies.user_id'
          )
            ->with('client', 'person')
            ->join('people', 'people.user_id', '=', 'testimonies.user_id')
            ->where(function ($query) use ($request)
          { 
            if($request->search)
            {
                $query->whereRaw('concat(people.firstnames, " ", people.lastnames) LIKE ?', ["%{$request->search}%"])
                    ->orWhere('description', "like", "%" . $request->search . "%");
            }
          });
        
          $data->orderBy('id','desc')->groupBy(
            'testimonies.id',
            'testimonies.description',
            'testimonies.type',
            'testimonies.user_id'
          );

        if ($request->perPage > 0)
        {   
            
            $data = $data->paginate($request->perPage);
        }
        else
        {   
            $data = $data->get();
        }

        return response()->json($data);
    }
    public function delete($id)
    {
        $client = Testimony::find($id);
        $client->delete();

        return response()->json($client);
    }
}