<?php

namespace App\Http\Controllers\Interview;

use Illuminate\Http\Request;
use App\Http\Traits\QuickEmailVerificationTrait;
use App\Http\Traits\Interview\ContactTrait;
use App\Models\Country;
use App\Http\Resources\Interview\CountriesResource;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    use QuickEmailVerificationTrait, ContactTrait;

    public function getStepData() {
        try {
            $countryCodes = CountriesResource::collection(Country::orderBy('name')->get());
            return response()->json([
                'countryCodes' => $countryCodes 
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function registerUser(Request $req) {
        try {
            $apiTesting = (bool) getenv('API_TESTING');
            if ($apiTesting) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);

                $res = json_decode($res);
                $res = (array) $res;
            }

            if($res['success']) { // Recaptcha was successfull
                if ($apiTesting) {
                    $result = $this->registerUserTrait($req);
                } else {
                    $verified = $this->verify($req->email);

                    if($verified === 'valid') {
                        $result = $this->registerUserTrait($req);
                    } else {
                        return [
                            'notValidEmail' => true
                        ];
                    }
                }
            } else { // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }

            return response()->json($result, 200);

        } catch (\Throwable $th) {
            throw $th;
        }

    }

    public function updateUser(Request $req, $id)
    {
        try {
            $apiTesting = (bool) getenv('API_TESTING');
            if ($apiTesting) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);

                $res = json_decode($res);
                $res = (array) $res;
            }

            if($res['success']) { // Recaptcha was successfull
                if ($apiTesting) {
                    $result = $this->updateUserTrait($req, $id);
                } else {
                    $verified = $this->verify($req->email);

                    if($verified === 'valid') {
                        $result = $this->updateUserTrait($req, $id);
                    } else {
                        return [
                            'notValidEmail' => true,
                        ];
                    }
                }
            } else {
                // Recaptcha errors.
                return [
                    'captchaError' => true
                ];
            }
            return response()->json($result, 200);

        } catch (\Throwable $th) {
            throw $th;
        }


    }
}