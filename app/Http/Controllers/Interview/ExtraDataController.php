<?php

namespace App\Http\Controllers\Interview;

use Illuminate\Http\Request;
use App\Models\Application;
use App\Models\User;
use Hashids\Hashids;
use App\Http\Traits\Interview\ExtraDataTrait;
use App\Http\Controllers\Controller;

class ExtraDataController extends Controller
{
    use ExtraDataTrait;

    public function syncExtras(Request $req, $id) {
        try {
            if (getenv('API_TESTING')) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);
    
                $res = json_decode($res);
                $res = (array) $res;
            }

            if($res['success']) { // Recaptcha was successfull
                $result = $this->syncExtrasTrait($req, $id);
            } else {
                // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }
            return response()->json($result,200);

        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
