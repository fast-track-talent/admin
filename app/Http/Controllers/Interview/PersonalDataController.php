<?php

namespace App\Http\Controllers\Interview;

use Illuminate\Http\Request;
use App\Models\CountryState;
use App\Http\Resources\Interview\StatesResource;
use App\Http\Resources\Interview\InterviewCountryCodesResource;
use App\Http\Resources\Interview\CountriesResource;
use App\Models\Country;
use App\Http\Traits\Interview\PersonalDataTrait;
use Hashids\Hashids;
use App\Http\Controllers\Controller;

class PersonalDataController extends Controller
{
    use PersonalDataTrait;

    public function getStepData() {
        try {
            $countries = CountriesResource::collection(Country::orderBy('name')->get());
            return response()->json([
                'countries' => $countries 
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function getStates($code)
    {
        try {
            $hashids = new Hashids('countries-talents-assistant', 20);
            $decoded = $hashids->decode($code);
            if (count($decoded) === 0) {
                return response()->json([
                    'validCountry' => false
                ], 200);
            }
            $states = CountryState::where('country', $decoded[0])->orderBy('name')->get();
            return response()->json([
                "states" => StatesResource::collection($states)
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function getState($code)
    {
        try {
            $hashids = new Hashids('countries-talents-assistant', 20);
            $decoded = $hashids->decode($code);
            if (count($decoded) === 0) {
                return response()->json([
                    "error" => 'No se ha encontrado el país'
                ], 200);
            }
            $state = CountryState::find($decoded[0]);
            return response()->json([
                "state" => $state->name
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function registerPersonData(Request $req)
    {
        try {
            if (getenv('API_TESTING')) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);

                $res = json_decode($res);
                $res = (array) $res;
            }

            if($res['success']) { // Recaptcha was successfull
                $result = $this->registerPersonDataTrait($req);
            } else {
                // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }
            return response()->json($result, 200);

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function updatePersonData(Request $req, $id)
    {
        try {
            if (getenv('API_TESTING')) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);

                $res = json_decode($res);
                $res = (array) $res;
            }

            if($res['success']) { // Recaptcha was successfull
                $result = $this->updatePersonDataTrait($req, $id);
            } else {
                // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }
            return response()->json($result, 200);

        } catch (\Throwable $th) {
            throw $th;
        }
    }

}
