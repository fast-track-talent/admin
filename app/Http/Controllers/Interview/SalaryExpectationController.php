<?php

namespace App\Http\Controllers\Interview;

use Illuminate\Http\Request;
use App\Models\SalaryExpectation;
use App\Http\Resources\SalaryExpectationsResource;
use App\Http\Traits\Interview\SalaryExpectationTrait;
use App\Http\Controllers\Controller;

class SalaryExpectationController extends Controller
{
    use SalaryExpectationTrait;
    
    public function getSalaryExpectations()
    {
        try {
            $salaries = SalaryExpectation::all();
            return response()->json([
                'salaries' => SalaryExpectationsResource::collection($salaries)
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function saveApplicationSalary(Request $req, $id)
    {
        try {
            if (getenv('API_TESTING')) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);
    
                $res = json_decode($res);
                $res = (array) $res;
            }

            if($res['success']) { // Recaptcha was successfull
                $result = $this->saveApplicationSalaryTrait($req, $id);
            } else {
                // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }
            return response()->json($result, 200);
            
        } catch (\Throwable $th) {
            throw $th;
        }

    }
}
