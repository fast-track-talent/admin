<?php

namespace App\Http\Controllers\Interview;

use App\Http\Resources\Interview\CountriesResource;

use App\Http\Resources\Interview\LanguageLevelsResource;
use App\Http\Resources\Interview\StudyTypesResource;
use App\Http\Resources\Interview\LanguagesResource;
use App\Http\Resources\Interview\DegreesResource;
use App\Http\Traits\Interview\StudiesTrait;

use App\Models\LanguageLevel;
use Illuminate\Http\Request;
use App\Models\StudyType;
use App\Models\Language;
use App\Models\Country;
use App\Models\Degree;
use App\Http\Controllers\Controller;


class StudiesController extends Controller
{
    use StudiesTrait;

    public function getStepData()
    {
        try {
            $studyTypes = StudyType::orderBy('name')->get();
            $degrees = Degree::orderBy('degree')->get();
            $languages = Language::orderBy('lang')->get();
            $languageLevels = LanguageLevel::all();
            $countries = CountriesResource::collection(Country::orderBy('name')->get());
            return response()->json([
                'studyTypes' => StudyTypesResource::collection($studyTypes),
                'degrees' => DegreesResource::collection($degrees),
                'languages' => LanguagesResource::collection($languages),
                'languageLevels' => LanguageLevelsResource::collection($languageLevels),
                'countries' => CountriesResource::collection($countries)
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function saveStudies(Request $req)
    {
        try {
            if (getenv('API_TESTING')) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);
    
                $res = json_decode($res);
                $res = (array) $res;
            }

            if ($res['success']) { // Recaptcha was successfull
                $result = $this->saveStudiesTrait($req);
            } else {
                // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }
            return response()->json($result, 200);
        
        } catch (\Throwable $th) {
            throw $th;
        }
            
    }
}
