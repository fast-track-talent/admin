<?php

namespace App\Http\Controllers\Interview;

use Illuminate\Http\Request;

use App\Http\Resources\Profiles\CompanyActivitiesResource;
use App\Http\Resources\Profiles\PositionsResource;
use App\Models\CompanyActivity;
use App\Models\Position;
use App\Http\Traits\Interview\WorkExperienceTrait;
use App\Http\Controllers\Controller;

class WorkExperienceController extends Controller
{
    use WorkExperienceTrait;

    public function getStepData()
    {
        try {
            $companyActivities = CompanyActivity::orderBy('activity')->get();
            $positions = Position::orderBy('position')->get();
            return response()->json([
                'companyActivities' => CompanyActivitiesResource::collection($companyActivities),
                'positions' => PositionsResource::collection($positions)
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function syncExperiences(Request $req)
    {
        try {
            if (getenv('API_TESTING')) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);
    
                $res = json_decode($res);
                $res = (array) $res;
            }

            if($res['success']) { // Recaptcha was successfull
                $result = $this->syncExperiencesTrait($req);
            } else {
                // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }
            return response()->json($result,200);

        } catch (\Throwable $th) {
            throw $th;
        }
       
    }
}
