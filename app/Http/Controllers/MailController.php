<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Traits\QuickEmailVerificationTrait;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Models\PositionRequest;
use Illuminate\Http\Request;
use App\Mail\AssistantMail;
use App\Mail\CompanyAssistantMail;
use App\Mail\ContactMail;
use App\Models\Client;
use Hashids\Hashids;
use App\Models\User;

class MailController extends Controller
{
    use QuickEmailVerificationTrait;
    public function sendCompletedAssesmentEmail($id) {

        try {
            // We have to get user email to send.
            // Decode user id
            $userIdHash = new Hashids('assistant-user', 20);
            $decodedUserId = $userIdHash->decode($id);
            if (count($decodedUserId) > 0) {
                // get user information.
                $user = User::find($decodedUserId[0]);
    
                if (!$user->application->sent_finished_email) {
                    // Set the email as sent in DB.
                    $application['finished'] = 1;
                    $application['sent_finished_email'] = 1;
                    $user->application()->update($application);
                    $user->refresh();
                    
                    // Set users email information.
                    $userMessage = [
                        'forAdmins' => false,
                        'subject' => '¡Hemos recibido tu aplicación!',
                        'title' => '¡Bienvenido a Fast Track Talent!',
                        'userData' => $user,
                        'personalData' => $user->personData
                    ];
        
                    // Set admins information.
                    $adminMessage = [
                        'forAdmins' => true,
                        'subject' => '¡Nueva entrevista completada!',
                        'title' => 'Una persona ha completado su entrevista. Ya puedes ver su perfil profesional en Fast Track Talent Admin',
                        'userData' => $user,
                        'personalData' => $user->personData
                    ];
        
                    // Send email to user.
                    Mail::to($user->email)->send(new AssistantMail($userMessage));
                    // Send email to admin.
                    Mail::to(env('APPLICATIONS_MAIL_ADDRESS'))->send(new AssistantMail($adminMessage));
                }
            } else {
                return ['error' => 'No se ha encontrado el usuario'];
            }

            return response()->json([
                'success' => true
            ], 200);
        } catch (Exception $th) {
            return response()->json([
                'success' => false,
                'error' => $th->getMessage()
            ], 200);
        }
    }
    
    public function sendCompletedCompanyAssesmentEmail(Request $req) {

        try {
            DB::beginTransaction();
            // We have to get client contact email to send.
            // Decode client contact id
            $hashids = new Hashids('assistant-company', 20);
            if (!$decodedContactId = $hashids->decode($req->contactId)) throw new ModelNotFoundException;
            // get user information.
            $user = User::find($decodedContactId[0]);
            if (!$decodedClientId = $hashids->decode($req->clientId)) throw new ModelNotFoundException;
            // get client information.
            $client = Client::find($decodedClientId[0]);

            // set all position requests as finished
            $posReqs = $req->positionRequests;
            $positionRequestsData = [];
            for ($i = 0; $i < count($posReqs); $i++) {

                // Check if the position request exists...
                if (!$decodedPosReq = $hashids->decode($posReqs[$i])) throw new ModelNotFoundException;
                $posReq = PositionRequest::findOrFail($decodedPosReq[0]);
                $posReq->finished = 1;
                $posReq->save();
                $posReq->refresh();
                array_push($positionRequestsData, $posReq);
            }

            // Set users email information.
            $userMessage = [
                'forAdmins' => false,
                'subject' => '¡Hemos recibido tu requerimiento!',
                'title' => '¡Bienvenido a Fast Track Talent!',
                'userData' => $user,
                'personalData' => $user->personData,
                'positionRequests' => $positionRequestsData
            ];

            // Set admins information.
            $adminMessage = [
                'forAdmins' => true,
                'subject' => '¡Nueva petición de talentos!',
                'title' => 'Una empresa ha solicitado talentos',
                'userData' => $user,
                'personalData' => $user->personData,
                'client' => $client,
                'positionRequests' => $positionRequestsData
            ];

            // Send email to user.
            Mail::to($user->email)->send(new CompanyAssistantMail($userMessage));
            // Send email to admin.
            Mail::to(env('REQUESTS_MAIL_ADDRESS'))->send(new CompanyAssistantMail($adminMessage));

            DB::commit();

            return response()->json([
                'success' => true
            ], 200);
        } catch (ModelNotFoundException $ex) {
            DB::rollback();
            return [
                'error' => true,
                'message' => 'Revise los datos enviados'
            ];
        } catch (Exception $th) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'error' => $th->getMessage()
            ], 200);
        }
    }

    public function sendContactEmail(Request $req) {
        try {
            $secret = getenv('RECAPTCHA_SECRET');
            // We validate the token of the recaptcha.
            $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);

            $res = json_decode($res);
            $res = (array) $res;

            if($res['success']) { // Recaptcha was successfull
                $verified = $this->verify($req->email);

                if($verified === 'valid') {
                    // Set admins information.
                    $details = [
                        'emailSubject' => '¡Nuevo Mensaje de Contacto!',
                        'title' => 'Una persona ha enviado un mensaje de contacto',
                        'fullname' => $req->fullname,
                        'email' => $req->email,
                        'category' => $req->category,
                        'message' => $req->message
                    ];

                    // Send email to admin.
                    Mail::to(env('CONTACT_MAIL_ADDRESS'))->send(new ContactMail($details));

                    $result = [
                        'validEmail' => true,
                        'success' => true
                    ];
                } else {
                    $result = [
                        'validEmail' => false,
                        'success' => true
                    ];
                }
            } else {
                // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }
            return response()->json($result, 200);
        } catch (Exception $th) {
            return response()->json([
                'success' => false,
                'error' => $th->getMessage()
            ], 200);
        }
    }
}
