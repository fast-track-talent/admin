<?php

namespace App\Http\Controllers\Profiles;

use Illuminate\Http\Request;
use App\Http\Traits\QuickEmailVerificationTrait;
use App\Http\Traits\Profiles\ContactTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Interview\CountriesResource;
use App\Models\Country;

class ContactController extends Controller
{
    use ContactTrait, QuickEmailVerificationTrait;

    public function getStepData() {
        try {
            $countries = CountriesResource::collection(Country::orderBy('name')->get());
            return response()->json([
                'countries' => $countries
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    
    public function saveClientContact(Request $req) {
        try {
            $apiTesting = (bool) getenv('API_TESTING');
            if ($apiTesting) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);
    
                $res = json_decode($res);
                $res = (array) $res;
            }

            if($res['success']) { // Recaptcha was successfull
                if ($apiTesting) {
                    $result = $this->saveClientContactTrait($req);
                } else {
                    $verified = $this->verify($req->email);
    
                    if($verified === 'valid') {
                        $result = $this->saveClientContactTrait($req);
                    } else {
                        return [
                            'notValidEmail' => true,
                        ];
                    }
                }
            } else {
                // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }

            return response()->json($result, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    //
}
