<?php

namespace App\Http\Controllers\Profiles;

use Illuminate\Http\Request;
use App\Http\Traits\Profiles\IdentificationTrait;
use App\Http\Controllers\Controller;

class IdentificationController extends Controller
{
    use IdentificationTrait;
    
    public function saveClient(Request $req) {
        try {
            $apiTesting = (bool) getenv('API_TESTING');
            if ($apiTesting) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);
    
                $res = json_decode($res);
                $res = (array) $res;
            }

            if($res['success']) { // Recaptcha was successfull
                $result = $this->saveClientTrait($req);
            } else {
                // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }

            return response()->json($result, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function updateClient(Request $req, $id) {
        try {
            $apiTesting = (bool) getenv('API_TESTING');
            if ($apiTesting) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);
    
                $res = json_decode($res);
                $res = (array) $res;
            }

            if($res['success']) { // Recaptcha was successfull
                $result = $this->updateClientTrait($req, $id);
            } else {
                // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }

            return response()->json($result, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
