<?php

namespace App\Http\Controllers\Profiles;

use Illuminate\Http\Request;
use App\Http\Traits\Profiles\IndustrySectorTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Profiles\CompanyActivitiesResource;
use App\Models\CompanyActivity;

class IndustrySectorController extends Controller
{
    use IndustrySectorTrait;
    

    public function getStepData() {
        try {
            $activities = CompanyActivitiesResource::collection(CompanyActivity::all());
            return response()->json([
                'activities' => $activities
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function saveClientIndustries(Request $req) {
        try {
            if (getenv('API_TESTING')) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);
    
                $res = json_decode($res);
                $res = (array) $res;
            }
    
            if($res['success']) { // Recaptcha was successfull
                $result = $this->saveClientActivitiesTrait($req);
            } else {
                // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }
    
            return response()->json($result, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}