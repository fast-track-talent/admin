<?php

namespace App\Http\Controllers\Profiles;

use Illuminate\Http\Request;
use App\Http\Traits\Profiles\ProductsTrait;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    use ProductsTrait;
    
    public function saveClientProducts(Request $req) {
        try {
            if (getenv('API_TESTING')) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);
    
                $res = json_decode($res);
                $res = (array) $res;
            }
    
            if($res['success']) { // Recaptcha was successfull
                $result = $this->saveClientProductsTrait($req);
            } else {
                // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }
    
            return response()->json($result, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
