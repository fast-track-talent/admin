<?php

namespace App\Http\Controllers\Profiles;

use Illuminate\Http\Request;
use App\Http\Traits\Profiles\ProfileRequestsTrait;
use App\Models\SalaryExpectation;
use App\Models\Position;
use App\Http\Resources\SalaryExpectationsResource;
use App\Http\Resources\Profiles\PositionsResource;
use App\Http\Controllers\Controller;

class ProfileRequestsController extends Controller
{
    use ProfileRequestsTrait;

    public function getStepData()
    {
        try {
            $salaries = SalaryExpectation::all();
            $positions = Position::orderBy('position')->get();
            return response()->json([
                'salaries' => SalaryExpectationsResource::collection($salaries),
                'positions' => PositionsResource::collection($positions),
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function savePositionRequests(Request $req) {
        try {
            if (getenv('API_TESTING')) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);
    
                $res = json_decode($res);
                $res = (array) $res;
            }
    
            if($res['success']) { // Recaptcha was successfull
                $result = $this->savePositionRequestsTrait($req);
            } else {
                // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }
    
            return response()->json($result, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
