<?php

namespace App\Http\Controllers\Profiles;
use App\Http\Resources\Profiles\LanguageLevelsResource;
use App\Http\Resources\Profiles\LanguagesResource;
use App\Http\Resources\SkillsResource;
use App\Http\Traits\Profiles\ProfileSkillsTrait;
use App\Models\LanguageLevel;
use Illuminate\Http\Request;
use App\Models\Language;
use App\Models\Skill;
use App\Http\Controllers\Controller;

class ProfileSkillsController extends Controller
{
    use ProfileSkillsTrait;

    public function getStepData(){
        try {
            $languages = Language::orderBy('lang')->get();
            $languageLevels = LanguageLevel::all();
            return response()->json([
                'languages' => LanguagesResource::collection($languages),
                'languageLevels' => LanguageLevelsResource::collection($languageLevels),
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function getSkills(Request $req) {
        try {
            $value = $req->skill;
            $param = "%$value%";
            $skills = Skill::where('skill', 'like', $param)->get();
            return response()->json([
                'skills' => SkillsResource::collection($skills)
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function savePositionRequestsSkills(Request $req) {
        try {
            if (getenv('API_TESTING')) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);
    
                $res = json_decode($res);
                $res = (array) $res;
            }
    
            if($res['success']) { // Recaptcha was successfull
                $result = $this->savePositionRequestsSkillsTrait($req);
            } else {
                // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }
    
            return response()->json($result, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
