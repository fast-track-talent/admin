<?php

namespace App\Http\Controllers\Profiles;

use Illuminate\Http\Request;
use App\Http\Traits\Profiles\ProfileYearsOfExperienceTrait;
use App\Models\AgeRange;
use App\Models\ExperienceRange;
use App\Models\Skill;
use App\Models\Language;
use App\Models\LanguageLevel;
use App\Models\Country;
use App\Models\CompanyActivity;
use App\Models\StudyType;
use App\Models\Degree;
use App\Http\Resources\Profiles\AgeRangesResource;
use App\Http\Resources\Profiles\ExpRangesResource;
use App\Http\Resources\SkillsResource;
use App\Http\Resources\Profiles\LanguagesResource;
use App\Http\Resources\Profiles\LanguageLevelsResource;
use App\Http\Resources\Interview\CountriesResource;
use App\Http\Resources\Profiles\CompanyActivitiesResource;
use App\Http\Resources\Profiles\StudyTypesResource;
use App\Http\Resources\Profiles\DegreesResource;
use App\Http\Controllers\Controller;

class ProfileYearsOfExperienceController extends Controller
{
    use ProfileYearsOfExperienceTrait;

    public function getStepData()
    {
        try {
            $ageRanges = AgeRange::all();
            $expRanges = ExperienceRange::all();
            return response()->json([
                'ageRanges' => AgeRangesResource::collection($ageRanges),
                'expRanges' => ExpRangesResource::collection($expRanges),
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    
    public function getDataForStats()
    {
        try {
            $skillsArr = SkillsResource::collection(Skill::all());
            $languagesArr = LanguagesResource::collection(Language::all());
            $languageLevelsArr = LanguageLevelsResource::collection(LanguageLevel::all());
            $countriesArr = CountriesResource::collection(Country::all());
            $ageRangesArr = AgeRangesResource::collection(AgeRange::all());
            $expRangesArr = ExpRangesResource::collection(ExperienceRange::all());
            $companyActivitiesArr = CompanyActivitiesResource::collection(CompanyActivity::all());
            $studyTypesArr = StudyTypesResource::collection(StudyType::all());
            $degreesArr = DegreesResource::collection(Degree::all());
            return response()->json([
                'skillsArr' => SkillsResource::collection(Skill::all()),
                'languagesArr' => LanguagesResource::collection(Language::all()),
                'languageLevelsArr' => LanguageLevelsResource::collection(LanguageLevel::all()),
                'countriesArr' => CountriesResource::collection(Country::all()),
                'ageRangesArr' => AgeRangesResource::collection(AgeRange::all()),
                'expRangesArr' => ExpRangesResource::collection(ExperienceRange::all()),
                'companyActivitiesArr' => CompanyActivitiesResource::collection(CompanyActivity::all()),
                'studyTypesArr' => StudyTypesResource::collection(StudyType::all()),
                'degreesArr' => DegreesResource::collection(Degree::all()),
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function savePositionRequestsAgeExperiences(Request $req) {
        try {
            if (getenv('API_TESTING')) {
                $res['success'] = true;
            } else {
                $secret = getenv('RECAPTCHA_SECRET');
                // We validate the token of the recaptcha.
                $res = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$req->recaptcha);
    
                $res = json_decode($res);
                $res = (array) $res;
            }
    
            if($res['success']) { // Recaptcha was successfull
                $result = $this->savePositionRequestsAgeExperiencesTrait($req);
            } else {
                // Recaptcha errors.
                return response()->json([
                    'captchaError' => true
                ], 501);
            }
    
            return response()->json($result, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
