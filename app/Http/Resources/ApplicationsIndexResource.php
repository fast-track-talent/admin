<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApplicationsIndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'finished' => $this->finished,
            'fullname' => $this->talent->personData->firstnames . " " .$this->talent->personData->lastnames,
            'gender' => $this->talent->personData->gender,
            'salary_expectation' => [
                'id' => $this->salary_expectation->id,
                'range' => $this->salary_expectation->range,
            ],
            'reputation' => $this->reputation,
            'studies' => $this->getSTudiesIds($this->talent->studies),
            'country' => [
                'id' => $this->talent->country->id,
                'name' => $this->talent->country->name,
            ]
        ];
    }

    private function getSTudiesIds($studies) {
        $ids = [];
        foreach ($studies as $sts) {
            array_push($ids, $sts->study_type_id);
        }

        return $ids;
    }
}
