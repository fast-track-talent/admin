<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApplicationsListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->talent->id,
            'firstnames' => $this->talent->personData->firstnames,
            'lastnames' => $this->talent->personData->lastnames,
            'gender' => $this->talent->personData->gender,
            'email' => $this->talent->email,
            'reputation' => $this->reputation,
            'finished' => $this->finished,
            'curriculum' => $this->curriculum
        ];
    }

}
