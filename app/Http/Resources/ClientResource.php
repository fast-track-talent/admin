<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Profiles\CompanyActivitiesResource;
use App\Http\Resources\Profiles\ClientProductsResource;
use App\Http\Resources\Profiles\CompanyActivitiesWithPivotIdResource;
use Hashids\Hashids;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */


    public function toArray($request)
    {
        $hashids = new Hashids('assistant-company', 20);
        return [
            'id' => $hashids->encode($this->id),
            'name' => $this->name,
            'number_id' => $this->number_id,
            'description' => $this->description,
            'contact' => $this->contact,
            'activities' => CompanyActivitiesWithPivotIdResource::collection($this->activities),
            'products'=> ClientProductsResource::collection($this->products),
            'personData'=> $this->loadMissing('contact.personData'),
            'country' => $this->loadMissing('contact.country'),
        ];
    }
}
