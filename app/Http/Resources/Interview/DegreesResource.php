<?php

namespace App\Http\Resources\Interview;

use Illuminate\Http\Resources\Json\JsonResource;
use Hashids\Hashids;

class DegreesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $hashids = new Hashids('step-four-data-in-client', 20);
        return [
            'id' => $hashids->encode($this->id),
            'studyTypeId' => $hashids->encode($this->study_type_id),
            'degree' => $this->degree
        ];
    }
}
