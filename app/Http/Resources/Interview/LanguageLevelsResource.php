<?php

namespace App\Http\Resources\Interview;

use Illuminate\Http\Resources\Json\JsonResource;
use Hashids\Hashids;

class LanguageLevelsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $hashids = new Hashids('step-four-data-in-client', 20);
        return [
            'id' => $hashids->encode($this->id),
            'level' => $this->level
        ];
    }
}
