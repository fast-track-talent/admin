<?php

namespace App\Http\Resources\Interview;

use Illuminate\Http\Resources\Json\JsonResource;
use Hashids\Hashids;

class SkillsInUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $hashids = new Hashids('assistant-user', 20);
        $skillHash = new Hashids('assistant-company', 20);

        return [
            "id" => $skillHash->encode($this->pivot->id),
            "skill_id" => $skillHash->encode($this->pivot->skill_id),
            "user_id" => $hashids->encode($this->pivot->user_id) 
        ];
    }
}
