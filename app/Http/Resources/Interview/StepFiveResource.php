<?php

namespace App\Http\Resources\Interview;

use Illuminate\Http\Resources\Json\JsonResource;
use Hashids\Hashids;

class StepFiveResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $hashids = new Hashids('step-five-data-in-client', 20);
        $positionsHash = new Hashids('positions-in-client', 20);
        return [
            'experienceId' => $hashids->encode($this->id),
            'company' => $this->company,
            'companyActivityId' => $hashids->encode($this->company_activity_id),
            'position' => $positionsHash->encode($this->position),
            'dependents' => $this->dependents,
            'from' => $this->from,
            'to' => $this->to,
            'actual' => $this->actual,
            'toDelete' => false
        ];
    }
}
