<?php

namespace App\Http\Resources\Interview;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\SkillsInUserResource\Interview;
use Hashids\Hashids;

class UserSkillsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $hashids = new Hashids('assistant-user', 20);
        return [
            "userId" => $hashids->encode($this->id),
            "skills" => SkillsInUserResource::collection($this->skills)
        ];
    }
}