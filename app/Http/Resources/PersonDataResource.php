<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Interview\CountriesResource;
use App\Http\Resources\Interview\StatesResource;
use Hashids\Hashids;

class PersonDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $personDataIdHash = new Hashids('assistant-person', 20);
        $userHash = new Hashids('assistant-user', 20);
        return [
            'id' => $personDataIdHash->encode($this->id),
            'firstnames' => $this->firstnames,
            'lastnames' => $this->lastnames,
            'gender' => $this->gender,
            'country' => new CountriesResource($this->countryInfo),
            'state' => new StatesResource($this->state),
            'birthdate' => $this->birthdate,
            'user_id' => $userHash->encode($this->user_id)
        ];
    }

}
