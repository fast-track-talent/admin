<?php

namespace App\Http\Resources\Profiles;

use Illuminate\Http\Resources\Json\JsonResource;
use Hashids\Hashids;

class ClientPositionRequestsLocationsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $hashids = new Hashids('assistant-company', 20);
        $countryHash = new Hashids('countries-talents-assistant', 20);
        $stateIdHash = new Hashids('states-in-client', 20);

        return [
            'positionRequestId' => $hashids->encode($this->id),
            'countryId' => ($this->country !== null) ? $countryHash->encode($this->country) : null,
            'countryStateId' => ($this->country_state_id !== null) ? $stateIdHash->encode($this->country_state_id) : null,
            'remote' => $this->remote,
            'vehicle' => $this-> vehicle,
            'machine' => $this-> machine
        ];
    }
}
