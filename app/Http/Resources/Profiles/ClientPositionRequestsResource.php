<?php

namespace App\Http\Resources\Profiles;

use Illuminate\Http\Resources\Json\JsonResource;
use Hashids\Hashids;

class ClientPositionRequestsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $hashids = new Hashids('assistant-company', 20);
        $salariesHash = new Hashids('salaries-exp-in-client', 20);
        $positionsHash = new Hashids('positions-in-client', 20);
        return [
            'positionRequestId' => $hashids->encode($this->id),
            'positionId' => $positionsHash->encode($this->position_id),
            'salaryExpectationId' => $salariesHash->encode($this->salary_expectation_id),
            'confidential' => $this->confidential,
            'title' => $this->title,
            'goals' => $this->goals,
            'reportTo' => ($this->report_to === null) ? '' : $this->report_to,
            'superviseTo' => ($this->supervise_to === null) ? '' : $this->supervise_to,
            'finished' => $this->finished,
            "toDelete" => false
        ];
    }
}
