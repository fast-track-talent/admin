<?php

namespace App\Http\Resources\Profiles;

use Illuminate\Http\Resources\Json\JsonResource;
use Hashids\Hashids;

class ClientProductsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $hashids = new Hashids('assistant-company', 20);
        $activitiesHash = new Hashids('step-five-data-in-client', 20);
        return [
            'id' => $hashids->encode($this->id),
            'name' => $this->name,
            'description' => ($this->description === null) ? '' : $this->description
        ];
    }
}
