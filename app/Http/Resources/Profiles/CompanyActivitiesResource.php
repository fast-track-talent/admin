<?php

namespace App\Http\Resources\Profiles;

use Illuminate\Http\Resources\Json\JsonResource;
use Hashids\Hashids;

class CompanyActivitiesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $hashids = new Hashids('step-five-data-in-client', 20);
        return [
            'id' => $hashids->encode($this->id),
            'activity' => $this->activity
        ];
    }
}
