<?php

namespace App\Http\Resources\Profiles;

use Illuminate\Http\Resources\Json\JsonResource;
use Hashids\Hashids;

class CompanyActivitiesWithPivotIdResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   $activityHash = new Hashids('assistant-company', 20);
        $hashids = new Hashids('step-five-data-in-client', 20);
        return [
			"id" => $activityHash->encode($this->pivot->id),
			"activityId"=> $hashids->encode($this->id),
			"toDelete"=> false
        ];
    }
}
