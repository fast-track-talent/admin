<?php

namespace App\Http\Resources\Profiles;

use Illuminate\Http\Resources\Json\JsonResource;
use Hashids\Hashids;

class CountriesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $hashids = new Hashids('countries-talents-assistant', 20);
        return [
            'id' => $hashids->encode($this->id),
            'name' => $this->name,
            'numberCode' => $this->number_code,
        ];
    }
}
