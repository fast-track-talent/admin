<?php

namespace App\Http\Resources\Profiles;

use Illuminate\Http\Resources\Json\JsonResource;
use Hashids\Hashids;

class PosReqSkillsAndLangsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $hashids = new Hashids('assistant-company', 20);
        $langHash = new Hashids('step-four-data-in-client', 20);

        return [
            "posReqId" => $hashids->encode($this->id),
            "skills" => SkillsInPosReqResource::collection($this->skills),
            "langs" => LangsInPosReqResource::collection($this->languages)
        ];
    }
}