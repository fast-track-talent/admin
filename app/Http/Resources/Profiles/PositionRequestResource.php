<?php

namespace App\Http\Resources\Profiles;

use Illuminate\Http\Resources\Json\JsonResource;

class PositionRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $country = null;
        if ($this->country !== null) $country = $this->country_rel->name;
        return [
            'id' => $this->id,
            'title' => $this->title,
            'position' => $this->position->position,
            'country' => $country,
            'finished' => $this->finished
        ];
    }
}
