<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Hashids\Hashids;

class SalaryExpectationsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $hashids = new Hashids('salaries-exp-in-client', 20);
        return [
            "id" => $hashids->encode($this->id),
            "range" => $this->range
        ];
    }
}
