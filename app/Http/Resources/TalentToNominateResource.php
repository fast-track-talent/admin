<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TalentToNominateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
        ];

        if ($this->personData !== null) {
            $data['full_name'] = "{$this->personData->lastnames}, {$this->personData->firstnames}";
            $data['gender'] = getGenderStr($this->personData->gender);
            $data['age'] = $this->personData->age;
            $data['country'] = $this->personData->countryInfo->name;
            $data['state'] = ($this->personData->state !== null) ? $this->personData->state->name : null;
        } else {
            $data['full_name'] = null;
            $data['gender'] = null;
            $data['age'] = null;
            $data['country'] = null;
            $data['state'] = null;
        }

        if ($this->application !== null) {
            $data['reputation'] = "{$this->application->reputation}/10";
            $data['finishedAssistant'] = $this->application->finished;
        } else {
            $data['reputation'] = null;
            $data['finishedAssistant'] = null;
        }
        return $data;
    }
}

function getGenderStr($gender) {
    switch ($gender) {
        case 'M':
            return 'Masculino';
            break;
        case 'F':
            return 'Femenino';
            break;
        
        default:
            return 'Otro';
            break;
    }
}