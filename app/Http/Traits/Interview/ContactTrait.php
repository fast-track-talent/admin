<?php

namespace App\Http\Traits\Interview;

use Illuminate\Support\Facades\DB;
use Hashids\Hashids;
use App\Models\User;
use App\Models\Application;
use App\Http\Resources\Interview\StepFourResource;
use App\Http\Resources\SkillsResource;
use App\Http\Resources\Interview\StepFiveResource;
use App\Http\Resources\Interview\UserLanguagesResource;
use App\Http\Traits\QuickEmailVerificationTrait;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

trait ContactTrait {
    use QuickEmailVerificationTrait;

    public function registerUserTrait($req) {
         try {
            DB::beginTransaction();
            $hashids = new Hashids('assistant-user', 20);
            $countryHash = new Hashids('countries-talents-assistant', 20);
            $stateHash = new Hashids('states-in-client', 20);
            // Check if user exists in db
            $user = User::where([['email', $req->email], ['user_type_id', 3]])->first();
            if ($user === null) { // It does not exist
                // Register user
                $user = new User;
                $user->user_type_id = 3;
                $user->email = $req->email;
                if ($req->countryCode) {
                    $countryDecoded = $countryHash->decode($req->countryCode);
                    if (count($countryDecoded) > 0) {
                        $user->country_code = $countryDecoded[0];
                        $user->phone_number = $req->telephone;
                    } else {
                        DB::rollBack();
                        return ['error' => 'Revise los datos enviados'];
                    }
                }

                $user->updated_at = null;
                $user->save();
                $user->refresh();
                $application = new Application(['finished' => false]);
                $user->application()->save($application);
                DB::commit();
                $result = [
                    'finished' => false,
                    'validEmail' => true,
                    'interviewContact' => [
                        'id' => $hashids->encode($user->id),
                        'email' => $user->email,
                        'countryCode' => $countryHash->encode($user->country_code),
                        'telephone' => $user->phone_number,
                    ]
                ];
            } else { // It exists
                // check if user finished the application already.
                if ($user->application->finished) {
                    $result = [
                        'finished' => true,
                        'validEmail' => true,
                        'interviewContact' => [
                            'id' => $hashids->encode($user->id),
                            'email' => $user->email,
                            'countryCode' => $countryHash->encode($user->country_code),
                            'telephone' => $user->phone_number,
                        ]
                    ];
                } else {
                    // Create an array to return the progress of the application process.
                    $result = [];
                    $result['finished'] = false;
                    $result['validEmail'] = true;
                    $result['interviewContact'] = [
                        'id' => $hashids->encode($user->id),
                        'email' => $user->email,
                        'telephone' => $user->phone_number,
                        'countryCode' => $countryHash->encode($user->country_code),
                    ];

                    // Check if user has other steps
                    // Step two
                    if ($user->personData !== null) {
                        // Create hashes for ids in this step
                        $personDataIdHash = new Hashids('assistant-person', 20);
                        $result['interviewPersonalData'] = [
                            'id' => $personDataIdHash->encode($user->personData->id),
                            'state' => $stateHash->encode($user->personData->state_id),
                            'firstnames' => $user->personData->firstnames,
                            'lastnames' => $user->personData->lastnames,
                            'gender' => $user->personData->gender,
                            'birthdate' => $user->personData->birthdate,
                            'country' => $countryHash->encode($user->personData->country)
                        ];
                    }
                    // Step three
                    if ($user->application->salary_expect_id !== null) {
                        $salaryIdHash = new Hashids('salaries-exp-in-client', 20);
                        $result['interviewSalaryExpectation'] = [
                            'salary' => $salaryIdHash->encode($user->application->salary_expect_id)
                        ];
                    }
                    // Step four
                    if (count($user->studies) > 0) {
                        $result['interviewStudies'] = [
                            'userId' => $hashids->encode($user->id),
                            'studies' => StepFourResource::collection($user->studies),
                            'languages' => UserLanguagesResource::collection($user->languages),
                        ];
                    }
                    // Step five
                    if (count($user->experiences) > 0) {
                        $result['interviewWorkExperience'] = [
                            'userId' => $hashids->encode($user->id),
                            'experiences' => StepFiveResource::collection($user->experiences),
                        ];
                    }
                    // Skills
                    if (count($user->skills) > 0) {
                        $result['interviewSkills'] = [
                            'userId' => $hashids->encode($user->id),
                            'skills' => SkillsResource::collection($user->skills),
                        ];
                    }

                    // Step six
                    if ($user->application->description !== null || $user->application->curriculum !== null) {
                        $path = $user->application->curriculum;
                        $path = public_path().'/'.$path;
                        $obj = (object)[];
                        $type = pathinfo($path, PATHINFO_EXTENSION);
                        $data = file_get_contents($path);
                        $mime = File::mimeType($path);
                        $base64 = 'data:@file/' . $type . ';base64,' . base64_encode($data);
                        $uploadedFile = base64_encode(storage_path($user->application->curriculum));
                        $obj->name = substr($user->application->curriculum, strrpos($user->application->curriculum,'/')+1);
                        $obj->value = $base64;
                        $obj->type = mime_content_type($user->application->curriculum);
                        $result['interviewExtraData'] = [
                            'cv' => $obj,
                            'description' => $user->application->description,
                        ];
                    }
                }
            }
            return $result;
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    public function updateUserTrait($arr, $id) {
        $req = (object) $arr;
        try {
            // Decode hash
            $hashids = new Hashids('assistant-user', 20);
            $decoded = $hashids->decode($id);
            if (count($decoded) > 0) {

                $user = User::find($decoded[0]);
                // Compare user email with request email
                if ($user->email !== $req->email) {

                    // Check if email exists
                    $userEmail = User::where([['email', $req->email], ['user_type_id', 3]])->first();
                    if ($userEmail !== null) {
                        return [
                            'takenEmail' => true,
                        ];
                    }
                }

                if (!getenv('API_TESTING')) {
                    $verified = $this->verify($req->email);

                    if($verified !== 'valid') {
                        return [
                            'notValidEmail' => true,
                        ];
                    }
                }

                $countryHash = new Hashids('countries-talents-assistant', 20);
                $user->email = $req->email;
                if ($req->countryCode) {
                    $countryDecoded = $countryHash->decode($req->countryCode);
                    if (count($countryDecoded) > 0) {
                        $user->country_code = $countryDecoded[0];
                        $user->phone_number = $req->telephone;
                    } else {
                        return ['error' => 'Revise los datos enviados'];
                    }
                }
                $user->updated_at = date('Y-m-d H:i:s');
                $user->save();
                return [
                    'finished' => false,
                    'interviewContact' => [
                        'id' => $hashids->encode($user->id),
                        'email' => $user->email,
                        'countryCode' => $countryHash->encode($user->country_code),
                        'telephone' => $user->phone_number,
                    ]
                ];
            } else {
                return [
                    'error' => 'No se ha encontrado el usuario',
                ];
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
