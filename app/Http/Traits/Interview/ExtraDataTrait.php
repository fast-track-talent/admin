<?php

namespace App\Http\Traits\Interview;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Models\Application;
use App\Models\User;
use Hashids\Hashids;
use App\Http\Traits\ToolsTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

trait ExtraDataTrait {

    use ToolsTrait;
    
    public function syncExtrasTrait($req, $id)
    {
        try {
            $userIdHash = new Hashids('assistant-user', 20);
            $decodedUserId = $userIdHash->decode($id);
            
            if (count($decodedUserId) > 0) {
                $user = User::with('application')->find($decodedUserId[0]);
                $application = [];
                $extraDataForm = new Request([
                    'description'   => $req->desc,
                    'reputation' => $req->calification,
                    'file' =>$req->file
                ]);
                
                $rules = [
                    'file' => 'required|file|max:1024|mimes:txt,doc,docx,pdf',
                    'description' => 'nullable',
                    'reputation' => 'nullable'
                ];

                $messages = [
                    'uploaded' => 'El curriculum debe pesar hasta 1mb.',
                    'mimes' => 'El curriculum debe ser de tipo DOC, DOCX, PDF o TXT.',
                    'required' => 'El curriculum es obligatorio.',
                ];

                $validator = Validator::make($extraDataForm->all(), $rules, $messages);

                if($validator->fails()){
                    return response()->json($validator->errors(),400);
                }
                
                if(isset($req->desc)) {
                    $application['description'] = trim($req->desc);
                }
                
                if(isset($req->calification)) {
                    $application['reputation'] = $req->calification;
                }
                
                $file = $req->file;
                $ext = $file->extension();
                $path = 'storage/users/'.$decodedUserId[0];
                    
                if (File::exists(public_path($user->application->curriculum))) {
                    File::delete($user->application->curriculum);          
                }

                if ($file->move(public_path($path), "cv.$ext")) {
                    $application['curriculum'] = "$path/cv.$ext";
                }

                $user->application()->update($application);
            } else {
                return ['error' => 'No se ha encontrado el usuario'];
            }

            $result = [
                'userId' => $id,
                'userExtras' => $application
            ];
            return $result;
            
        } catch (\Throwable $th) {
            if (array_key_exists('curriculum', $application)) {
                File::delete($application->curriculum);
            }
            throw $th;
        }
    }
}