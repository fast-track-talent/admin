<?php

namespace App\Http\Traits\Interview;
use Hashids\Hashids;
use Illuminate\Support\Facades\DB;
use App\Models\Person;

trait PersonalDataTrait {

    public function registerPersonDataTrait($req)
    {
        try {
            DB::beginTransaction();
            $person = new Person;
            $userIdHash = new Hashids('assistant-user', 20);
            $decodedUserId = $userIdHash->decode($req->userId);
            $countryHash = new Hashids('countries-talents-assistant', 20);
            $countryDecoded = $countryHash->decode($req->countryCode);
            
            if (count($decodedUserId) > 0) {
                if (count($countryDecoded) > 0) {                    
                    if ($req->state !== null) {
                        $stateIdHash = new Hashids('states-in-client', 20);
                        $decodedStateId = $stateIdHash->decode($req->state);
                        if (count($decodedStateId) === 0) {
                            return [
                                'validState' => false
                            ];
                        }
                        $person->state_id = $decodedStateId[0];
                        $stateId = $stateIdHash->encode($person->state_id);
                    } else {
                        $stateId = '';
                    }
                    $person->user_id = $decodedUserId[0];
                    $person->firstnames = $req->firstnames;
                    $person->lastnames = $req->lastnames;
                    $person->gender = $req->gender;
                    $person->birthdate = $req->birthdate;
                    $person->country = $countryDecoded[0];
                    $person->updated_at = null;
                    $person->save();
                    DB::commit();
                    $hashids = new Hashids('assistant-person', 20);
                    $result = [
                        'id' => $hashids->encode($person->id),
                        'state' => $stateId,
                        'firstnames' => $person->firstnames,
                        'lastnames' => $person->lastnames,
                        'gender' => $person->gender,
                        'birthdate' => $person->birthdate,
                        'country' => $countryHash->encode($person->country)
                    ];
                } else {
                    return [
                        'validCountry' => false
                    ];
                }
            } else {
                return [
                    'validUser' => false
                ];
            }

            return $result;
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    public function updatePersonDataTrait($req, $id)
    {
        try {
            DB::beginTransaction();
            // Decode person id hash
            $hashids = new Hashids('assistant-person', 20);
            $decoded = $hashids->decode($id);
            $countryHash = new Hashids('countries-talents-assistant', 20);
            $countryDecoded = $countryHash->decode($req->country);
            if (count($decoded) > 0) {
                if (count($countryDecoded) > 0) {     
                    $person = Person::find($decoded[0]);
                    // Decode state id hash
                    if ($req->state !== null) {
                        $stateIdHash = new Hashids('states-in-client', 20);
                        $decodedStateId = $stateIdHash->decode($req->state);
                        if (count($decodedStateId) === 0) {
                            return [
                                'validState' => false
                            ];
                        }
                        $person->state_id = $decodedStateId[0];
                        $stateId = $stateIdHash->encode($person->state_id);
                    } else {
                        $stateId = '';
                    }
                    $person->firstnames = $req->firstnames;
                    $person->lastnames = $req->lastnames;
                    $person->gender = $req->gender;
                    $person->birthdate = $req->birthdate;
                    $person->country = $countryDecoded[0];
                    $person->updated_at = null;
                    $person->save();
                    DB::commit();
                    $result = [
                        'id' => $hashids->encode($person->id),
                        'state' => $stateId,
                        'firstnames' => $person->firstnames,
                        'lastnames' => $person->lastnames,
                        'gender' => $person->gender,
                        'birthdate' => $person->birthdate,
                        'country' => $countryHash->encode($person->country)
                    ];
                } else {
                    return [
                        'validCountry' => false
                    ];
                }
            } else {
                return [
                    'validUser' => false
                ];
            }

            return $result;
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }


}