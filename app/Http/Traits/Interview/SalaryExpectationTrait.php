<?php

namespace App\Http\Traits\Interview;
use Hashids\Hashids;
use App\Models\Application;

trait SalaryExpectationTrait {
    
    static function saveApplicationSalaryTrait($req, $id)
    {
        try {
            // Decode user id
            $userIdHash = new Hashids('assistant-user', 20);
            $decodedUserId = $userIdHash->decode($id);
            if (count($decodedUserId) > 0) {
                // Decode salary range id
                $salaryHash = new Hashids('salaries-exp-in-client', 20);
                $decodedSalary = $salaryHash->decode($req->salary);

                if (count($decodedSalary) > 0) {
                    // Get user application in DB
                    $application = Application::where('user_id', $decodedUserId[0])->first();
                    $application->salary_expect_id = $decodedSalary[0];
                    $application->save();
                    $result = [
                        'salary' => $salaryHash->encode($application->salary_expect_id)
                    ];
                } else {
                    return [
                        'error' => 'No se ha encontrado la expectativa salarial'
                    ];
                }
            } else {
                return [
                    'error' => 'No se ha encontrado el usuario'
                ];
            }

            return $result;
            
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}