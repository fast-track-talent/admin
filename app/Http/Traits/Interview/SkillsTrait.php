<?php

namespace App\Http\Traits\Interview;

use App\Http\Resources\Interview\ClientPositionRequestsStudiesResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Resources\Interview\UserSkillsResource;
use App\Http\Resources\skillsResource;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\UserSkill;
use App\Models\Client;
use App\Models\Skill;
use Hashids\Hashids;

trait SkillsTrait {

    public function saveUserSkillsTrait($req) {
        try {
            DB::beginTransaction();
            $hashids = new Hashids('assistant-user', 20);
            $skillHash = new Hashids('assistant-company', 20);
            $userReqs = $req;
            $skillsInsertArr = [];
            $resultArr = [];
            $userId = $req->userId;

            // Check if the  user exists...
            if (!$decodedUser = $hashids->decode($userId)) throw new ModelNotFoundException;
            $user = User::findOrFail($decodedUser[0]);

            // If there are skills to add...
            if ($req->skillsToAdd) {
                $skills = $userReqs->skillsToAdd;
                for ($j = 0; $j < count($skills); $j++) {
                    // Check if the skill exists...
                    if (!$decodedSkill = $skillHash->decode($skills[$j])) throw new ModelNotFoundException;
                    
                    $userSkill = UserSkill::where('user_id', '=', $decodedUser[0] , 'and')->where('skill_id', '=', $decodedSkill[0])->first();
                    if(!$userSkill){
                        // Create the user skill record...
                        UserSkill::create(['user_id' => $decodedUser[0], 'skill_id' => $decodedSkill[0], 'created_at' => date('Y-m-d H:i:s')]);
                    }
                }

  
            }

            // If there are skills to delete...
            if ($req->skillsToDelete) {
                $skillsDeleteArr = [];
                $skills = $userReqs->skillsToDelete;
                for ($j = 0; $j < count($skills); $j++) {

                // Check if the skill exists
                    $decodedUserSkill = $skillHash->decode($skills[$j]);
                    $userSkill = UserSkill::where('user_id', '=', $decodedUser[0] , 'and')->where('skill_id', '=', $decodedUserSkill[0])->first();
                    if(!$userSkill) throw new ModelNotFoundException;

                    array_push($skillsDeleteArr, $decodedUserSkill[0]);
                }
                UserSkill::whereIn('skill_id', $skillsDeleteArr)->delete();
            }
            $user = user::with(['skills'])->find($decodedUser[0]);
            $skills = $user->skills; 
            $skillsArr= [];

            foreach($skills as $key=> $skill){
                $aux = (object)[];
                $aux->skill = $skill->skill;
                $aux->id = $skillHash->encode($skill->pivot->skill_id);   
                $skillsArr[] = $aux;
            }
            //array_push($resultArr, new UserSkillsResource(user::with(['skills'])->find($decodedUser[0])));
            
            // $skillsArr = skillsResource::collection($skillsArr);

            DB::commit();
            return [
                'error' => false,
                'interviewSkills' => $skillsArr
            ];            
        } catch (ModelNotFoundException $ex) {
            DB::rollback();
            return [
                'error' => true,
                'message' => 'Revise los datos enviados'
            ];
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }
}