<?php

namespace App\Http\Traits\Interview;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Http\Resources\Interview\StepFourResource;
use App\Http\Resources\Interview\UserLanguagesResource;
use App\Models\UserStudy;
use App\Models\UserLang;
use Hashids\Hashids;

trait StudiesTrait {
    
    static function saveStudiesTrait($req)
    {
        try {
            DB::beginTransaction();
            // Create an array of inserts for the user studies
            $studies = [];
            $languages = [];
            $studiesReq = $req->studies;
            $languagesReq = $req->languages;
            $userIdHash = new Hashids('assistant-user', 20);
            $hashids = new Hashids('step-four-data-in-client', 20);
            $countryHash = new Hashids('countries-talents-assistant', 20);
            $decodedUserId = $userIdHash->decode($req->userId);
            if (count($decodedUserId) > 0) {

                if (count($studiesReq) > 0) {
                    // Check if all study types and degrees are in DB...
                    if (checkStudyTypesAndDegrees($studiesReq)) {
                        for ($i = 0; $i < count($studiesReq); $i++) {
                            // Decode country id
                            $countryDecoded = $countryHash->decode($studiesReq[$i]['country']);
                            // Decode study type id
                            $decodedStudyType = $hashids->decode($studiesReq[$i]['studyType']);
                            // Decode degree
                            $decodedDegree = $hashids->decode($studiesReq[$i]['degree']);
                            if ($studiesReq[$i]['toDelete']) {
                                $userStudyId = $hashids->decode($studiesReq[$i]['id']);
                                $userStudy = UserStudy::find($userStudyId[0]);
                                if ($userStudy) {
                                    $userStudy->delete();
                                } else {
                                    DB::rollBack();
                                    return ['error' => 'Revise los datos enviados'];
                                }
                            } else {
                                if ($studiesReq[$i]['id'] !== null) {
                                    $userStudyId = $hashids->decode($studiesReq[$i]['id']);
                                    $userStudy = UserStudy::find($userStudyId[0]);
                                    if ($userStudy) {
                                        $userStudy->study_type_id = $decodedStudyType[0];
                                        $userStudy->degree_id = $decodedDegree[0];
                                        $userStudy->country = $countryDecoded[0];
                                        $userStudy->save();
                                    } else {
                                        DB::rollBack();
                                        return ['error' => 'Revise los datos enviados'];
                                    }
                                } else {
                                    $userStudy = new UserStudy;
                                    $userStudy->user_id = $decodedUserId[0];
                                    $userStudy->study_type_id = $decodedStudyType[0];
                                    $userStudy->degree_id = $decodedDegree[0];
                                    $userStudy->country = $countryDecoded[0];
                                    $userStudy->save();
                                    $studiesReq[$i]['id'] = $hashids->encode($userStudy->id);
                                }
                            }
            
                            array_push($studies, $studiesReq[$i]);
                        }
                    } else {
                        return ['error' => 'Revise los datos enviados'];
                    }
                }

                if (count($languagesReq) > 0) {
                    // Check if all study types and degrees are in DB...
                    if (checkLanguages($languagesReq)) {
                        for ($i = 0; $i < count($languagesReq); $i++) {
                            // Decode language id in DB
                            $decoded = $hashids->decode($languagesReq[$i]['langId']);
                            $decodedLevel = $hashids->decode($languagesReq[$i]['levelId']);
                            if ($languagesReq[$i]['toDelete']) {
                                $userLangId = $hashids->decode($languagesReq[$i]['id']);
                                $userLanguage = UserLang::find($userLangId[0]);
                                if ($userLanguage) {
                                    $userLanguage->delete();
                                } else {
                                    DB::rollBack();
                                    return ['error' => 'Revise los datos enviados'];
                                }
                            } else {
                                if ($languagesReq[$i]['id'] !== null) {
                                    $userLangId = $hashids->decode($languagesReq[$i]['id']);
                                    $userLanguage = UserLang::findOrFail($userLangId[0]);
                                    $userLanguage->lang_id = $decoded[0];
                                    $userLanguage->level_id = $decodedLevel[0];
                                    $userLanguage->save();
                                } else {
                                    $userLanguage = new UserLang;
                                    $userLanguage->user_id = $decodedUserId[0];
                                    $userLanguage->lang_id = $decoded[0];
                                    $userLanguage->level_id = $decodedLevel[0];
                                    $userLanguage->save();
                                    $languagesReq[$i]['id'] = $hashids->encode($userLanguage->id);
                                }
                            }
            
                            array_push($languages, $languagesReq[$i]);
                        }
                    } else {
                        return ['error' => 'Revise los datos enviados'];
                    }
                }

                DB::commit();
                $user = User::find($decodedUserId[0]);
                $result = [
                    'userId' => $req->userId,
                    'userStudies' => StepFourResource::collection($user->studies),
                    'userLanguages' => UserLanguagesResource::collection($user->languages),
                ];
            } else {
                return ['error' => 'No se ha encontrado el usuario'];
            }
            return $result;
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}

function checkStudyTypesAndDegrees($studies) {
    $hashids = new Hashids('step-four-data-in-client', 20);
    $countryHash = new Hashids('countries-talents-assistant', 20);
    $counter = 0;
    for ($i = 0; $i < count($studies); $i++) {
        $countryDecoded = $countryHash->decode($studies[$i]['country']);
        $decodedStudyType = $hashids->decode($studies[$i]['studyType']);
        $decodedDegree = $hashids->decode($studies[$i]['degree']);
        if (count($decodedStudyType) === 0 || count($decodedDegree) === 0 || count($countryDecoded) === 0) {
            $counter++;
        }
    }

    return ($counter === 0) ? true : false;
}

function checkLanguages($languages) {

    $hashids = new Hashids('step-four-data-in-client', 20);
    $counter = 0;
    for ($i = 0; $i < count($languages); $i++) {
        $decoded = $hashids->decode($languages[$i]['langId']);
        $decodedLevel = $hashids->decode($languages[$i]['levelId']);
        if (count($decoded) === 0 || count($decodedLevel) === 0) {
            $counter++;
        }
    }

    return ($counter === 0) ? true : false;
}