<?php

namespace App\Http\Traits\Interview;

use Illuminate\Support\Facades\DB;
use App\Http\Resources\Interview\StepFiveResource;
use App\Models\Experience;
use App\Models\User;
use App\Models\CompanyActivity;
use App\Models\Position;
use Hashids\Hashids;

trait WorkExperienceTrait {
    
    public function syncExperiencesTrait($req)
    {
        try {
            // Save the user id to decode.
            $userIdHash = new Hashids('assistant-user', 20);
            // Decode the user id.
            $decodedUserId = $userIdHash->decode($req->userId);
            if (count($decodedUserId) > 0) {
                DB::beginTransaction();
                // Save the experiences comming from request.
                $experiencesInReq = $req->experiences;
                // Check if ids in experiences are correct.
                if (checkInformation($experiencesInReq)) {
                    // Decode experience id in BD
                    $hashids = new Hashids('step-five-data-in-client', 20);
                    // Decode position id in BD
                    $positionsHash = new Hashids('positions-in-client', 20);
                    for ($i = 0; $i < count($experiencesInReq); $i++) {
                        // Save the assoc array.
                        $exp = $experiencesInReq[$i];
                        // Check if experience needs to be deleted
                        if ($exp['toDelete']) {
                            $decodedExpId = $hashids->decode($exp['experienceId']);
                            $experience = Experience::find($decodedExpId[0]);
                            if ($experience) {
                                $experience->delete();
                            } else {
                                DB::rollBack();
                                return ['error' => 'Revise los datos enviados'];
                            }
                        } else {
                            if ($exp['experienceId'] !== null) {
                                $decodedExpId = $hashids->decode($exp['experienceId']);
                                if (count($decodedExpId) > 0) {
                                    $experience = Experience::find($decodedExpId[0]);
                                } else {
                                    DB::rollBack();
                                    return ['error' => 'Revise los datos enviados'];
                                }
                            } else {
                                $experience = new Experience;
                                $experience->user_id = $decodedUserId[0];
                            }
                            // Decode the activity id from request for this exp.
                            $decodedCompanyActivity = $hashids->decode($exp['companyActivityId']);
                            $decodesPosition = $positionsHash->decode($exp['position']);
                            $experience->company_activity_id = $decodedCompanyActivity[0];
                            $experience->company = $exp['company'];
                            $experience->position = $decodesPosition[0];
                            $experience->dependents = $exp['dependents'];
                            $experience->from = $exp['from'];
                            $experience->to = $exp['to'];
                            $experience->actual = $exp['actual'];
                            $experience->updated_at = date('Y-m-d H:i:s');
                            $experience->save();
                        }
                    }
                } else {
                    return ['error' => 'Revise los datos enviados'];
                }
            } else {
                return ['error' => 'No se ha encontrado el usuario'];
            }
            
            DB::commit();
            $user = User::find($decodedUserId[0]);
            $result = [
                'userId' => $req->userId,
                'userExperiences' => StepFiveResource::collection($user->experiences),
            ];

            return $result;
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}

function checkInformation($experiences) {
    $hashids = new Hashids('step-five-data-in-client', 20);
    $positionsHash = new Hashids('positions-in-client', 20);
    $counter = 0;
    for ($i = 0; $i < count($experiences); $i++) {
        $decoded = $hashids->decode($experiences[$i]['companyActivityId']);
        $decodedPosition = $positionsHash->decode($experiences[$i]['position']);
        if (count($decoded) === 0 || count($decodedPosition) === 0) $counter++;
    }

    return ($counter === 0) ? true : false;
}