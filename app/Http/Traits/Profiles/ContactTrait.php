<?php

namespace App\Http\Traits\Profiles;

use App\Http\Resources\Profiles\PositionRequestsStepOneResource;
use Illuminate\Support\Facades\DB;
use App\Models\PositionRequest;
use App\Models\Person;
use App\Models\Client;
use App\Models\User;
use Hashids\Hashids;

trait ContactTrait {

    public function saveClientContactTrait($req) {
        try {
            DB::beginTransaction();
            $hashids = new Hashids('assistant-company', 20);
            $countryHash = new Hashids('countries-talents-assistant', 20);
            $decodedClient = $hashids->decode($req->clientId);
            
            if(count($decodedClient) > 0) {
                // Find the client to create its contact after creating the user model.
                $client = Client::find($decodedClient[0]);
                // Create the contact for this client.
                // First we check if the user id is comming in the request.
                if ($req->userId) {
                    $decodedUser = $hashids->decode($req->userId);
                    if (count($decodedUser) === 0) {
                        DB::rollBack();
                        return ['error' => 'Revise los datos enviados'];
                    }
                    $user = User::find($decodedUser[0]);     
                } else {
                    // Register user
                    $user = new User;
                    $user->user_type_id = 2;
                }
                $user->email = $req->email;
                if ($req->countryCode) {
                    $decodedCountry = $countryHash->decode($req->countryCode);
                    if (count($decodedCountry) > 0) {
                        $user->country_code = $decodedCountry[0];
                        $user->phone_number = $req->telephone;
                    } else {
                        DB::rollBack();
                        return ['error' => 'Revise los datos enviados'];
                    }
                }
                $user->updated_at = null;
                $user->save();
                $user->refresh();
                // Create person data for this user.
                // First we check if the person id is comming in the request.
                if ($req->has('personId')) {
                    $decodedPerson = $hashids->decode($req->personId);
                    if (count($decodedPerson) === 0) {
                        DB::rollBack();
                        return ['error' => 'Revise los datos enviados'];
                    }
                    $person = Person::find($decodedPerson[0]);
                } else {
                    // Register person
                    $person = new Person;
                    $person->user_id = $user->id;
                }
                $person->firstnames = $req->firstnames;
                $person->lastnames = $req->lastnames;
                $person->updated_at = null;
                $person->save();
                // Now we relate the user with the client.
                $client->contact_id = $user->id;
                $client->save();
                DB::commit();
                $result = [
                    'profilesContact' => [
                        'userId' => $hashids->encode($user->id),
                        'personId' => $hashids->encode($person->id),
                        'firstnames' => $person->firstnames,
                        'lastnames' => $person->lastnames,
                        'countryCode' => $countryHash->encode($user->country_code),
                        'telephone' => $user->phone_number,
                        'email' => $user->email,
                    ]
                ];
            } else {
                return ['error' => 'Revise los datos enviados'];
            }
            return $result;
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}