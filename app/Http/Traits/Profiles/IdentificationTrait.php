<?php

namespace App\Http\Traits\Profiles;

use App\Http\Resources\Profiles\ClientPositionRequestsLocationsResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Resources\Profiles\PositionRequestsStepOneResource;
use App\Http\Resources\Profiles\ClientPositionRequestsResource;
use App\Http\Resources\Profiles\PosReqSkillsAndLangsResource;
use App\Http\Resources\Profiles\ClientActivitiesResource;
use App\Http\Resources\Profiles\ClientProductsResource;
use App\Http\Resources\Profiles\LanguagesResource;
use App\Http\Resources\Profiles\LanguageLevelsResource;
use App\Http\Resources\Profiles\CountriesResource;
use App\Http\Resources\Profiles\StudyTypesResource;
use App\Http\Resources\SkillsResource;
use App\Http\Resources\Profiles\CompanyActivitiesResource;
use App\Http\Resources\Profiles\AgeRangesResource;
use App\Http\Resources\Profiles\ExpRangesResource;
use App\Http\Resources\Profiles\DegreesResource;
use App\Http\Resources\Profiles\PositionRequest as PositionRequestSpecial;
use App\Models\PositionRequestDegree;
use App\Models\PositionCandidateDetail;
use Illuminate\Support\Facades\DB;
use App\Models\PositionRequest;
use App\Models\LanguageLevel;
use App\Models\Client;
use App\Models\Degree;
use App\Models\Skill;
use App\Models\Language;
use App\Models\Country;
use App\Models\AgeRange;
use App\Models\ExperienceRange;
use App\Models\CompanyActivity;
use App\Models\StudyType;
use Hashids\Hashids;

trait IdentificationTrait {

    public function saveClientTrait($req) {
        try {
            // Create hashes for ids in this step
            $hashids = new Hashids('assistant-company', 20);
            DB::beginTransaction();
            // Check if client exists in db
            $client = Client::where('number_id', $req->numberId)->first();
            if ($client === null) { // It does not exist
                // Register client
                $client = new Client;
                $client->name = $req->name;
                $client->number_id = $req->numberId;
                $client->description = $req->description;
                $client->updated_at = null;
                $client->save();
                DB::commit();
                $result = [
                    'identification' => [
                        'id' => $hashids->encode($client->id),
                        'name' => $client->name,
                        'numberId' => $client->number_id,
                        'description' => $client->description
                    ]
                ];
            } else { // It exists
                // Create an array to return the progress of the application process.
                $result = [];

                // Step One
                $result['identification'] = [
                    'id' => $hashids->encode($client->id),
                    'name' => $client->name,
                    'numberId' => $client->number_id,
                    'description' => $client->description
                ];

                // Step two
                if ($client->contact !== null) {
                    $countryCode = null;
                    if ($client->contact->country_code !== null) {
                        $countryHash = new Hashids('countries-talents-assistant', 20);
                        $countryCode = $countryHash->encode($client->contact->country_code);
                    }
                    $result['profilesContact'] = [
                        'userId' => $hashids->encode($client->contact->id),
                        'personId' => $hashids->encode($client->contact->personData->id),
                        'firstnames' => $client->contact->personData->firstnames,
                        'lastnames' => $client->contact->personData->lastnames,
                        'countryCode' => $countryCode,
                        'telephone' => $client->contact->phone_number,
                        'email' => $client->contact->email,
                    ];
                }

                // Step three
                if (count($client->activities) > 0) {
                    $result['profilesIndustrySector'] = ClientActivitiesResource::collection($client->activities);
                }

                // Step four
                if (count($client->products) > 0) {
                    $result['profilesProducts'] = ClientProductsResource::collection($client->products);
                }

                // Check if client has unfinished position requests
                $unfinishedPosReqs = $client->positionRequests()->where('finished', 0)->get();
                if (count($unfinishedPosReqs) > 0) {
                    $result['unfinished'] = true;

                    
                    // Step five
                    if ($client->positionRequests !== null) {
                        $result['profilesRequests'] = ClientPositionRequestsResource::collection($client->positionRequests);
                        // Get ids of candidate details model
                    }

                    // Step six
                    $balancer = 0;
                    $posReqs = $client->positionRequests;
                    for ($i = 0; $i < count($posReqs); $i++) { 
                        if (
                            $posReqs[$i]['country'] !== null ||
                            $posReqs[$i]['country_state_id'] !== null ||
                            $posReqs[$i]['remote'] !== null ||
                            $posReqs[$i]['vehicle'] !== null ||
                            $posReqs[$i]['machine'] !== null
                        ) {
                            $balancer++;
                        }
                    }
                    if ($balancer > 0) {
                        $result['profilesLocations'] = ClientPositionRequestsLocationsResource::collection($client->positionRequests);
                    }

                    // Step seven
                    $stepFourTalentsHash = new Hashids('step-four-data-in-client', 20);
                    $posReqs = $client->positionRequests()->with(['degrees'])->where('finished', false)->get();
                    
                    $posCandDegrees = [];
                    if (count($posReqs) > 0) {
                        $stepFourTalentsHash = new Hashids('step-four-data-in-client', 20);
                        foreach ($posReqs as $pr) {
                            foreach ($pr->degrees as $prd) {
                                if ($prd->study_type_id) {
                                    $degrees = Degree::where('study_type_id', $prd->study_type_id)->get();
                                } else {
                                    $degrees = Degree::whereNull('study_type_id')->get();
                                }

                                $elem = [
                                    'degreeId' => $stepFourTalentsHash->encode($prd->degree_id),
                                    'degrees' => DegreesResource::collection($degrees),
                                    'posReqDegreeId' => $hashids->encode($prd->id),
                                    'positionRequestId' => $hashids->encode($prd->position_request_id),
                                    'studyTypeId' => $stepFourTalentsHash->encode($prd->study_type_id),
                                ];
                                array_push($posCandDegrees, $elem);
                            }
                        }
                        if (count($posCandDegrees) > 0) $result['profilesStudies'] = $posCandDegrees;
                    }

                    // Step eight
                    $posReqs = $client->positionRequests()->with(['skills', 'languages'])->where('finished', false)->get();
                    $posReqSkillsAndLangs = [];
                    if (count($posReqs) > 0) {
                        $langHash = new Hashids('step-four-data-in-client', 20);
                        foreach ($posReqs as $pr) {
                            $elem = [
                                'posReqId' => $hashids->encode($pr->id),
                                'skills' => [],
                                'langs' => [],
                            ];

                            foreach ($pr->skills as $s) {
                                $skill = [
                                    'skillId' => $hashids->encode($s->id),
                                    'skill' => $s->skill,
                                    'posSkillId' => $hashids->encode($s->pivot->id),
                                ];
                                array_push($elem['skills'], $skill);
                            }
                            foreach ($pr->languages as $l) {
                                $level = LanguageLevel::find($l->pivot->level);
                                $lang = [
                                    'langId' => $langHash->encode($l->id),
                                    'langName' => $l->lang,
                                    'levelId' => $langHash->encode($l->pivot->level),
                                    'levelName' => $level->level,
                                    'posReqLangId' => $hashids->encode($l->pivot->id),
                                ];
                                array_push($elem['langs'], $lang);
                            }
                            if (count($elem['skills']) > 0 || count($elem['langs']) > 0) array_push($posReqSkillsAndLangs, $elem);
                        }
                        if (count($posReqSkillsAndLangs) > 0) $result['profilesSkills'] = $posReqSkillsAndLangs;
                    }

                    // Step nine
                    $posReqs = $client->positionRequests()->with(['candidateDetails'])->where('finished', false)->get();
                    $posReqCandDetails = [];
                    if (checkIfCandDetailsExist($posReqs)) {
                        $langHash = new Hashids('step-four-data-in-client', 20);
                        foreach ($posReqs as $pr) {
                            $elem = [
                                'ageRangeId' => $hashids->encode($pr->candidateDetails->age_range_id),
                                'expRangeId' => $hashids->encode($pr->candidateDetails->experience_range_id),
                                'posReqCandDetId' => $hashids->encode($pr->candidateDetails->id),
                                'positionRequestId' => $hashids->encode($pr->candidateDetails->position_request_id),
                            ];
                            array_push($posReqCandDetails, $elem);
                        }
                        $result['profilesYearsOfExperience'] = $posReqCandDetails;
                        $result['skillsArr'] = SkillsResource::collection(Skill::all());
                        $result['languagesArr'] = LanguagesResource::collection(Language::all());
                        $result['languageLevelsArr'] = LanguageLevelsResource::collection(LanguageLevel::all());
                        $result['countriesArr'] = CountriesResource::collection(Country::all());
                        $result['ageRangesArr'] = AgeRangesResource::collection(AgeRange::all());
                        $result['expRangesArr'] = ExpRangesResource::collection(ExperienceRange::all());
                        $result['companyActivitiesArr'] = CompanyActivitiesResource::collection(CompanyActivity::all());
                        $result['studyTypesArr'] = StudyTypesResource::collection(StudyType::all());
                        $result['degreesArr'] = DegreesResource::collection(Degree::all());
                    }
                    
                }
                // } else $result['unfinished'] = false;
            }
            return $result;
        } catch (ModelNotFoundException $ex) {
            DB::rollback();
            return [
                'error' => true,
                'message' => 'Revise los datos enviados'
            ];
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    public function updateClientTrait($req, $id) {
        try {
            // Create hashes for ids in this step
            $hashids = new Hashids('assistant-company', 20);
            $clientId = $hashids->decode($id);
            if (count($clientId) > 0) {

                DB::beginTransaction();
                // Check if client exists in db
                $client = Client::findOrFail($clientId[0]);
                if ($client !== null) { // It does exist
                    $duplicatedClient = Client::where('number_id', $req->numberId)->first();
                    if ($duplicatedClient !== null) {
                        return [
                            'rifExists' => true
                        ];
                    } else {
                        // update client
                        $client->name = $req->name;
                        $client->number_id = $req->numberId;
                        $client->description = $req->description;
                        $client->updated_at = date('Y-m-d H:i:s');
                        $client->save();
                        DB::commit();
                        return $result = [
                            'identification' => [
                                'id' => $hashids->encode($client->id),
                                'name' => $client->name,
                                'numberId' => $client->number_id,
                                'description' => $client->description
                            ]
                        ];
                    }
                }
            } else {
                return [
                    'error' => true,
                    'message' => 'Revise los datos enviados'
                ];
            }
        } catch (ModelNotFoundException $ex) {
            DB::rollback();
            return [
                'error' => true,
                'message' => 'Revise los datos enviados'
            ];
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }
}

function checkIfCandDetailsExist($posReqs) {
    $flag = false;
    foreach ($posReqs as $pr) {
        if ($pr->candidateDetails !== null) {
            $flag = true;
        }
    }
    return $flag;
}