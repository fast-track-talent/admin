<?php

namespace App\Http\Traits\Profiles;

use App\Http\Resources\Profiles\ClientActivitiesResource;
use Illuminate\Support\Facades\DB;
use App\Models\ClientsActivity;
use App\Models\Client;
use Hashids\Hashids;

trait IndustrySectorTrait {

    public function saveClientActivitiesTrait($req) {
        try {
            DB::beginTransaction();
            $hashids = new Hashids('assistant-company', 20);
            $activitiesHash = new Hashids('step-five-data-in-client', 20);
            $activities = $req->activities;
            $decodedClient = $hashids->decode($req->clientId);

            if(count($decodedClient) > 0) {
                $client = Client::find($decodedClient[0]);
                // Check if all company activities are in DB...
                if (checkActivities($activities)) {
		  if (!checkDuplicated($activities)) {
                       for ($i = 0; $i < count($activities); $i++) {
                            // Decode activity id
                            $decodedActivity = $activitiesHash->decode($activities[$i]['activityId']);
                            // Decode client activity id if it comes in the request
                            if ($activities[$i]['id'] !== null) {
                                $clientActivityId = $hashids->decode($activities[$i]['id']);
                                if (count($clientActivityId) > 0) {
                                    // Find the client activity model
                                    $clientActivity = ClientsActivity::find($clientActivityId[0]);
                                    // If delete flag is true
                                    if ($activities[$i]['toDelete']) {
                                        $clientActivity->delete();
                                    } else { // Update the model
                                        $clientActivity->activity_id = $decodedActivity[0];
                                        $clientActivity->save();
                                    }
                                } else {
                                    DB::rollBack();
                                    return ['error' => 'Revise los datos enviados'];
                                }
                            } else { // Create the new model
                                $clientActivity = new ClientsActivity;
                                $clientActivity->client_id = $decodedClient[0];
                                $clientActivity->activity_id = $decodedActivity[0];
                                $clientActivity->save();
                            }
                        }

                        DB::commit();
                        $client->refresh();
                        $result = [
                            'profilesIndustrySector' => ClientActivitiesResource::collection($client->activities),
                        ];
                    } else {
                        return ['duplicated' => true];
                    }
                } else {
                    return ['error' => 'Revise los datos enviados'];
                }
            } else {
                return ['error' => 'No se encontró el cliente'];
            }
            return $result;
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}

function checkActivities($activities) {
    $hashids = new Hashids('step-five-data-in-client', 20);
    $counter = 0;
    for ($i = 0; $i < count($activities); $i++) {
        $decoded = $hashids->decode($activities[$i]['activityId']);
        if (count($decoded) === 0) $counter++;
    }

    return ($counter === 0) ? true : false;
}

function checkDuplicated($activities) {
    for ($i = 0; $i < count($activities); $i++) { 
        for ($j = 0; $j < count($activities); $j++) {
            $toDelete = (bool) $activities[$j]['toDelete'];
            if ($i !== $j) {
                if ($activities[$i]['activityId'] === $activities[$j]['activityId'] && !$toDelete) {
                    return (bool) true;
                }
            }
        }
    }
    return (bool) false;
}
