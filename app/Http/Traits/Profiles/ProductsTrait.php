<?php

namespace App\Http\Traits\Profiles;

use App\Models\Client;
use App\Models\ClientsProduct;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\Profiles\ClientProductsResource;
use Hashids\Hashids;

trait ProductsTrait {

    public function saveClientProductsTrait($req) {
        try {
            DB::beginTransaction();
            $hashids = new Hashids('assistant-company', 20);
            $productsHash = new Hashids('step-four-data-in-client-asssist', 20);
            $products = $req->products;
            $decodedClient = $hashids->decode($req->clientId);

            if(count($decodedClient) > 0) {
                $client = Client::find($decodedClient[0]);
                if (!checkDuplicatedNames($products)) {
                    for ($i = 0; $i < count($products); $i++) {
                        // Decode client activity id if it comes in the request
                        if ($products[$i]['productId'] !== null) {
                            // Decode activity id
                            $decodedActivity = $productsHash->decode($products[$i]['productId']);
                            $clientProductId = $hashids->decode($products[$i]['productId']);
                            if (count($clientProductId) > 0) {
                                // Find the client activity model
                                $clientProduct = ClientsProduct::find($clientProductId[0]);
                                // If delete flag is true
                                if ($products[$i]['toDelete']) {
                                    $clientProduct->delete();
                                } else { // Update the model
                                    $clientProduct->name = $products[$i]['name'];
                                    $clientProduct->description = $products[$i]['description'];
                                    $clientProduct->save();
                                }
                            } else {
                                DB::rollBack();
                                return ['error' => 'Revise los datos enviados'];
                            }
                        } else { // Create the new model
                            if ($products[$i]['name'] !== null) {
                                $clientProduct = new ClientsProduct;
                                $clientProduct->client_id = $decodedClient[0];
                                $clientProduct->name = $products[$i]['name'];
                                $clientProduct->description = $products[$i]['description'];
                                $clientProduct->save();
                            }
                        }
                    }
    
                    DB::commit();
                    $client->refresh();
                    $result = [
                        'profilesProducts' => ClientProductsResource::collection($client->products),
                    ];
                } else {
                    return ['duplicatedNames' => true];
                }
            } else {
                return ['error' => 'No se encontró el cliente'];
            }
            return $result;
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}

function checkDuplicatedNames($products) {
    for ($i = 0; $i < count($products); $i++) { 
        for ($j = 0; $j < count($products); $j++) {
            $toDelete = (bool) $products[$j]['toDelete'];
            if ($i !== $j) {
                if ($products[$i]['name'] === $products[$j]['name'] && !$toDelete) {
                    return (bool) true;
                }
            }
        }
    }
    return (bool) false;
}
