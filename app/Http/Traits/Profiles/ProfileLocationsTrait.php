<?php

namespace App\Http\Traits\Profiles;

use App\Models\Client;
use App\Models\PositionRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\Profiles\ClientPositionRequestsLocationsResource;
use Hashids\Hashids;

trait ProfileLocationsTrait {

    public function savePositionRequestsLocationsTrait($req) {
        try {
            DB::beginTransaction();
            $hashids = new Hashids('assistant-company', 20);
            $countryHash = new Hashids('countries-talents-assistant', 20);
            $stateIdHash = new Hashids('states-in-client', 20);
            $positionRequestsLocations = $req->positionRequestsLocations;
            $decodedClient = $hashids->decode($req->clientId);

            // If the client id doesn't exist
            if (count($decodedClient) === 0) return ['error' => 'No se encontró el cliente'];
            $client = Client::find($decodedClient[0]);
            
            // We pass through all the position requests
            for ($i = 0; $i < count($positionRequestsLocations); $i++) {

                // Decode country id
                if ($positionRequestsLocations[$i]['countryId'] !== null) {
                    $decodedCountryData = $countryHash->decode($positionRequestsLocations[$i]['countryId']);
                    if (count($decodedCountryData) === 0) return ['error' => 'No se encontró el país'];
                    $decodedCountry = $decodedCountryData[0];
                } else $decodedCountry = null;
                
                // Decode country state id
                if ($positionRequestsLocations[$i]['countryStateId'] !== null) {
                    $decodedStateData = $stateIdHash->decode($positionRequestsLocations[$i]['countryStateId']);
                    if (count($decodedStateData) === 0) return ['error' => 'No se encontró el estado'];
                    $decodedState = $decodedStateData[0];
                } else $decodedState = null;

                // Decode client position request id
                $positionRequestId = $hashids->decode($positionRequestsLocations[$i]['positionRequestId']);
                
                // If the client id doesn't exist
                if (count($positionRequestId) === 0) return ['error' => 'No se encontró la petición de cargo'];
                                    
                // Find the position request model
                $positionRequest = PositionRequest::find($positionRequestId[0]);

                $positionRequest->country = $decodedCountry;
                $positionRequest->country_state_id = $decodedState;
                $positionRequest->remote = $positionRequestsLocations[$i]['remote'];
                $positionRequest->vehicle = $positionRequestsLocations[$i]['vehicle'];
                $positionRequest->machine = $positionRequestsLocations[$i]['machine'];
                $positionRequest->save();
            }

            DB::commit();
            $client->refresh();
            $result = [
                'profilesLocations' => ClientPositionRequestsLocationsResource::collection($client->positionRequests),
            ];
            return $result;
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}