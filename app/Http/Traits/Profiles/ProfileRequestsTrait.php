<?php

namespace App\Http\Traits\Profiles;

use App\Models\Client;
use App\Models\PositionRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\Profiles\ClientPositionRequestsResource;
use Hashids\Hashids;

trait ProfileRequestsTrait {

    public function savePositionRequestsTrait($req) {
        try {
            DB::beginTransaction();
            $hashids = new Hashids('assistant-company', 20);
            $positionsHash = new Hashids('positions-in-client', 20);
            $salariesHash = new Hashids('salaries-exp-in-client', 20);
            $positionRequests = $req->positionRequests;
            $decodedClient = $hashids->decode($req->clientId);

            // If the client id doesn't exist
            if (count($decodedClient) === 0) return ['error' => 'No se encontró el cliente'];
            
            $client = Client::find($decodedClient[0]);
            
            // We pass through all the position requests
            for ($i = 0; $i < count($positionRequests); $i++) {

                // Decode position id
                $decodedPosition = $positionsHash->decode($positionRequests[$i]['positionId']);
                if (count($decodedPosition) === 0) return ['error' => 'No se encontró la posición'];

                // Decode salary id
                $decodedSalary = $salariesHash->decode($positionRequests[$i]['salaryExpectationId']);
                if (count($decodedSalary) === 0) return ['error' => 'No se encontró la expectativa salarial'];

                // Decode client activity id if it comes in the request
                if ($positionRequests[$i]['positionRequestId'] !== null) {
                    $positionRequestId = $hashids->decode($positionRequests[$i]['positionRequestId']);
                    
                    // If the client id doesn't exist
                    if (count($positionRequestId) === 0) return ['error' => 'No se encontró la petición de cargo'];
                                        
                    // Find the position request model
                    $positionRequest = PositionRequest::find($positionRequestId[0]);
                    
                    // If delete flag is true
                    if ($positionRequests[$i]['toDelete']) {
                        $positionRequest->delete();
                        continue;
                    }
                } else { // Create the new model
                    $positionRequest = new PositionRequest;
                    $positionRequest->client_id = $decodedClient[0];
                }

                $positionRequest->position_id = $decodedPosition[0];
                $positionRequest->salary_expectation_id = $decodedSalary[0];
                $positionRequest->title = $positionRequests[$i]['title'];
                $positionRequest->goals = $positionRequests[$i]['goals'];
                $positionRequest->report_to = $positionRequests[$i]['reportTo'];
                $positionRequest->supervise_to = $positionRequests[$i]['superviseTo'];
                $positionRequest->confidential = $positionRequests[$i]['confidential'];
                $positionRequest->save();
            }

            DB::commit();
            $client->refresh();
            $result = [
                'profilesRequests' => ClientPositionRequestsResource::collection($client->positionRequests->where('finished', 0)),
            ];
            return $result;
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}