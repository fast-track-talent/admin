<?php

namespace App\Http\Traits\Profiles;

use App\Http\Resources\ClientPositionRequestsStudiesResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Resources\Profiles\PosReqSkillsAndLangsResource;
use App\Models\PositionRequestLanguage;
use Illuminate\Support\Facades\DB;
use App\Models\PositionRequest;
use App\Models\LanguageLevel;
use App\Models\PositionSkill;
use App\Models\Language;
use App\Models\Client;
use App\Models\Skill;
use Hashids\Hashids;

trait ProfileSkillsTrait {

    public function savePositionRequestsSkillsTrait($req) {
        try {
            DB::beginTransaction();
            $hashids = new Hashids('assistant-company', 20);
            $posReqs = $req->positionRequests;
            $skillsInsertArr = [];
            $langsInsertArr = [];
            $resultArr = [];

            for ($i = 0; $i < count($posReqs); $i++) {
                
                // Check if the position request exists...
                if (!$decodedPosReq = $hashids->decode($posReqs[$i]['posReqId'])) throw new ModelNotFoundException;
                PositionRequest::findOrFail($decodedPosReq[0]);

                // If there are skills to add...
                if (array_key_exists('skillsToAdd', $posReqs[$i])) {
                    $skills = $posReqs[$i]['skillsToAdd'];
                    for ($j = 0; $j < count($skills); $j++) {

                        // Check if the skill exists...
                        if (!$decodedSkill = $hashids->decode($skills[$j])) throw new ModelNotFoundException;
                        Skill::findOrFail($decodedSkill[0]);

                        // Create the position skill record...
                        $posSKill = PositionSkill::create(['position_request_id' => $decodedPosReq[0], 'skill_id' => $decodedSkill[0], 'created_at' => date('Y-m-d H:i:s')]);
                    }
                }

                // If there are skills to delete...
                if (array_key_exists('skillsToDelete', $posReqs[$i])) {
                    $skillsDeleteArr = [];
                    $skills = $posReqs[$i]['skillsToDelete'];
                    for ($j = 0; $j < count($skills); $j++) {

                        // Check if the skill exists
                        if (!$decodedPosReqSkill = $hashids->decode($skills[$j])) throw new ModelNotFoundException;
                        PositionSkill::findOrFail($decodedPosReqSkill[0]);
                        array_push($skillsDeleteArr, $decodedPosReqSkill[0]);
                    }
                    PositionSkill::whereIn('id', $skillsDeleteArr)->delete();
                }

                // If there are langs to add...
                if (array_key_exists('langsToAdd', $posReqs[$i])) {
                    $langHash = new Hashids('step-four-data-in-client', 20);
                    $langs = $posReqs[$i]['langsToAdd'];
                    for ($j = 0; $j < count($langs); $j++) {

                        // Check if the language exists
                        if (!$decodedLang = $langHash->decode($langs[$j]['langId'])) throw new ModelNotFoundException;
                        Language::findOrFail($decodedLang[0]);

                        // Check if the language level exists
                        if (!$decodedLangLevel = $langHash->decode($langs[$j]['levelId'])) throw new ModelNotFoundException;
                        LanguageLevel::findOrFail($decodedLangLevel[0]);

                        // Create position language record...
                        $posReqLang = PositionRequestLanguage::create(['position_request_id' => $decodedPosReq[0], 'language_id' => $decodedLang[0], 'level' => $decodedLangLevel[0], 'created_at' => date('Y-m-d H:i:s')]);
                    }
                }

                // If there are langs to delete...
                if (array_key_exists('langsToDelete', $posReqs[$i])) {
                    $langsDeleteArr = [];
                    $langs = $posReqs[$i]['langsToDelete'];
                    for ($j = 0; $j < count($langs); $j++) {

                        // Check if the skill exists
                        if (!$decodedPosReqSkill = $hashids->decode($langs[$j])) throw new ModelNotFoundException;
                        PositionRequestLanguage::findOrFail($decodedPosReqSkill[0]);
                        array_push($langsDeleteArr, $decodedPosReqSkill[0]);
                    }
                    PositionRequestLanguage::whereIn('id', $langsDeleteArr)->delete();
                }

                array_push($resultArr, new PosReqSkillsAndLangsResource(PositionRequest::with(['skills', 'languages'])->find($decodedPosReq[0])));
            }

            DB::commit();
            return [
                'error' => false,
                'profilesSkills' => $resultArr
            ];            
        } catch (ModelNotFoundException $ex) {
            DB::rollback();
            return [
                'error' => true,
                'message' => 'Revise los datos enviados'
            ];
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }
}