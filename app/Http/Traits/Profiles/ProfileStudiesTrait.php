<?php

namespace App\Http\Traits\Profiles;

use App\Http\Resources\Profiles\ClientPositionRequestsStudiesResource;
use App\Models\PositionRequestDegree;
use App\Models\PositionRequest;
use Illuminate\Support\Facades\DB;
use App\Models\Client;
use Hashids\Hashids;

trait ProfileStudiesTrait {

    public function savePositionRequestsStudiesTrait($req) {
        try {
            DB::beginTransaction();
            $hashids = new Hashids('assistant-company', 20);
            $stepFourTalentsHash = new Hashids('step-four-data-in-client', 20);
            $posResDegrees = $req->posResDegrees;
            $savedPosResDegrees = [];
            $decodedClient = $hashids->decode($req->clientId);

            // If the client id doesn't exist
            if (count($decodedClient) === 0) return ['error' => 'No se encontró el cliente'];
            $client = Client::find($decodedClient[0]);
            
            // We pass through all the position requests
            for ($i = 0; $i < count($posResDegrees); $i++) {

                // Decode client position request id
                $positionRequestId = $hashids->decode($posResDegrees[$i]['positionRequestId']);
                
                // If the client id doesn't exist
                if (count($positionRequestId) === 0) return ['error' => 'No se encontró la petición de cargo'];
                $positionRequest = PositionRequest::find($positionRequestId[0]);

                // Decode study type id
                $decodedStudyTypeData = $stepFourTalentsHash->decode($posResDegrees[$i]['studyTypeId']);
                if (count($decodedStudyTypeData) === 0) return ['error' => 'No se encontró el tipo de estudio'];
                $decodedStudyType = $decodedStudyTypeData[0];
                
                // Decode degree id
                $decodedDegreeData = $stepFourTalentsHash->decode($posResDegrees[$i]['degreeId']);
                if (count($decodedDegreeData) === 0) return ['error' => 'No se encontró el área de estudio'];
                $decodedDegree = $decodedDegreeData[0];
                
                if ($posResDegrees[$i]['posReqDegreeId'] !== null) {
                    
                    // Decode position request candidate details id
                    $posReqDegreeId = $hashids->decode($posResDegrees[$i]['posReqDegreeId']);
                    
                    // If the position request candidate details id doesn't exist
                    if (count($posReqDegreeId) === 0) return ['error' => 'No se encontró el detalle del candidato de esta petición de cargo'];

                    $positionRequestDegree = PositionRequestDegree::find($posReqDegreeId[0]);

                    if ($posResDegrees[$i]['toDelete']) {
                        $positionRequestDegree->delete();
                        continue;
                    }
                } else {
                    $positionRequestDegree = new PositionRequestDegree;
                    $positionRequestDegree->position_request_id = $positionRequestId[0];
                }

                $positionRequestDegree->study_type_id = $decodedStudyType;
                $positionRequestDegree->degree_id = $decodedDegree;
                $positionRequestDegree->save();
                $formedArray = [
                    'posReqDegreeId' => $hashids->encode($positionRequestDegree->id),
                    'positionRequestId' => $posResDegrees[$i]['positionRequestId'],
                    'studyTypeId' => $posResDegrees[$i]['studyTypeId'],
                    'degreeId' => $posResDegrees[$i]['degreeId']
                ];
                array_push($savedPosResDegrees, $formedArray);
            }

            DB::commit();
            $client->refresh();
            $result = [
                'profilesStudies' => $savedPosResDegrees,
            ];
            return $result;
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}