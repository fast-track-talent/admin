<?php

namespace App\Http\Traits\Profiles;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\PositionCandidateDetail;
use Illuminate\Support\Facades\DB;
use App\Models\AgeRange;
use App\Models\ExperienceRange;
use Hashids\Hashids;

trait ProfileYearsOfExperienceTrait {

    public function savePositionRequestsAgeExperiencesTrait($req) {
        try {
            DB::beginTransaction();
            $hashids = new Hashids('assistant-company', 20);
            $posReqCandDets = $req->posReqCandDets;
            $responseArr = [];

            for ($i = 0; $i < count($posReqCandDets); $i++) {
                // Check if the position request exists...
                $decodedPosReq = $hashids->decode($posReqCandDets[$i]['positionRequestId']);
                if (count($decodedPosReq) === 0) throw new ModelNotFoundException;

                // Check if the age range exists...
                $decodedAgeRange = $hashids->decode($posReqCandDets[$i]['ageRangeId']);
                if (count($decodedAgeRange) === 0) throw new ModelNotFoundException;
                AgeRange::findOrFail($decodedAgeRange[0]);

                // Check if the exp range exists...
                $decodedExpRange = $hashids->decode($posReqCandDets[$i]['expRangeId']);
                if (count($decodedExpRange) === 0) throw new ModelNotFoundException;
                ExperienceRange::findOrFail($decodedExpRange[0]);

                // Check if position request candidate id comes in the request
                if ($posReqCandDets[$i]['posReqCandDetId'] !== null) {
                    $decodedPosReqCandDetId = $hashids->decode($posReqCandDets[$i]['posReqCandDetId']);
                    if (count($decodedPosReqCandDetId) === 0) throw new ModelNotFoundException;
                    $posReqCandDetail = PositionCandidateDetail::findOrFail($decodedPosReqCandDetId[0]);
                } else {
                    $posReqCandDetail = new PositionCandidateDetail;
                }

                $posReqCandDetail->position_request_id = $decodedPosReq[0];
                $posReqCandDetail->age_range_id = $decodedAgeRange[0];
                $posReqCandDetail->experience_range_id = $decodedExpRange[0];
                $posReqCandDetail->save();
                $elem = $posReqCandDets[$i];
                $elem['posReqCandDetId'] = $hashids->encode($posReqCandDetail->id);
                array_push($responseArr, $elem);
            }

            DB::commit();
            return [
                'error' => false,
                'profilesYearsOfExperience' => $responseArr
            ];
        } catch (ModelNotFoundException $ex) {
            DB::rollback();
            return [
                'error' => true,
                'message' => 'Revise los datos enviados'
            ];
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }
}