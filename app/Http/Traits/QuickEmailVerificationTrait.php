<?php

namespace App\Http\Traits;

use QuickEmailVerification;

trait QuickEmailVerificationTrait {

    public function verify($email) {
        try {
            $client = new QuickEmailVerification\Client(getenv('QEV_API_KEY'));
            $quickemailverification = $client->quickemailverification();
            $response = $quickemailverification->verify($email);
            $qev_res = (array) $response;
            return $qev_res['body']['result'];
        } catch (Exception $ex) {
            return [
                'catched' => $ex->getMessage()
            ];
        }
    }
}