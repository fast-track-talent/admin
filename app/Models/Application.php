<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hashids\Hashids;

class Application extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'user_id',
      'salary_expect_id',
      'description',
      'curriculum',
      'finished',
      'sent_finished_email',
      'reputation',
      'clients_list' // Check if is still needed.
    ];

    public function clients()
    {
      return $this->belongsToMany(Client::class, 'applications_clients', 'application_id', 'client_id');
    }

    public function talent()
    {
      return $this->belongsTo(User::class, 'user_id');
    }

    public function salary_expectation()
    {
      return $this->belongsTo(SalaryExpectation::class, 'salary_expect_id');
    }

    public function personData()
    {
      return $this->hasOne(Person::class, 'user_id', 'user_id');
    }

    public function comments() {
      return $this->hasMany(TalentComment::class);
    }
}
