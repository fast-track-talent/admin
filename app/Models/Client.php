<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Hashids\Hashids;

class Client extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
      'name',
      'number_id',
      'contact_id',
      'description'
    ];
    protected $appends = ['hash_activity'];

    public function getHashActivityAttribute($query)
    {
      $hash_activity = new Hashids('step-five-data-in-client', 20);
      return  $hash_activity->encode($this->attributes['contact_id']);
    }

    public function contact() {
      return $this->hasOne(User::class, 'id', 'contact_id');
    }

    public function products() {
      return $this->hasMany(ClientsProduct::class, 'client_id');
    }

    public function activities()
    {
      return $this->belongsToMany(CompanyActivity::class, 'clients_activities', 'client_id', 'activity_id')->withPivot('id');
    }

    public function applications()
    {
      return $this->belongsToMany(Application::class, 'applications_clients', 'client_id', 'application_id');
    }

    public function positionRequests() {
      return $this->hasMany(PositionRequest::class, 'client_id');
    }

    public function comments() {
      return $this->hasMany(ClientComment::class);
    }
}