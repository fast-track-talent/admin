<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Database\Factories\ClientProductFactory;

class ClientsProduct extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id',
        'name',
        'description'
    ];

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return ClientProductFactory::new();
    }
}
