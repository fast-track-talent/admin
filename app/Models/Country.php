<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Hashids\Hashids;

use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'number_code'];
    protected $appends = ['hash_id'];

    public function getHashIdAttribute($query)
    {
      $hashids = new Hashids('countries-talents-assistant', 20);
      return  $hashids->encode($this->attributes['id']);
    }
}
