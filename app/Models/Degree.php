<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Hashids\Hashids;

use Illuminate\Database\Eloquent\SoftDeletes;

class Degree extends Model
{
    use HasFactory;
    use SoftDeletes;


    protected $dates = ['deleted_at'];
    protected $fillable = ['study_type_id', 'degree'];

    protected $appends = ['hashid'];

    public function studyType() {
      return $this->belongsTo(StudyType::class, 'study_type_id');
    }

    public function getHashidAttribute($query)
    {
      $hashids = new Hashids('step-four-data-in-client', 20);
      return  $hashids->encode($this->attributes['study_type_id']);
    }
}
