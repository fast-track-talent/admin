<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Hashids\Hashids;
use Carbon\Carbon;

class Experience extends Model
{
    use HasFactory;

    protected $appends = ['hash_company_activity','company_activity','position_exp', 'exp_years'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_activity_id',
        'company',
        'position',
        'dependents',
        'from',
        'to',
        'created_at',
        'updated_at'
    ];

    public function getExpYearsAttribute($value) {
      if ($this->attributes['actual']) {
        $from = Carbon::parse($this->attributes['from']);
        $to = Carbon::now()->endOfDay();

        $diff = $from->diffInYears($to);
      } else {
        $from = Carbon::parse($this->attributes['from']);
        $to = Carbon::parse($this->attributes['to'])->endOfDay();

        $diff = $from->diffInYears($to);
      }
      return $diff;
    }

    public function getHashCompanyActivityAttribute($query)
    {
      $hash_company_activity = new Hashids('step-five-data-in-client', 20);
      return  $hash_company_activity->encode($this->attributes['company_activity_id']);
    }
    
    public function getHashExpericenceAttribute($query)
    {
      $hash_company_activity = new Hashids('step-five-data-in-client', 20);
      return  $hash_company_activity->encode($this->attributes['id']);
    }
  
    public function getCompanyActivityAttribute($query)
    {
      $company_activity = $this->CompanyActivities->activity;
   
      return  $company_activity;
    }

    public function CompanyActivities()
    {
        return $this->belongsTo(CompanyActivity::class, 'company_activity_id');
    }

    public function getPositionExpAttribute($query)
    {
      $position_exp = $this->ExperiencePosition->position;
    
      return  $position_exp;
    }
    
    public function ExperiencePosition()
    {
        return $this->belongsTo(Position::class, 'position');
    }  

}
