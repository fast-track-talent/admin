<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Hashids\Hashids;

class Language extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['lang'];
    protected $appends = ['hash_id'];

    public function getHashIdAttribute($query)
    {
      $hashids = new Hashids('step-four-data-in-client', 20);
      return  $hashids->encode($this->attributes['id']);
    }

}
