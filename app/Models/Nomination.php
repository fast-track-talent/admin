<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nomination extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'position_request_id',
        'user_id',
    ];

    public function positionRequest() {
        return $this->belongsTo(PositionRequest::class);
    }

    public function client() {
        return $this->hasOneThrough(PositionRequest::class, Client::class);
    }

    public function talent() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function position() {
        return $this->hasOneThrough(PositionRequest::class, Position::class);
    }

    public function salaryExpectation() {
        return $this->hasOneThrough(PositionRequest::class, SalaryExpectation::class);
    }
}
