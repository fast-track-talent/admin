<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Database\Factories\ClientPersonFactory;
use Database\Factories\TalentPersonFactory;
use Hashids\Hashids;
use Carbon\Carbon;

class Person extends Model
{
    use HasFactory;

    protected $date = [
        'user_id',
        'state_id',
        'firstnames',
        'lastnames',
        'birthdate',
        'gender',
        'country'
    ];

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory($option)
    {
      // return ClientPersonFactory::new();
      if ($option === 'talent') {
        return TalentPersonFactory::new();
      } else if ($option === 'client') {
        return ClientPersonFactory::new();
      }
    }

    protected $appends = ['age'];

    // Relations
    public function user() {
      return $this->belongsTo(User::class, 'user_id');
    }

    public function country() {
      return $this->belongsTo(Country::class, 'country');
    }

    public function countryInfo() {
      return $this->belongsTo(Country::class, 'country');
    }

    public function state() {
      return $this->belongsTo(CountryState::class, 'state_id');
    }

    // Magic attributes
    public function getAgeAttribute() {
      return Carbon::parse($this->attributes['birthdate'])->age;
    } 
}
