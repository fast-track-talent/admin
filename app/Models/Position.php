<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Hashids\Hashids;

use Illuminate\Database\Eloquent\SoftDeletes;

class Position extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $fillable = ['position'];
    protected $appends = ['hash_id'];

    public function getHashIdAttribute($query)
    {
      $hashids = new Hashids('positions-in-client', 20);
      return  $hashids->encode($this->attributes['id']);
    }
    
}
