<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PositionCandidateDetail extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'position_request_id',
        'age_range_id',
        'experience_range_id',
    ];

    public function ageRange() {
        return $this->belongsTo(AgeRange::class, 'age_range_id');
    }

    public function expRange() {
        return $this->belongsTo(ExperienceRange::class, 'experience_range_id');
    }
}
