<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PositionRequest extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id',
        'salary_expectation_id',
        'country_state_id',
        'remote',
        'confidential',
        'vehicle',
        'country',
        'title',
        'goals',
        'report_to',
        'supervise_to',
    ];

    public function client() {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function position() {
        return $this->belongsTo(Position::class, 'position_id');
    }

    public function salaryExpectation() {
        return $this->belongsTo(SalaryExpectation::class, 'salary_expectation_id');
    }

    public function country_rel() {
        return $this->belongsTo(Country::class, 'country');
    }

    public function state() {
        return $this->belongsTo(CountryState::class, 'country_state_id');
    }

    public function candidateDetails() {
        return $this->hasOne(PositionCandidateDetail::class, 'position_request_id');
    }

    public function degrees() {
        return $this->hasMany(PositionRequestDegree::class, 'position_request_id');
    }

    public function studies() {
        return $this->belongsToMany(
            Degree::class,
            'position_request_degrees'
        );
    }
    
    public function skills() {
        return $this->belongsToMany(Skill::class, 'position_skills')->withPivot('id');
    }
    
    public function languages() {
        return $this->belongsToMany(Language::class, 'position_request_languages')->withPivot(['id', 'level']);
    }
    
    public function posReqlangs() {
        return $this->hasMany(PositionRequestLanguage::class);
    }

    /* public function nominations() {
        return $this->belongsToMany(PositionRequest::class, 'nominations', 'position_request_id')->withPivot(['id']);
    } */
    public function posReqNominations()
    {
      return $this->hasMany(Nomination::class);
    }

    public function comments() {
        return $this->hasMany(RequestComment::class, 'request_id');
    }
}
