<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PositionRequestLanguage extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'position_request_id',
        'language_id',
        'level'
    ];

    public function language() {
        return $this->belongsTo(Language::class);
    }

    public function level() {
        return $this->belongsTo(LanguageLevel::class, 'level');
    }
}
