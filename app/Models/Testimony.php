<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Database\Factories\ClientTestimonyFactory;
use Database\Factories\TalentTestimonyFactory;

class Testimony extends Model
{
  use HasFactory;
  use SoftDeletes;

  protected $dates = ['deleted_at'];
  protected $fillable = [
    'description',
    'name',
    'position_id',
    'client_id',
    'user_id',
    'approved'
  ];

  /**
   * Create a new factory instance for the model.
   *
   * @return \Illuminate\Database\Eloquent\Factories\Factory
   */
  protected static function newFactory($op)
  {
    if ($op === 'talent') {
      return ClientTestimonyFactory::new();
    } else if ($op === 'client') {
      return TalentTestimonyFactory::new();
    }
  }

  public function client() {
    return $this->belongsTo(Clients::class, 'client_id');
  }

  public function person()
  {
      return $this->hasOne(Person::class, 'user_id', 'user_id');
  }
}