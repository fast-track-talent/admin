<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Database\Factories\ClientUserFactory;
use Database\Factories\TalentUserFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use QuickEmailVerification;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_type_id',
        'email',
        'password',
        'country_code',
        'phone_number',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory($option)
    {
        if ($option === 'talent') {
            return TalentUserFactory::new();
        } else if ($option === 'client') {
            return ClientUserFactory::new();
        }
    }

    // Relations
    public function country() {
        return $this->hasOne(Country::class, 'id', 'country_code');
    }

    public function personData() {
        return $this->hasOne(Person::class, 'user_id');
    }

    public function application() {
        return $this->hasOne(Application::class, 'user_id')->withTrashed();
    }

    public function studies() {
        return $this->hasMany(UserStudy::class, 'user_id');
    }

    public function skills()
    {
      return $this->belongsToMany(Skill::class, 'user_skills', 'user_id', 'skill_id')->withPivot('id');
    }

    public function languages() {
        return $this->hasMany(UserLang::class, 'user_id');
    }

    public function experiences() {
        return $this->hasMany(Experience::class, 'user_id');
    }

    public function nominations() {
        return $this->belongsToMany(PositionRequest::class, 'nominations', 'user_id')->withPivot(['id']);
    }

    public function client() {
        return $this->belongsTo(Client::class, 'contact_id');
    }

    // Magic attributes
    public function getAvatarPathAttribute($avatar) {
        return asset($avatar ?: 'img/avatar.png');
    }
}
