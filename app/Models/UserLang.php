<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLang extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'lang_id',
        'level_id',
    ];
    
    public function language() {
        return $this->belongsTo(Language::class, 'lang_id');
    }

    public function level() {
        return $this->belongsTo(LanguageLevel::class, 'level_id');
    }
}
