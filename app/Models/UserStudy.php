<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Hashids\Hashids;

class UserStudy extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'study_type_id',
        'degree_id',
        'country',
        'created_at',
        'updated_at'
    ];
    

    // protected $appends = ['hash_study', 'hash_degree', 'degree', 'study_type', 'study_country'];

    /* public function getHashStudyAttribute($query)
    {
      $hash_study = new Hashids('step-four-data-in-client', 20);
      return  $hash_study->encode($this->attributes['study_type_id']);
    }
    public function getHashDegreeAttribute($query)
    {
      $hash_degree = new Hashids('step-four-data-in-client', 20);
      return  $hash_degree->encode($this->attributes['degree_id']);
    }

    public function getDegreeAttribute($value)
    {
      $degree = $this->Degrees->degree;
      return  $degree;
    } */

    public function degree() {
        return $this->belongsTo(Degree::class, 'degree_id');
    }
   
    /* public function getStudyTypeAttribute($value)
    {
        $study_type = $this->StudyTypes->name;
        
        return  $study_type;
    } */

    public function studyType() {
        return $this->belongsTo(StudyType::class, 'study_type_id');
    }

    /* public function getStudyCountryAttribute($value)
    {
        $study_country = $this->StudyCountrys->name;
        return  $study_country;
    } */

    public function country() {
        return $this->belongsTo(Country::class, 'country');
    }
    
}
