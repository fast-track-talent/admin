<?php

/*
    |--------------------------------------------------------------------------
    | Cross-Origin Resource Sharing (CORS) Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure your settings for cross-origin resource sharing
    | or "CORS". This determines what cross-origin operations may execute
    | in web browsers. You are free to adjust these settings as needed.
    |
    | To learn more: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
    |
    */

$cors_config = [

    'paths' => ['*'],

    'allowed_methods' => ['*'],

    'allowed_origins_patterns' => [],

    'allowed_headers' => ['*'],

    'exposed_headers' => [],

    'max_age' => 0,

    'supports_credentials' => false,

];

switch (getenv('DEPLOY_SERVER')) {
    case 'local':
        $cors_config['allowed_origins'] = [
            'http://localhost:3000',
            'localhost:3000',
            'localhost'
        ];
        break;
    case 'staging':
        $cors_config['allowed_origins'] = [
            'https://ftt-web.ekodevs.com'
        ];
        break;
    
    default:
        $cors_config['allowed_origins'] = [
            'https://fttalent.work'
        ];
        break;
}

return $cors_config;