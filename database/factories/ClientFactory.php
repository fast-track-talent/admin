<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Provider\es_VE\Company;
use Faker\Provider\Barcode;
use App\Models\Client;
use App\Models\User;
use App\Models\Person;

class ClientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Client::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'avatar' => $this->faker->imageUrl(360, 360, 'animals', true, 'cats'),
            'name' =>  $this->faker->company(),
            'number_id' =>  $this->faker->ean13(),
            'description' => $this->faker->sentence(1),
            'contact_id' => User::newFactory('client')->has(Person::newFactory()->count(1), 'personData')
        ];
    }
}
