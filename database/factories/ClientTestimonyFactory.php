<?php

namespace Database\Factories;

use App\Models\Testimony;
use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Provider\es_VE\Person;

class ClientTestimonyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Testimony::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'description' => $this->faker->text(),
            'name' => $this->faker->name(),
            'position' => $this->faker->unique()->words(3, true),
            'client_id' => Client::factory(),
            'type' => $this->faker->numberBetween(0, 2),
        ];
    }
}
