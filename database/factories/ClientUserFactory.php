<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Person as PClass;
use App\Models\Client;
use App\Models\Testimony;
use App\Models\ClientsActivity;
use App\Models\ClientsProduct;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Provider\es_VE\Person;
use Faker\Provider\es_VE\PhoneNumber;
use Faker\Provider\en_US\Company;

class ClientUserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterCreating(function (User $user) {
            // Client Person data
            $person = PClass::create([
                'user_id' => $user->id,
                'firstnames' => $this->faker->firstName(null),
                'lastnames' => $this->faker->lastName(),
                'gender' => $this->getGender($this->faker->numberBetween(0, 10)),
                'birthdate' => $this->faker->dateTimeBetween($startDate = '-70 years', $endDate = 'now', $timezone = null),
                'country' => $user->country_code,
            ]);

            // Client data
            $client = Client::create([
                'avatar' => $this->faker->imageUrl(360, 360, 'animals', true, 'dogs', true),
                'name' => $this->faker->company(),
                'number_id' =>  $this->faker->ean13(),
                'contact_id' => $user->id,
                'description' => $this->faker->sentence(6)
            ]);

            // Client activities data
            for ($i=0; $i < $this->faker->numberBetween(1, 10); $i++) { 
                $clients_activities = ClientsActivity::create([
                    'client_id' => $client->id,
                    'activity_id' => $this->faker->numberBetween(1, 72),
                ]);
            }
            // Client products data
            for ($i=0; $i < $this->faker->numberBetween(1, 10); $i++) { 
                $clients_products = ClientsProduct::create([
                    'client_id' => $client->id,
                    'name' => $this->getProductName(),
                    'description' => $this->faker->paragraph($nbSentences = 2, $variableNbSentences = true)
                ]);
            }

            // Client testimonies data
            if ($this->faker->boolean()) {
                $testimonies = Testimony::create([
                    'description' => $this->faker->paragraph($nbSentences = 1, $variableNbSentences = true),
                    'name' => $this->faker->firstName(null). ' ' .$this->faker->lastName(),
                    'position_id' => $this->faker->numberBetween(1, 119),
                    'client_id' => $client->id,
                    'user_id' => null,
                    'approved' => $this->faker->boolean(),
                ]);
            }
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_type_id' => 2,
            'email' => $this->faker->unique()->safeEmail(),
            'country_code' => $this->faker->numberBetween(1, 15),
            'phone_number' => $this->faker->unique()->phoneNumber(),
        ];
    }

    private function getGender($num) {
        if ($num % 2 === 0) {
            return 'M';
        } else {
            return 'F';
        }
    }

    private function getPhoneNumber() {
        $phone = null;
        for ($i=0; $i < 10; $i++) { 
            $phone .= $this->faker->numberBetween(0, 9);
        }
        return $phone;
    }

    private function getProductName() {
        $words = $this->faker->words(3);
        $max = count($words);
        $product = null;
        for ($i = 0; $i < $max; $i++) {
            if ($i === ($max -1)) {
                $product .= $words[$i];
            } else {
                $product .= $words[$i] . " ";
            }
        }
        return $product;
    }
}
