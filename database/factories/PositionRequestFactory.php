<?php

namespace Database\Factories;

use App\Models\PositionRequest;
use App\Models\CountryState;
use App\Models\PositionRequestDegree;
use App\Models\PositionCandidateDetail;
use App\Models\PositionSkill;
use App\Models\PositionRequestLanguage;
use Illuminate\Database\Eloquent\Factories\Factory;

class PositionRequestFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PositionRequest::class;

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        /*
            Age ranges:
            1. Menos de 20
            2. De 20 a 30
            3. 30-40
            4. +40
            
            Exp. ranges:
            1. No es necesaria experiencia 
            2. 1-5 
            3. 5-10 
            4. 10-15
            5. +20
        */
        return $this->afterCreating(function (PositionRequest $positionRequest) {
            
            // Position candidate details
            PositionRequestDegree::create([
                'position_request_id' => $positionRequest->id,
                'study_type_id' => $this->faker->numberBetween(1, 8),
                'degree_id' => $this->faker->numberBetween(1, 151),
            ]);

            // Position candidate details
            PositionCandidateDetail::create([
                'position_request_id' => $positionRequest->id,
                'age_range_id' => $this->faker->numberBetween(1, 6),
                'experience_range_id' => $this->faker->numberBetween(1, 4),
            ]);

            // Position skills
            /*
                Skill level:
                1. Básico.
                2. Intermedio.
                3. Avanzado.
            */
            for ($i=0; $i < $this->faker->numberBetween(1, 10); $i++) { 
                PositionSkill::create([
                    'position_request_id' => $positionRequest->id,
                    'skill_id' => $this->faker->numberBetween(1, 500),
                ]);
            }

            // Position request languages
            /*
                1- A1 - Beginner - Novice (Low/Mid/High)
                2- A2 - Elementary - Intermediate (Low/Mid)
                3- B1 - Intermediate - Intermediate High
                4- B2 - Upper Intermediate - Advanced (Low/Mid/High)
            */
            if ($this->faker->boolean()) {
                $languages = [];
                switch ($this->faker->numberBetween(1, 3)) {
                    case 1: $languages = [1, 3] ; break;
                    case 2: $languages = [1] ; break;
                    case 3: $languages = [1, 2, 3] ; break;
                }
                for ($i=0; $i < count($languages); $i++) {
                    PositionRequestLanguage::create([
                        'position_request_id' => $positionRequest->id,
                        'language_id' => $languages[$i],
                        'level' => $this->faker->numberBetween(1, 4),
                    ]);
                }
            }
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // Find country code from state_id
        $countryState = CountryState::find($this->faker->numberBetween(1, 88));
        $countryStateId = $countryState->id;
        $countryId = $countryState->country;

        $query = [];
        $number = $this->faker->numberBetween(1, 3);
        switch ($number) {
            case 1:
                $query = [
                    'position_id' => $this->faker->numberBetween(1, 119),
                    'client_id' => $this->faker->numberBetween(1, 36),
                    'salary_expectation_id' => $this->faker->numberBetween(1, 5),
                    'country_state_id' => null,
                    'remote' => true,
                    'confidential' => $this->faker->boolean(),
                    'vehicle' => false,
                    'machine' => $this->faker->boolean(),
                    'country' => null,
                    'title' => $this->faker->unique()->words(3, true),
                    'goals' => $this->faker->text(),
                    'report_to' => $this->faker->word(),
                    'supervise_to' => $this->faker->word(),
                    'finished' => 1,
                ];
            break;
            case 2:
                $query = [
                    'position_id' => $this->faker->numberBetween(1, 119),
                    'client_id' => $this->faker->numberBetween(1, 36),
                    'salary_expectation_id' => $this->faker->numberBetween(1, 5),
                    'country_state_id' => $countryStateId,
                    'remote' => false,
                    'confidential' => $this->faker->boolean(),
                    'vehicle' => $this->faker->boolean(),
                    'machine' => false,
                    'country' => $countryId,
                    'title' => $this->faker->unique()->words(3, true),
                    'goals' => $this->faker->text(),
                    'report_to' => $this->faker->word(),
                    'supervise_to' => $this->faker->word(),
                    'finished' => 1,
                ];
            break;
            default:
            $query = [
                'position_id' => $this->faker->numberBetween(1, 119),
                'client_id' => $this->faker->numberBetween(1, 36),
                'salary_expectation_id' => $this->faker->numberBetween(1, 5),
                'country_state_id' => $countryStateId,
                'remote' => true,
                'confidential' => $this->faker->boolean(),
                'vehicle' => $this->faker->boolean(),
                'machine' => $this->faker->boolean(),
                'country' => $countryId,
                'title' => $this->faker->unique()->words(3, true),
                'goals' => $this->faker->text(),
                'report_to' => $this->faker->word(),
                'supervise_to' => $this->faker->word(),
                'finished' => 1,
            ];
            break;
        }

        return $query;
    }
}
