<?php

namespace Database\Factories;

use App\Models\Testimony;
use Illuminate\Database\Eloquent\Factories\Factory;

class TalentTestimonyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Testimony::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'description' => $this->faker->text(),
            'name' => $this->faker->name(),
            'position' => $this->faker->unique()->words(3, true),
            'user_id' => User::newFactory('talent')->has(Person::newFactory()->count(1), 'personData'),
            'type' => $this->faker->numberBetween(0, 2),
        ];
    }
}
