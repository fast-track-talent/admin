<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Person as PClass;
use App\Models\Testimony;
use App\Models\Application;
use App\Models\UserStudy;
use App\Models\Experience;
use App\Models\UserLang;
use App\Models\CountryState;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Provider\es_VE\Person;
use Faker\Provider\es_VE\Company;

class TalentUserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterCreating(function (User $user) {
            // Talent person data
            // Get states from country id
            $stateIds = CountryState::where('country', $user->country_code)->pluck('id');
            if (count($stateIds) > 0) {
                $arrLength = count($stateIds) - 1;
                $person = PClass::create([
                    'user_id' => $user->id,
                    'firstnames' => $this->faker->firstName(null),
                    'lastnames' => $this->faker->lastName(),
                    'gender' => $this->getGender($this->faker->numberBetween(0, 10)),
                    'birthdate' => $this->faker->dateTimeBetween($startDate = '-70 years', $endDate = '-18 years', $timezone = null),
                    'country' => $user->country_code,
                    'state_id' => $stateIds[$this->faker->numberBetween(0, $arrLength)],
                ]);
            } else {
                $person = PClass::create([
                    'user_id' => $user->id,
                    'firstnames' => $this->faker->firstName(null),
                    'lastnames' => $this->faker->lastName(),
                    'gender' => $this->getGender($this->faker->numberBetween(0, 10)),
                    'birthdate' => $this->faker->dateTimeBetween($startDate = '-70 years', $endDate = '-18 years', $timezone = null),
                    'country' => $user->country_code,
                ]);
            }

            // Talent degrees
            if ($this->faker->boolean()) {
                $studyType = 2;
                UserSTudy::create([
                    'user_id' => $user->id,
                    'study_type_id' => $studyType,
                    'degree_id' => $this->faker->numberBetween(1, 2),
                    'country' => ($this->faker->boolean()) ? $user->country_code : $this->faker->numberBetween(1, 15),
                ]);

                do {
                    $studyType = $this->faker->numberBetween(1, 8);
                } while ($studyType === 2);

                for ($i=0; $i < $this->faker->numberBetween(1, 3); $i++) {
                    UserSTudy::create([
                        'user_id' => $user->id,
                        'study_type_id' => $studyType,
                        'degree_id' => $this->faker->numberBetween(3, 151),
                        'country' => ($this->faker->boolean()) ? $user->country_code : $this->faker->numberBetween(1, 15),
                    ]);
                }
            }

            // Talent experiences
            if ($this->faker->boolean()) {
                $fromNumber = $this->faker->numberBetween(6, 10);
                for ($i=0; $i < $this->faker->numberBetween(1, 3); $i++) {
                    if ($this->faker->boolean()) {
                        $toNumber = $fromNumber - 1;
                        $from = new \DateTime("-$fromNumber years");
                        $to = new \DateTime("-$toNumber years");
                    } else {
                        $toNumber = $this->faker->numberBetween(1, 5);
                        $from = new \DateTime("-$fromNumber years");
                        $to = new \DateTime("-$toNumber years");
                    }
                    Experience::create([
                        'user_id' => $user->id,
                        'company_activity_id' => $this->faker->numberBetween(1, 72),
                        'company' => $this->faker->company(),
                        'position' => $this->faker->numberBetween(1, 119),
                        'dependents' => $this->faker->numberBetween(1, 10),
                        'from' => $from,
                        'to' => $to,
                    ]);
                }
            }

            // Talent testimony
            if ($this->faker->boolean()) {
                $testimonies = Testimony::create([
                    'description' => $this->faker->paragraph($nbSentences = 1, $variableNbSentences = true),
                    'name' => $this->faker->firstName(null). ' ' .$this->faker->lastName(),
                    'position_id' => $this->faker->numberBetween(1, 119),
                    'client_id' => null,
                    'user_id' => $user->id,
                    'approved' => $this->faker->boolean(),
                ]);
            }

            $finished = $this->faker->boolean();

            // Talent applications
            Application::create([
                'user_id' => $user->id,
                'salary_expect_id' => $this->faker->numberBetween(1, 5),
                'description' => $this->faker->paragraph($nbSentences = 2, $variableNbSentences = true),
                'finished' => $finished,
                'sent_finished_email' => $finished,
                'reputation' => $this->faker->numberBetween(1, 10),
            ]);

            // Talent languages
            if ($this->faker->boolean()) {
                $languages = [];
                switch ($this->faker->numberBetween(1, 3)) {
                    case 1: $languages = [1, 3] ; break;
                    case 2: $languages = [1] ; break;
                    case 3: $languages = [1, 2, 3] ; break;
                }
                for ($i=0; $i < count($languages); $i++) {
                    UserLang::create([
                        'user_id' => $user->id,
                        'lang_id' => $languages[$i],
                        'level_id' => $this->faker->numberBetween(1, 6),
                    ]);
                }
            }
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'avatar' => $this->faker->imageUrl(360, 360, 'animals', true, 'dogs', true),
            'user_type_id' => 3,
            'email' => $this->faker->unique()->safeEmail(),
            'country_code' => $this->faker->numberBetween(1, 15),
            'phone_number' => $this->getPhoneNumber(),
        ];
    }

    private function getGender($num) {
        if ($num % 2 === 0) {
            return 'M';
        } else {
            return 'F';
        }
    }
    
    private function getPhoneNumber() {
        $phone = null;
        for ($i=0; $i < 10; $i++) { 
            $phone .= $this->faker->numberBetween(0, 9);
        }
        return $phone;
    }
}
