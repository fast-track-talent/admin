<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserStudiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_studies', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('study_type_id')->nullable();
            $table->foreignId('degree_id')->nullable();
            $table->string('country', 3);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelente('cascade');
            $table->foreign('study_type_id')->references('id')->on('study_types')->onDelete('set null');
            $table->foreign('degree_id')->references('id')->on('degrees')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_studies');
    }
}
