<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestimoniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testimonies', function (Blueprint $table) {
            $table->id();
            $table->string('description', 200);
            $table->string('name', 100)->nullable();
            $table->foreignId('position_id')->nullable();
            $table->foreignId('client_id')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->boolean('approved')->default(false);
            $table->softDeletes();
            $table->timestamps();
            
            $table->foreign('position_id')->references('id')->on('positions')->onDelete('set null');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimonies');
    }
}