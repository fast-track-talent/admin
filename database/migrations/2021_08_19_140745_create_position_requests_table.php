<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePositionRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('position_requests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client_id')->unsigned()->notNullable();
            $table->foreignId('position_id')->unsigned()->notNullable();
            $table->foreignId('salary_expectation_id')->unsigned()->nullable();
            $table->foreignId('country')->unsigned()->nullable();
            $table->foreignId('country_state_id')->unsigned()->nullable();
            $table->boolean('remote')->nullable();
            $table->boolean('confidential')->notNullable();
            $table->boolean('vehicle')->nullable();
            $table->boolean('machine')->nullable();
            $table->string('title', 50)->nullable();
            $table->string('goals', 255)->nullable();
            $table->string('report_to', 255)->nullable();
            $table->string('supervise_to', 255)->nullable();
            $table->boolean('finished')->default(false);
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('position_id')->references('id')->on('positions')->onDelete('cascade');
            $table->foreign('salary_expectation_id')->references('id')->on('salary_expectations')->onDelete('set null');
            $table->foreign('country_state_id')->references('id')->on('country_states')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_requests');
    }
}
