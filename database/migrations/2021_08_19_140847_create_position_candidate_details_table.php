<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePositionCandidateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('position_candidate_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('position_request_id')->unsigned()->notNullable();
            $table->foreignId('age_range_id')->nullable();
            $table->foreignId('experience_range_id')->nullable();
            $table->timestamps();

            $table->foreign('position_request_id')->references('id')->on('position_requests')->onDelete('cascade');
            $table->foreign('age_range_id')->references('id')->on('age_ranges')->onDelete('set null');
            $table->foreign('experience_range_id')->references('id')->on('experience_ranges')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_candidate_details');
    }
}
