<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePositionSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('position_skills', function (Blueprint $table) {
            $table->id();
            $table->foreignId('position_request_id')->unsigned();
            $table->foreignId('skill_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('position_request_id')->references('id')->on('position_requests')->onDelete('cascade');
            $table->foreign('skill_id')->references('id')->on('skills')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_skills');
    }
}
