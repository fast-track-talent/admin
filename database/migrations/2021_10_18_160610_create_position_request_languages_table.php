<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePositionRequestLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('position_request_languages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('position_request_id')->unsigned()->notNullable();
            $table->foreignId('language_id')->unsigned()->nullable();
            $table->integer('level');
            $table->softDeletes();
            $table->timestamps();
            
            $table->foreign('position_request_id')->references('id')->on('position_requests')->onDelete('cascade');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_request_languages');
    }
}
