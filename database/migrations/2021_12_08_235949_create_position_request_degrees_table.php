<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePositionRequestDegreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('position_request_degrees', function (Blueprint $table) {
            $table->id();
            $table->foreignId('position_request_id')->unsigned()->notNullable();
            $table->foreignId('study_type_id')->unsigned()->nullable();
            $table->foreignId('degree_id')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('position_request_id')->references('id')->on('position_requests')->onDelete('cascade');
            $table->foreign('study_type_id')->references('id')->on('study_types')->onDelete('set null');
            $table->foreign('degree_id')->references('id')->on('degrees')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_request_degrees');
    }
}
