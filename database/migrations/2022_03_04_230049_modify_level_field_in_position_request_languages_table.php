<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyLevelFieldInPositionRequestLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('position_request_languages', function (Blueprint $table) {
            $table->foreignId('level')->unsigned()->nullable()->change();

            $table->foreign('level')->references('id')->on('language_levels')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('position_request_languages', function (Blueprint $table) {
            $table->integer('level')->change();
        });
    }
}
