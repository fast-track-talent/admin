<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AgeRangesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('age_ranges')->insert([
            [
                'age_range' => 'Menos de 20',
                'min' => 18,
                'max' => 20,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'age_range' => 'De 20 a 25',
                'min' => 20,
                'max' => 25,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'age_range' => 'De 25 a 30',
                'min' => 25,
                'max' => 30,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'age_range' => 'De 30 a 40',
                'min' => 30,
                'max' => 40,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'age_range' => 'De 40 a 50',
                'min' => 40,
                'max' => 50,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'age_range' => 'Más de 50',
                'min' => 50,
                'max' => null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ]
        ]);
    }
}
