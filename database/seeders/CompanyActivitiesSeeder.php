<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanyActivitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company_activities')->insert([
            [
                'activity' => 'Agricultura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Ganadería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Pesca y acuicultura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Silvicultura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Alimentación y nutrición',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Bebidas alcohólicas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Bebidas sin alcohol',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Cosmética y cuidado personal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Equipamiento del hogar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Juguetes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Mascotas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Productos de limpieza',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Reformas en el hogar y jardinería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Ropa y complementos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Tabaco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Automóviles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Cifras clave del comercio electrónico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Comercio electrónico B2B',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Comercio electrónico B2C',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Comercio electrónico C2C',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Pagos digitales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Alimentación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Centros comerciales y outlets',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Cifras clave del comercio minorista',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Comercio al por mayor',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Comercio internacional',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Comportamiento de compra',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Deporte y ocio',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'DIY/Bricolaje',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Electrónica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Material de oficina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Mobiliario',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Moda y complementos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Salud e higiene',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Arquitectura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Ingeniería civil',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Construcción',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Aficiones',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Arte y cultura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Deporte y fitness',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Juegos de azar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Parques y actividades al aire libre',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Agua y aguas residuales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Emisiones',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Energía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Energía y tecnología medioambiental',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Gestión de residuos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Entidades financieras',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Mercados financieros',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Sector inmobiliario',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Seguros',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Servicios financieros',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Alcance y tráfico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Ciberdelincuencia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Demografía y uso',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Internet móvil y aplicaciones',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Motores de búsqueda y SEO',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Publicidad y marketing',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Social media y contenido creado por el usuario',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Vídeos online y entretenimiento',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Aviación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Logística',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Tráfico rodado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Transporte acuático',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Transporte ferroviario',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Cine, radio y televisión',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Industria del videojuego',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Mercado editorial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Música',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Publicidad y marketing',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Uso de medios de comunicación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Construcción naval',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Electrónica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Industria aeroespacial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Industria de defensa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Ingeniería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Metales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Producción de material rodante',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Producción de vehículos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Combustibles fósiles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Industria química',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Minería, metales y minerales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Papel y pasta de papel',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Plástico y caucho',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Productos minerales no metálicos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Refinerías de petróleo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Asistencia y cuidados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Estado de salud',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Hospitales, farmacias y médicos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Industria farmacéutica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Sistema sanitario',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Tecnología médica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Empleo cualificado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Servicios empresariales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Servicios Primarios',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Servicios Secundarios',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Servicios Terciarios',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Delincuencia y aplicación de la ley',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Demografía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Economía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Educación y ciencia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Geografía y medio ambiente',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Política y Gobierno',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Religión',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Electrodomésticos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Electrónica de consumo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Hardware',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Servicios de TI',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Software',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Telecomunicaciones',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Alojamiento',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Restaurantes y cafeterías',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Viajes de negocios',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'activity' => 'Viajes de ocio',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
        ]);
    }
}
