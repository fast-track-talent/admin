<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountryStatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('country_states')->insert([
            // Albania
            [
                'country' => 2,
                'name' => 'Berat',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 2,
                'name' => 'Diber',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 2,
                'name' => 'Durres',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 2,
                'name' => 'Elbasan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 2,
                'name' => 'Fier',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 2,
                'name' => 'Gjirokaster',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 2,
                'name' => 'Korce',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 2,
                'name' => 'Kukes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 2,
                'name' => 'Lezhe',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 2,
                'name' => 'Shkoder',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 2,
                'name' => 'Tirane',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 2,
                'name' => 'Vlore',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Alemania
            [
                'country' => 3,
                'name' => 'Brandenburg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 3,
                'name' => 'Baden-Württemberg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 3,
                'name' => 'Bayern',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 3,
                'name' => 'Hessen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 3,
                'name' => 'Hamburg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 3,
                'name' => 'Mecklenburg-Vorpommern',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 3,
                'name' => 'Niedersachsen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 3,
                'name' => 'Nordrhein-Westfalen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 3,
                'name' => 'Rheinland-Pfalz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 3,
                'name' => 'Schleswig-Holstein',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 3,
                'name' => 'Sachsen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 3,
                'name' => 'Sachsen-Anhalt',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 3,
                'name' => 'Thüringen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 3,
                'name' => 'Berlin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 3,
                'name' => 'Bremen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 3,
                'name' => 'Saarland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Andorra
            [
                'country' => 4,
                'name' => 'Canillo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 4,
                'name' => 'Encamp',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 4,
                'name' => 'La Massana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 4,
                'name' => 'Ordino',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 4,
                'name' => 'Sant Julia de Loria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 4,
                'name' => 'Andorra la Vella',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 4,
                'name' => 'Escaldes-Engordany',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Argentina
            [
                'country' => 11,
                'name' => 'Buenos Aires',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Catamarca',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Chaco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Chubut',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Córdoba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Corrientes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Distrito Federal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Entre Ríos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Formosa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Jujuy',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'La Pampa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'La Rioja',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Mendoza',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Misiones',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Neuquén',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Río Negro',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Salta',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'San Juan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'San Luis',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Santa Cruz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Santa Fe',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Santiago del Estero',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Tierra del Fuego',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 11,
                'name' => 'Tucumán',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Armenia
            [
                'country' => 12,
                'name' => 'Aragatsotn',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 12,
                'name' => 'Ararat',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 12,
                'name' => 'Armavir',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 12,
                'name' => 'Gegharkunik',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 12,
                'name' => 'Kotayk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 12,
                'name' => 'Lorri',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 12,
                'name' => 'Shirak',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 12,
                'name' => 'Syunik',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 12,
                'name' => 'Tavush',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 12,
                'name' => 'Vayots Dzor',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 12,
                'name' => 'Yerevan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Australia
            [
                'country' => 14,
                'name' => 'Australian Capital Territory',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 14,
                'name' => 'New South Wales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 14,
                'name' => 'Northern Territory',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 14,
                'name' => 'Queensland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 14,
                'name' => 'South Australia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 14,
                'name' => 'Tasmania',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 14,
                'name' => 'Victoria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 14,
                'name' => 'Western Australia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Austria
            [
                'country' => 15,
                'name' => 'Burgenland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 15,
                'name' => 'Kärnten',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 15,
                'name' => 'Niederösterreich',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 15,
                'name' => 'Oberösterreich',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 15,
                'name' => 'Salzburg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 15,
                'name' => 'Steiermark',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 15,
                'name' => 'Tirol',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 15,
                'name' => 'Vorarlberg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 15,
                'name' => 'Wien',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Azerbaiyán
            [
                'country' => 16,
                'name' => 'Azerbaijan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 16,
                'name' => 'Nargorni Karabakh',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 16,
                'name' => 'Nakhichevanskaya Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Bélgica
            [
                'country' => 17,
                'name' => 'Bruxelles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 17,
                'name' => 'West-Vlaanderen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 17,
                'name' => 'Oost-Vlaanderen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 17,
                'name' => 'Limburg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 17,
                'name' => 'Vlaams Brabant',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 17,
                'name' => 'Antwerpen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 17,
                'name' => 'LiÄ ge',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 17,
                'name' => 'Namur',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 17,
                'name' => 'Hainaut',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 17,
                'name' => 'Luxembourg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 17,
                'name' => 'Brabant Wallon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Bielorrusia 25
            [
                'country' => 25,
                'name' => 'Brestskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 25,
                'name' => 'Vitebskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 25,
                'name' => 'Gomelskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 25,
                'name' => 'Grodnenskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 25,
                'name' => 'Minskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 25,
                'name' => 'Mogilevskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Bolivia
            [
                'country' => 27,
                'name' => 'Chuquisaca',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 27,
                'name' => 'Cochabamba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 27,
                'name' => 'El Beni',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 27,
                'name' => 'La Paz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 27,
                'name' => 'Oruro',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 27,
                'name' => 'Pando',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 27,
                'name' => 'Potosí',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 27,
                'name' => 'Santa Cruz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 27,
                'name' => 'Tarija',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Bosnia y Herzegovina
            [
                'country' => 28,
                'name' => 'Federation of Bosnia and Herzegovina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 28,
                'name' => 'Republika Srpska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Brasil
            [
                'country' => 30,
                'name' => 'Acre',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Alagoas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Amapa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Amazonas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Bahia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Ceara',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Distrito Federal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Espirito Santo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Mato Grosso do Sul',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Maranhao',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Mato Grosso',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Minas Gerais',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Para',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Paraiba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Parana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Piaui',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Rio de Janeiro',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Rio Grande do Norte',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Rio Grande do Sul',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Rondonia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Roraima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Santa Catarina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Sao Paulo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Sergipe',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Goias',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Pernambuco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 30,
                'name' => 'Tocantins',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Bulgaria
            [
                'country' => 32,
                'name' => 'Mikhaylovgrad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Blagoevgrad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Burgas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Dobrich',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Gabrovo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Grad Sofiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Khaskovo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Kurdzhali',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Kyustendil',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Lovech',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Montana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Pazardzhik',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Pernik',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Pleven',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Plovdiv',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Razgrad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Ruse',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Shumen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Silistra',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Sliven',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Smolyan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Sofiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Stara Zagora',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Turgovishte',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Varna',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Veliko Turnovo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Vidin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Vratsa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 32,
                'name' => 'Yambol',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Camerún
            [
                'country' => 37,
                'name' => 'Littoral',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 37,
                'name' => 'Southwest Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 37,
                'name' => 'North',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 37,
                'name' => 'Central',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Canadá
            [
                'country' => 38,
                'name' => 'Newfoundland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 38,
                'name' => 'Nova Scotia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 38,
                'name' => 'Prince Edward Island',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 38,
                'name' => 'New Brunswick',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 38,
                'name' => 'Quebec',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 38,
                'name' => 'Ontario',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 38,
                'name' => 'Manitoba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 38,
                'name' => 'Saskatchewan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 38,
                'name' => 'Alberta',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 38,
                'name' => 'British Columbia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 38,
                'name' => 'Nunavut',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 38,
                'name' => 'Northwest Territories',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 38,
                'name' => 'Yukon Territory',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Chile
            [
                'country' => 40,
                'name' => 'Región de Tarapacá',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 40,
                'name' => 'Región de Antofagasta',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 40,
                'name' => 'Región de Atacama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 40,
                'name' => 'Región de Coquimbo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 40,
                'name' => 'Región de Valparaíso',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 40,
                'name' => 'Región del Libertador General Bernardo O\'Higgins',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 40,
                'name' => 'Región del Maule',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 40,
                'name' => 'Región del Bío Bío',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 40,
                'name' => 'Región de La Araucanía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 40,
                'name' => 'Región de Los Lagos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 40,
                'name' => 'Región Aisén del General Carlos Ibáñez del Campo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 40,
                'name' => 'Región de Magallanes y de la Antártica Chilena',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 40,
                'name' => 'Región Metropolitana de Santiago',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // China
            [
                'country' => 41,
                'name' => 'Anhui',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Zhejiang',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Jiangxi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Jiangsu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Jilin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Qinghai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Fujian',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Heilongjiang',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Henan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Hebei',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Hunan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Hubei',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Xinjiang',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Xizang',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Gansu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Guangxi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Guizhou',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Liaoning',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Nei Mongol',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Ningxia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Beijing',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Shanghai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Shanxi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Shandong',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Shaanxi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Sichuan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Tianjin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Yunnan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Guangdong',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Hainan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Chongqing',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Schanghai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Hongkong',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Neimenggu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 41,
                'name' => 'Aomen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Chipre
            [
                'country' => 42,
                'name' => 'Government controlled area',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 42,
                'name' => 'Turkish controlled area',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Colombia
            [
                'country' => 44,
                'name' => 'Amazonas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Antioquia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Arauca',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Atlántico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Caquetá',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Cauca',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'César',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Chocó',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Córdoba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Guaviare',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Guainía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Huila',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'La Guajira',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Meta',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Narino',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Norte de Santander',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Putumayo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Quindío',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Risaralda',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'San Andrés y Providencia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Santander',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Sucre',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Tolima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Valle del Cauca',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Vaupés',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Vichada',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Casanare',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Cundinamarca',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Distrito Especial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Caldas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 44,
                'name' => 'Magdalena',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Comoras
            [
                'country' => 45,
                'name' => 'Cheju',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // República del Congo
            [
                'country' => 46,
                'name' => 'Congo Brazzaville',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // República Democrática del Congo
            [
                'country' => 47,
                'name' => 'Congo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Corea del Norte
            [
                'country' => 48,
                'name' => 'Korea',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Corea del Sur
            [
                'country' => 49,
                'name' => 'Chollabuk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 49,
                'name' => 'Chollanam',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 49,
                'name' => 'Chungcheongbuk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 49,
                'name' => 'Chungcheongnam',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 49,
                'name' => 'Incheon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 49,
                'name' => 'Kangweon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 49,
                'name' => 'Kwangju',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 49,
                'name' => 'Kyeonggi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 49,
                'name' => 'Kyeongsangbuk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 49,
                'name' => 'Kyeongsangnam',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 49,
                'name' => 'Pusan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 49,
                'name' => 'Seoul',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 49,
                'name' => 'Taegu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 49,
                'name' => 'Taejeon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 49,
                'name' => 'Ulsan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Costa de Marfil
            [
                'country' => 50,
                'name' => 'Cote D Ivoire',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Costa Rica
            [
                'country' => 51,
                'name' => 'Alajuela',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 51,
                'name' => 'Cartago',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 51,
                'name' => 'Guanacaste',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 51,
                'name' => 'Heredia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 51,
                'name' => 'Limón',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 51,
                'name' => 'Puntarenas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 51,
                'name' => 'San José',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Croacia
            [
                'country' => 52,
                'name' => 'Bjelovarsko-Bilogorska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Brodsko-Posavska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Dubrovacko-Neretvanska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Istarska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Karlovacka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Koprivnicko-Krizevacka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Krapinsko-Zagorska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Licko-Senjska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Medimurska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Osjecko-Baranjska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Pozesko-Slavonska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Primorsko-Goranska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Sibensko-Kninska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Sisacko-Moslavacka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Splitsko-Dalmatinska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Varazdinska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Viroviticko-Podravska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Vukovarsko-Srijemska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Zadarska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Zagrebacka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 52,
                'name' => 'Grad Zagreb',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Cuba
            [
                'country' => 53,
                'name' => 'Pinar del Rio',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 53,
                'name' => 'Ciudad de la Habana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 53,
                'name' => 'Matanzas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 53,
                'name' => 'Isla de la Juventud',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 53,
                'name' => 'Camaguey',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 53,
                'name' => 'Ciego de Avila',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 53,
                'name' => 'Cienfuegos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 53,
                'name' => 'Granma',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 53,
                'name' => 'Guantanamo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 53,
                'name' => 'La Habana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 53,
                'name' => 'Holguin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 53,
                'name' => 'Las Tunas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 53,
                'name' => 'Sancti Spiritus',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 53,
                'name' => 'Santiago de Cuba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 53,
                'name' => 'Villa Clara',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Curazao
            [
                'country' => 54,
                'name' => 'Willemstad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Dinamarca
            [
                'country' => 55,
                'name' => 'Arhus',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 55,
                'name' => 'Bornholm',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 55,
                'name' => 'Frederiksborg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 55,
                'name' => 'Fyn',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 55,
                'name' => 'Kobenhavn',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 55,
                'name' => 'Staden Kobenhavn',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 55,
                'name' => 'Nordjylland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 55,
                'name' => 'Ribe',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 55,
                'name' => 'Ringkobing',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 55,
                'name' => 'Roskilde',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 55,
                'name' => 'Sonderjylland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 55,
                'name' => 'Storstrom',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 55,
                'name' => 'Vejle',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 55,
                'name' => 'Vestsjalland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 55,
                'name' => 'Viborg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Ecuador
            [
                'country' => 57,
                'name' => 'Galápagos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Azuay',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Bolívar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Canar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Carchi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Chimborazo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Cotopaxi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'El Oro',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Esmeraldas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Guayas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Imbabura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Loja',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Los Ríos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Manabí',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Morona-Santiago',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Pastaza',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Pichincha',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Tungurahua',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Zamora-Chinchipe',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Sucumbíos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Napo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 57,
                'name' => 'Orellana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Egipto
            [
                'country' => 58,
                'name' => 'Al QÄ hira',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 58,
                'name' => 'Aswan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 58,
                'name' => 'Asyut',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 58,
                'name' => 'Beni Suef',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 58,
                'name' => 'Gharbia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 58,
                'name' => 'Damietta',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 58,
                'name' => 'Egypt',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 58,
                'name' => 'Sinai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // El Salvador
            [
                'country' => 59,
                'name' => 'Ahuachapán',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 59,
                'name' => 'Cuscatlán',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 59,
                'name' => 'La Libertad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 59,
                'name' => 'La Paz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 59,
                'name' => 'La Unión',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 59,
                'name' => 'San Miguel',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 59,
                'name' => 'San Salvador',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 59,
                'name' => 'Santa Ana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 59,
                'name' => 'Sonsonate',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Emiratos Árabes Unidos
            [
                'country' => 60,
                'name' => 'Dubai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Eslovaquia
            [
                'country' => 62,
                'name' => 'Banska Bystrica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 62,
                'name' => 'Bratislava',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 62,
                'name' => 'Kosice',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 62,
                'name' => 'Nitra',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 62,
                'name' => 'Presov',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 62,
                'name' => 'Trencin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 62,
                'name' => 'Trnava',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 62,
                'name' => 'Zilina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Eslovenia
            [
                'country' => 63,
                'name' => 'Beltinci',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Bohinj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Borovnica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Bovec',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Brda',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Brezice',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Brezovica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Cerklje na Gorenjskem',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Cerkno',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Crna na Koroskem',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Crnomelj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Divaca',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Dobrepolje',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Dol pri Ljubljani',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Duplek',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Gornji Grad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Hrastnik',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Hrpelje-Kozina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Idrija',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Ig',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Ilirska Bistrica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Ivancna Gorica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Komen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Koper-Capodistria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Kozje',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Kranj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Kranjska Gora',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Krsko',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Lasko',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Ljubljana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Ljubno',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Logatec',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Medvode',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Menges',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Mezica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Moravce',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Mozirje',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Murska Sobota',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Nova Gorica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Ormoz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Pesnica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Postojna',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Radece',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Radenci',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Radovljica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Rogaska Slatina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Sencur',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Sentilj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Sevnica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Sezana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Skofja Loka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Slovenj Gradec',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Slovenske Konjice',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Smarje pri Jelsah',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Tolmin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Trbovlje',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Trzic',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Velenje',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Vipava',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Vrhnika',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Vuzenica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Zagorje ob Savi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Zelezniki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Ziri',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Zrece',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Domzale',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Jesenice',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Kamnik',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Kocevje',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Lenart',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Litija',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Ljutomer',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Maribor',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Novo Mesto',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Piran',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Preddvor',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Ptuj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Ribnica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Sentjur pri Celju',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Slovenska Bistrica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Videm',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 63,
                'name' => 'Zalec',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // España
            [
                'country' => 64,
                'name' => 'Las Palmas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Soria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Palencia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Zamora',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Cádiz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Navarra',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Ourense',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Segovia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Guipuacutezcoa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Ciudad Real',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Vizcaya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'álava',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'A Coruña',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Cantabria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Almería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Zaragoza',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Santa Cruz de Tenerife',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Cáceres',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Guadalajara',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'ávila',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Toledo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Castellón',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Tarragona',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Lugo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'La Rioja',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Ceuta',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Murcia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Salamanca',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Valladolid',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Jaén',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Girona',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Granada',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Alacant',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Córdoba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Albacete',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Cuenca',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Pontevedra',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Teruel',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Melilla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Barcelona',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Badajoz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Madrid',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Sevilla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Valegravencia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Huelva',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Lleida',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'León',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Illes Balears',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Burgos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Huesca',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Asturias',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 64,
                'name' => 'Málaga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Estados Unides de América
            [
                'country' => 65,
                'name' => 'Armed Forces Americas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Armed Forces Europe',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Alaska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Alabama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Armed Forces Pacific',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Arkansas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'American Samoa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Arizona',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'California',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Colorado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Connecticut',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'District of Columbia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Delaware',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Florida',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Federated States of Micronesia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Georgia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Hawaii',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Iowa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Idaho',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Illinois',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Indiana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Kansas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Kentucky',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Louisiana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Massachusetts',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Maryland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Maine',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Marshall Islands',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Michigan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Minnesota',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Missouri',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Northern Mariana Islands',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Mississippi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Montana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'North Carolina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'North Dakota',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Nebraska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'New Hampshire',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'New Jersey',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'New Mexico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Nevada',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'New York',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Ohio',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Oklahoma',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Oregon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Pennsylvania',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Palau',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Rhode Island',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'South Carolina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'South Dakota',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Tennessee',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Texas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Utah',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Virginia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Virgin Islands',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Vermont',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Washington',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'West Virginia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Wisconsin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 65,
                'name' => 'Wyoming',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Finlandia
            [
                'country' => 69,
                'name' => 'Lapland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 69,
                'name' => 'Oulu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 69,
                'name' => 'Southern Finland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 69,
                'name' => 'Eastern Finland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 69,
                'name' => 'Western Finland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Francia
            [
                'country' => 71,
                'name' => 'Ain',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Haute-Savoie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Aisne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Allier',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Alpes-de-Haute-Provence',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Hautes-Alpes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Alpes-Maritimes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Ardegraveche',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Ardennes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Ariegravege',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Aube',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Aude',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Aveyron',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Bouches-du-Rhocircne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Calvados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Cantal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Charente',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Charente Maritime',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Cher',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Corregraveze',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Dordogne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Corse',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Cocircte dOr',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Saocircne et Loire',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Cocirctes dArmor',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Creuse',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Doubs',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Drocircme',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Eure',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Eure-et-Loire',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Finistegravere',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Gard',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Haute-Garonne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Gers',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Gironde',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Hérault',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Ille et Vilaine',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Indre',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Indre-et-Loire',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Isère',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Jura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Landes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Loir-et-Cher',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Loire',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Rhocircne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Haute-Loire',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Loire Atlantique',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Loiret',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Lot',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Lot-et-Garonne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Lozegravere',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Maine et Loire',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Manche',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Marne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Haute-Marne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Mayenne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Meurthe-et-Moselle',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Meuse',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Morbihan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Moselle',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Niegravevre',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Nord',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Oise',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Orne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Pas-de-Calais',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Puy-de-Docircme',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Pyrénées-Atlantiques',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Hautes-Pyrénées',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Pyrénées-Orientales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Bas Rhin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Haut Rhin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Haute-Saocircne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Sarthe',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Savoie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Paris',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Seine-Maritime',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Seine-et-Marne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Yvelines',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Deux-Segravevres',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Somme',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Tarn',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Tarn-et-Garonne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Var',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Vaucluse',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Vendée',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Vienne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Haute-Vienne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Vosges',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Yonne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Territoire de Belfort',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Essonne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Hauts-de-Seine',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Seine-Saint-Denis',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 71,
                'name' => 'Val-de-Marne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Georgia
            [
                'country' => 74,
                'name' => 'Abkhazia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 74,
                'name' => 'Ajaria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 74,
                'name' => 'Georgia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 74,
                'name' => 'South Ossetia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Grecia
            [
                'country' => 78,
                'name' => 'Evros',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Rodhopi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Xanthi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Drama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Serrai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Kilkis',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Pella',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Florina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Kastoria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Grevena',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Kozani',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Imathia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Thessaloniki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Kavala',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Khalkidhiki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Pieria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Ioannina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Thesprotia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Preveza',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Arta',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Larisa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Trikala',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Kardhitsa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Magnisia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Kerkira',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Levkas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Kefallinia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Zakinthos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Fthiotis',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Evritania',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Aitolia kai Akarnania',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Fokis',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Voiotia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Evvoia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Attiki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Argolis',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Korinthia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Akhaia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Ilia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Messinia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Arkadhia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Lakonia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Khania',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Rethimni',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Iraklion',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Lasithi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Dhodhekanisos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Samos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Kikladhes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Khios',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 78,
                'name' => 'Lesvos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Guadalupe
            [
                'country' => 80,
                'name' => 'Grande-Terre',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 80,
                'name' => 'Basse-Terre',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Guatemala
            [
                'country' => 82,
                'name' => 'Alta Verapaz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Baja Verapaz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Chimaltenango',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Chiquimula',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'El Progreso',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Escuintla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Guatemala',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Huehuetenango',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Izabal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Jalapa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Jutiapa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Petén',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Quetzaltenango',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Quiché',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Retalhuleu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Sacatepéquez',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'San Marcos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Santa Rosa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Sololá',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Suchitepequez',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Totonicapán',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 82,
                'name' => 'Zacapa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Haití
            [
                'country' => 89,
                'name' => 'Artibonite',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 89,
                'name' => 'GrandAnse',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 89,
                'name' => 'North West',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 89,
                'name' => 'West',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 89,
                'name' => 'South',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 89,
                'name' => 'South East',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Honduras
            [
                'country' => 90,
                'name' => 'Tegucigalpa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Hungría
            [
                'country' => 92,
                'name' => 'Bacs-Kiskun',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Baranya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Bekes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Borsod-Abauj-Zemplen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Budapest',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Csongrad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Debrecen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Fejer',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Gyor-Moson-Sopron',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Hajdu-Bihar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Heves',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Komarom-Esztergom',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Miskolc',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Nograd',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Pecs',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Pest',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Somogy',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Szabolcs-Szatmar-Bereg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Szeged',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Jasz-Nagykun-Szolnok',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Tolna',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Vas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Veszprem',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Zala',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Gyor',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 92,
                'name' => 'Veszprem',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // India
            [
                'country' => 93,
                'name' => 'Delhi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Bangala',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Chhattisgarh',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Karnataka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Uttaranchal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Andhara Pradesh',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Assam',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Bihar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Gujarat',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Jammu and Kashmir',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Kerala',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Madhya Pradesh',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Manipur',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Maharashtra',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Megahalaya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Orissa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Punjab',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Pondisheri',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Rajasthan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Tamil Nadu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Tripura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Uttar Pradesh',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Haryana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'Chandigarh',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 93,
                'name' => 'India',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Irán
            [
                'country' => 95,
                'name' => 'Husistan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 95,
                'name' => 'Azarbayjan-e Khavari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 95,
                'name' => 'Esfahan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 95,
                'name' => 'Hamadan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 95,
                'name' => 'Kordestan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 95,
                'name' => 'Markazi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 95,
                'name' => 'Sistan-e Baluches',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 95,
                'name' => 'Yazd',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 95,
                'name' => 'Kerman',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 95,
                'name' => 'Kermanshakhan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 95,
                'name' => 'Mazenderan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 95,
                'name' => 'Tehran',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 95,
                'name' => 'Fars',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 95,
                'name' => 'Horasan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Irak
            [
                'country' => 96,
                'name' => 'Baghdad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 96,
                'name' => 'Basra',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 96,
                'name' => 'Mosul',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Irlanda
            [
                'country' => 97,
                'name' => 'Dublin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Galway',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Kildare',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Leitrim',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Limerick',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Mayo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Meath',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Carlow',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Kilkenny',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Laois',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Longford',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Louth',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Offaly',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Westmeath',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Wexford',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Wicklow',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Roscommon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Sligo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Clare',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Cork',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Kerry',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Tipperary',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Waterford',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Cavan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Donegal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 97,
                'name' => 'Monaghan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Isla Norfolk
            [
                'country' => 101,
                'name' => 'Kingston',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Islandia
            [
                'country' => 102,
                'name' => 'Akureyri',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Arnessysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Austur-Bardastrandarsysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Austur-Hunavatnssysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Austur-Skaftafellssysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Borgarfjardarsysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Dalasysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Eyjafjardarsysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Gullbringusysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Hafnarfjordur',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Kjosarsysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Kopavogur',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Myrasysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Neskaupstadur',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Nordur-Isafjardarsysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Nordur-Mulasysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Nordur-Tingeyjarsysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Olafsfjordur',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Rangarvallasysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Reykjavik',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Saudarkrokur',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Seydisfjordur',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Skagafjardarsysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Snafellsnes- og Hnappadalssysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Strandasysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Sudur-Mulasysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Sudur-Tingeyjarsysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Vestmannaeyjar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Vestur-Bardastrandarsysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Vestur-Isafjardarsysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 102,
                'name' => 'Vestur-Skaftafellssysla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Islas Bermudas
            [
                'country' => 103,
                'name' => 'Hamilton',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Islas Cook
            [
                'country' => 106,
                'name' => 'Avarua',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Islas Feroe
            [
                'country' => 108,
                'name' => 'Torshavn',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Islas Maldivas
            [
                'country' => 111,
                'name' => 'Maldives',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Islas Marianas del Norte
            [
                'country' => 113,
                'name' => 'Isle of Man',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Islas Pitcairn
            [
                'country' => 115,
                'name' => 'Adamstown',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Islas Turcas y Caico
            [
                'country' => 117,
                'name' => 'Grand Turk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Islas Vírgenes Británicas
            [
                'country' => 119,
                'name' => 'Road Town',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Israel
            [
                'country' => 121,
                'name' => 'Southern District',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 121,
                'name' => 'Central District',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 121,
                'name' => 'Northern District',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 121,
                'name' => 'Haifa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 121,
                'name' => 'Tel Aviv',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 121,
                'name' => 'Jerusalem',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 121,
                'name' => 'Ramat Hagolan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Italia
            [
                'country' => 122,
                'name' => 'Piemonte - Torino',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Piemonte - Alessandria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Piemonte - Asti',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Piemonte - Biella',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Piemonte - Cuneo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Piemonte - Novara',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Piemonte - Verbania',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Piemonte - Vercelli',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Valle dAosta - Aosta',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lombardia - Milano',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lombardia - Bergamo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lombardia - Brescia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lombardia - Como',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lombardia - Cremona',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lombardia - Lecco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lombardia - Lodi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lombardia - Mantova',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lombardia - Pavia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lombardia - Sondrio',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lombardia - Varese',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Trentino Alto Adige - Trento',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Trentino Alto Adige - Bolzano',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Veneto - Venezia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Veneto - Belluno',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Veneto - Padova',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Veneto - Rovigo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Veneto - Treviso',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Veneto - Verona',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Veneto - Vicenza',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Friuli Venezia Giulia - Trieste',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Friuli Venezia Giulia - Gorizia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Friuli Venezia Giulia - Pordenone',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Friuli Venezia Giulia - Udine',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Liguria - Genova',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Liguria - Imperia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Liguria - La Spezia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Liguria - Savona',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Emilia Romagna - Bologna',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Emilia Romagna - Ferrara',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Emilia Romagna - Forlì-Cesena',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Emilia Romagna - Modena',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Emilia Romagna - Parma',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Emilia Romagna - Piacenza',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Emilia Romagna - Ravenna',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Emilia Romagna - Reggio Emilia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Emilia Romagna - Rimini',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Toscana - Firenze',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Toscana - Arezzo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Toscana - Grosseto',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Toscana - Livorno',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Toscana - Lucca',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Toscana - Massa Carrara',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Toscana - Pisa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Toscana - Pistoia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Toscana - Prato',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Toscana - Siena',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Umbria - Perugia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Umbria - Terni',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Marche - Ancona',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Marche - Ascoli Piceno',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Marche - Macerata',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Marche - Pesaro - Urbino',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lazio - Roma',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lazio - Frosinone',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lazio - Latina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lazio - Rieti',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Lazio - Viterbo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Abruzzo - L´Aquila',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Abruzzo - Chieti',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Abruzzo - Pescara',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Abruzzo - Teramo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Molise - Campobasso',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Molise - Isernia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Campania - Napoli',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Campania - Avellino',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Campania - Benevento',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Campania - Caserta',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Campania - Salerno',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Puglia - Bari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Puglia - Brindisi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Puglia - Foggia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Puglia - Lecce',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Puglia - Taranto',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Basilicata - Potenza',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Basilicata - Matera',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Calabria - Catanzaro',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Calabria - Cosenza',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Calabria - Crotone',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Calabria - Reggio Calabria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Calabria - Vibo Valentia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Sicilia - Palermo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Sicilia - Agrigento',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Sicilia - Caltanissetta',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Sicilia - Catania',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Sicilia - Enna',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Sicilia - Messina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Sicilia - Ragusa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Sicilia - Siracusa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Sicilia - Trapani',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Sardegna - Cagliari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Sardegna - Nuoro',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Sardegna - Oristano',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 122,
                'name' => 'Sardegna - Sassari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Japón
            [
                'country' => 124,
                'name' => 'Aichi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Akita',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Aomori',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Wakayama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Gifu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Gunma',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Ibaraki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Iwate',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Ishikawa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Kagawa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Kagoshima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Kanagawa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Kyoto',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Kochi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Kumamoto',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Mie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Miyagi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Miyazaki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Nagano',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Nagasaki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Nara',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Niigata',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Okayama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Okinawa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Osaka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Saga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Saitama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Shiga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Shizuoka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Shimane',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Tiba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Tokyo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Tokushima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Tochigi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Tottori',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Toyama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Fukui',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Fukuoka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Fukushima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Hiroshima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Hokkaido',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Hyogo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Yoshimi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Yamagata',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Yamaguchi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 124,
                'name' => 'Yamanashi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Jersey
            [
                'country' => 125,
                'name' => 'Saint Helier',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Jordania
            [
                'country' => 126,
                'name' => 'Jordan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Kazajistán
            [
                'country' => 127,
                'name' => 'Aktyubinskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Alma-Atinskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Vostochno-Kazahstanskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Gurevskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Zhambylskaya obl. Dzhambulskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Dzhezkazganskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Karagandinskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kzyl-Ordinskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kokchetavskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kustanaiskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Mangystauskaya Mangyshlakskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Pavlodarskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Severo-Kazahstanskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Taldy-Kurganskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Turgaiskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Akmolinskaya obl. Tselinogradskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Chimkentskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Littoral',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Southwest Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'North',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Central',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Government controlled area',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Turkish controlled area',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Issik Kulskaya Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kyrgyzstan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Narinskaya Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Oshskaya Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tallaskaya Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'al-Jahra',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'al-Kuwait',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Latviya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tarabulus',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Bengasi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Litva',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Moldova',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Auckland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Bay of Plenty',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Canterbury',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Gisborne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Hawkes Bay',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Manawatu-Wanganui',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Marlborough',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Nelson',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Northland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Otago',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Southland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Taranaki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tasman',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Waikato',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Wellington',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'West Coast',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Saint-Denis',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Altaiskii krai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Amurskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Arhangelskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Astrahanskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Bashkiriya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Belgorodskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Bryanskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Buryatiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Vladimirskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Volgogradskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Vologodskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Voronezhskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Nizhegorodskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Dagestan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Evreiskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Ivanovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Irkutskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kabardino-Balkariya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kaliningradskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tverskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kalmykiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kaluzhskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kamchatskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kareliya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kemerovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kirovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Komi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kostromskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Krasnodarskii krai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Krasnoyarskii krai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kurganskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kurskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Lipetskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Magadanskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Marii El',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Mordoviya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Moscow amp Moscow Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Murmanskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Novgorodskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Novosibirskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Omskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Orenburgskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Orlovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Penzenskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Permskiy krai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Primorskii krai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Pskovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Rostovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Ryazanskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Samarskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Saint-Petersburg and Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Saratovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Saha (Yakutiya)',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Sahalin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Sverdlovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Severnaya Osetiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Smolenskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Stavropolskii krai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tambovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tatarstan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tomskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tulskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tyumenskaya obl. i Hanty-Mansiiskii AO',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Udmurtiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Ulyanovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Uralskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Habarovskii krai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Chelyabinskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Checheno-Ingushetiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Chitinskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Chuvashiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Yaroslavskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Ahuachapán',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Cuscatlán',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'La Libertad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'La Paz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'La Unión',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'San Miguel',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'San Salvador',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Santa Ana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Sonsonate',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Paramaribo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Gorno-Badakhshan Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kuljabsk Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kurgan-Tjube Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Sughd Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tajikistan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Ashgabat Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Krasnovodsk Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Mary Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tashauz Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Chardzhou Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Grand Turk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Bartin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Bayburt',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Karabuk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Adana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Aydin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Amasya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Ankara',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Antalya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Artvin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Afion',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Balikesir',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Bilecik',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Bursa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Gaziantep',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Denizli',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Izmir',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Isparta',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Icel',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kayseri',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kars',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kodjaeli',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Konya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kirklareli',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kutahya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Malatya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Manisa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Sakarya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Samsun',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Sivas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Istanbul',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Trabzon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Corum',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Edirne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Elazig',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Erzincan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Erzurum',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Eskisehir',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Jinja',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kampala',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Andijon Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Buxoro Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Jizzac Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Qaraqalpaqstan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Qashqadaryo Región',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Navoiy Región',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Namangan Región',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Samarkand Región',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Surxondaryo Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Sirdaryo Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tashkent Región',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Fergana Región',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Xorazm Región',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Vinnitskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Volynskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Dnepropetrovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Donetskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Zhitomirskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Zakarpatskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Zaporozhskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Ivano-Frankovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kievskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kirovogradskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Krymskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Luganskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Lvovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Nikolaevskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Odesskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Poltavskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Rovenskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Sumskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Ternopolskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Harkovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Hersonskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Hmelnitskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Cherkasskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Chernigovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Chernovitskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Estoniya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Cheju',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Chollabuk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Chollanam',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Chungcheongbuk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Chungcheongnam',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Incheon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kangweon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kwangju',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kyeonggi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kyeongsangbuk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kyeongsangnam',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Pusan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Seoul',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Taegu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Taejeon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Ulsan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Aichi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Akita',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Aomori',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Wakayama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Gifu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Gunma',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Ibaraki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Iwate',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Ishikawa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kagawa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kagoshima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kanagawa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kyoto',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kochi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kumamoto',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Mie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Miyagi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Miyazaki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Nagano',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Nagasaki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Nara',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Niigata',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Okayama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Okinawa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Osaka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Saga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Saitama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Shiga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Shizuoka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Shimane',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tiba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tokyo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tokushima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tochigi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Tottori',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Toyama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Fukui',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Fukuoka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Fukushima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Hiroshima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Hokkaido',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Hyogo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Yoshimi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Yamagata',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Yamaguchi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Yamanashi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Hong Kong',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Indonesia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Jordan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Malaysia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Singapore',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Taiwan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Kazahstan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Ukraina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'India',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Egypt',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Damascus',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Isle of Man',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 127,
                'name' => 'Zapadno-Kazahstanskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Kenia
            [
                'country' => 128,
                'name' => 'Central',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 128,
                'name' => 'Coast',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 128,
                'name' => 'Eastern',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 128,
                'name' => 'Nairobi Area',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 128,
                'name' => 'North-Eastern',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 128,
                'name' => 'Nyanza',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 128,
                'name' => 'Rift Valley',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 128,
                'name' => 'Western',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Kirguistán
            [
                'country' => 129,
                'name' => 'Issik Kulskaya Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 129,
                'name' => 'Kyrgyzstan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 129,
                'name' => 'Narinskaya Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 129,
                'name' => 'Oshskaya Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 129,
                'name' => 'Tallaskaya Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Kiribati
            [
                'country' => 130,
                'name' => 'Gilbert Islands',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 130,
                'name' => 'Line Islands',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 130,
                'name' => 'Phoenix Islands',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Kuwuait
            [
                'country' => 131,
                'name' => 'al-Jahra',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 131,
                'name' => 'al-Kuwait',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Líbano
            [
                'country' => 132,
                'name' => 'Beirut',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Libia
            [
                'country' => 137,
                'name' => 'Tarabulus',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 137,
                'name' => 'Bengasi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Liechtenstein
            [
                'country' => 138,
                'name' => 'Balzers',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 138,
                'name' => 'Eschen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 138,
                'name' => 'Gamprin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 138,
                'name' => 'Mauren',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 138,
                'name' => 'Planken',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 138,
                'name' => 'Ruggell',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 138,
                'name' => 'Schaan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 138,
                'name' => 'Schellenberg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 138,
                'name' => 'Triesen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 138,
                'name' => 'Triesenberg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 138,
                'name' => 'Vaduz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Luxemburgo
            [
                'country' => 140,
                'name' => 'Diekirch',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 140,
                'name' => 'Grevenmacher',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 140,
                'name' => 'Luxembourg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // México
            [
                'country' => 141,
                'name' => 'Aguascalientes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Baja California',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Baja California Sur',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Campeche',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Chiapas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Chihuahua',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Coahuila de Zaragoza',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Colima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Distrito Federal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Durango',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Guanajuato',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Guerrero',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Hidalgo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Jalisco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'México',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Michoacán de Ocampo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Morelos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Nayarit',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Nuevo León',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Oaxaca',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Puebla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Querétaro de Arteaga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Quintana Roo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'San Luis Potosí',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Sinaloa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Sonora',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Tabasco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Tamaulipas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Tlaxcala',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Veracruz-Llave',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Yucatán',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 141,
                'name' => 'Zacatecas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Mónaco
            [
                'country' => 142,
                'name' => 'La Condamine',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 142,
                'name' => 'Monaco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 142,
                'name' => 'Monte-Carlo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Macedônia
            [
                'country' => 144,
                'name' => 'Aracinovo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Bac',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Belcista',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Berovo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Bistrica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Bitola',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Blatec',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Bogdanci',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Bogomila',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Bogovinje',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Bosilovo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Cair',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Capari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Caska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Cegrane',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Centar Zupa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Debar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Delcevo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Demir Hisar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Demir Kapija',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Dorce Petrov',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Gazi Baba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Gevgelija',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Gostivar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Gradsko',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Jegunovce',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Kamenjane',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Karpos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Kavadarci',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Kicevo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Kisela Voda',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Klecevce',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Kocani',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Kondovo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Kratovo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Krivogastani',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Krusevo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Kumanovo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Labunista',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Lipkovo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Makedonska Kamenica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Makedonski Brod',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Murtino',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Negotino',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Novo Selo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Ohrid',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Orizari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Petrovec',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Prilep',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Probistip',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Radovis',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Resen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Rosoman',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Saraj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Srbinovo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Star Dojran',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Stip',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Struga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Strumica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Studenicani',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Suto Orizari',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Sveti Nikole',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Tearce',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Tetovo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Valandovo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Veles',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Vevcani',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Vinica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Vrapciste',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Zelino',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 144,
                'name' => 'Zrnovci',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Marruecos
            [
                'country' => 150,
                'name' => 'Morocco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 150,
                'name' => 'Tangier',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Martinica
            [
                'country' => 151,
                'name' => 'Fort-de-France',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Martinica
            [
                'country' => 151,
                'name' => 'Fort-de-France',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Mongolia
            [
                'country' => 157,
                'name' => 'Ulan Bator',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Monte Negro
            [
                'country' => 158,
                'name' => 'Belgrade',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Nicaragua
            [
                'country' => 164,
                'name' => 'Boaco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 164,
                'name' => 'Carazo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 164,
                'name' => 'Chinandega',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 164,
                'name' => 'Chontales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 164,
                'name' => 'Estelí',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 164,
                'name' => 'Granada',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 164,
                'name' => 'Jinotega',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 164,
                'name' => 'León',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 164,
                'name' => 'Madriz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 164,
                'name' => 'Managua',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 164,
                'name' => 'Masaya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 164,
                'name' => 'Matagalpa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 164,
                'name' => 'Nueva Segovia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 164,
                'name' => 'Rio San Juan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 164,
                'name' => 'Rivas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 164,
                'name' => 'Zelaya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Noruega
            [
                'country' => 168,
                'name' => 'Akershus',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Aust-Agder',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Buskerud',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Finnmark',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Hedmark',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Hordaland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'More og Romsdal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Nordland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Nord-Trondelag',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Oppland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Oslo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Ostfold',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Rogaland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Sogn og Fjordane',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Sor-Trondelag',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Telemark',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Troms',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Vest-Agder',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 168,
                'name' => 'Vestfold',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Nueva Caledonia
            [
                'country' => 169,
                'name' => 'Noumea',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Nueva Zelanda
            [
                'country' => 170,
                'name' => 'Auckland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 170,
                'name' => 'Bay of Plenty',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 170,
                'name' => 'Canterbury',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 170,
                'name' => 'Gisborne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 170,
                'name' => 'Hawkes Bay',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 170,
                'name' => 'Manawatu-Wanganui',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 170,
                'name' => 'Marlborough',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 170,
                'name' => 'Nelson',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 170,
                'name' => 'Northland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 170,
                'name' => 'Otago',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 170,
                'name' => 'Southland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 170,
                'name' => 'Taranaki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 170,
                'name' => 'Tasman',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 170,
                'name' => 'Waikato',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 170,
                'name' => 'Wellington',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 170,
                'name' => 'West Coast',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Países Bajos
            [
                'country' => 172,
                'name' => 'Drenthe',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 172,
                'name' => 'Friesland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 172,
                'name' => 'Gelderland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 172,
                'name' => 'Groningen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 172,
                'name' => 'Limburg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 172,
                'name' => 'Noord-Brabant',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 172,
                'name' => 'Noord-Holland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 172,
                'name' => 'Utrecht',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 172,
                'name' => 'Zeeland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 172,
                'name' => 'Zuid-Holland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 172,
                'name' => 'Overijssel',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 172,
                'name' => 'Flevoland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Panamá
            [
                'country' => 176,
                'name' => 'Bocas del Toro',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 176,
                'name' => 'Chiriquí',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 176,
                'name' => 'Coclé',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 176,
                'name' => 'Colón',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 176,
                'name' => 'Darién',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 176,
                'name' => 'Herrera',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 176,
                'name' => 'Los Santos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 176,
                'name' => 'Panamá',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 176,
                'name' => 'San Blas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 176,
                'name' => 'Veraguas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Paraguay
            [
                'country' => 178,
                'name' => 'Alto Paraná',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Amambay',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Boquerón',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Caaguazuacute',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Caazapá',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Central',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Concepción',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Cordillera',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Guairá',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Itapuacutea',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Misiones',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Neembucuacute',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Paraguarí',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Presidente Hayes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'San Pedro',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Alto Paraguay',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Canindeyuacute',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 178,
                'name' => 'Chaco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Perú
            [
                'country' => 179,
                'name' => 'Amazonas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Ancash',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Apurímac',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Arequipa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Ayacucho',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Cajamarca',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Callao',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Cusco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Huancavelica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Huánuco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Ica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Junín',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'La Libertad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Lambayeque',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Lima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Loreto',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Madre de Dios',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Moquegua',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Pasco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Piura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Puno',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'San Martín',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Tacna',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Tumbes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 179,
                'name' => 'Ucayali',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Polinesia Francesa
            [
                'country' => 180,
                'name' => 'Papeete',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Polonia
            [
                'country' => 181,
                'name' => 'Biala Podlaska',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Bialystok',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Bielsko',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Bydgoszcz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Chelm',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Ciechanow',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Czestochowa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Elblag',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Gdansk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Gorzow',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Jelenia Gora',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Kalisz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Katowice',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Kielce',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Konin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Koszalin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Krakow',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Krosno',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Legnica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Leszno',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Lodz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Lomza',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Lublin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Nowy Sacz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Olsztyn',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Opole',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Ostroleka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Pila',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Piotrkow',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Plock',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Poznan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Przemysl',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Radom',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Rzeszow',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Siedlce',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Sieradz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Skierniewice',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Slupsk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Suwalki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Tarnobrzeg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Tarnow',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Torun',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Walbrzych',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Warszawa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Wloclawek',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Wroclaw',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Zamosc',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Zielona Gora',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Dolnoslaskie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Kujawsko-Pomorskie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Lodzkie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Lubelskie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Lubuskie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Malopolskie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Mazowieckie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Opolskie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Podkarpackie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Podlaskie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Pomorskie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Slaskie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Swietokrzyskie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Warminsko-Mazurskie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Wielkopolskie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 181,
                'name' => 'Zachodniopomorskie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Portugal
            [
                'country' => 182,
                'name' => 'Aveiro',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Beja',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Braga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Braganca',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Castelo Branco',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Coimbra',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Evora',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Faro',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Madeira',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Guarda',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Leiria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Lisboa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Portalegre',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Porto',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Santarem',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Setubal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Viana do Castelo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Vila Real',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Viseu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 182,
                'name' => 'Azores',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Qatar
            [
                'country' => 184,
                'name' => 'Doha',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Reino Unido
            [
                'country' => 185,
                'name' => 'Scotland North',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'England - East',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'England - West Midlands',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'England - South West',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'England - North West',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'England - Yorks amp Humber',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'England - South East',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'England - London',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'Northern Ireland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'England - North East',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'Wales South',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'Wales North',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'England - East Midlands',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'Scotland Central',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'Scotland South',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'Channel Islands',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 185,
                'name' => 'Isle of Man',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // República Checa
            [
                'country' => 187,
                'name' => 'Hlavni Mesto Praha',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 187,
                'name' => 'Jihomoravsky Kraj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 187,
                'name' => 'Jihocesky Kraj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 187,
                'name' => 'Vysocina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 187,
                'name' => 'Karlovarsky Kraj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 187,
                'name' => 'Kralovehradecky Kraj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 187,
                'name' => 'Liberecky Kraj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 187,
                'name' => 'Olomoucky Kraj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 187,
                'name' => 'Moravskoslezsky Kraj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 187,
                'name' => 'Pardubicky Kraj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 187,
                'name' => 'Plzensky Kraj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 187,
                'name' => 'Stredocesky Kraj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 187,
                'name' => 'Ustecky Kraj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 187,
                'name' => 'Zlinsky Kraj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // República Dominicana
            [
                'country' => 188,
                'name' => 'Santo Domingo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Reunión
            [
                'country' => 190,
                'name' => 'Saint-Denis',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Rumania
            [
                'country' => 192,
                'name' => 'Alba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Arad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Arges',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Bacau',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Bihor',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Bistrita-Nasaud',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Botosani',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Braila',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Brasov',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Bucuresti',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Buzau',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Caras-Severin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Cluj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Constanta',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Covasna',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Dambovita',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Dolj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Galati',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Gorj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Harghita',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Hunedoara',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Ialomita',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Iasi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Maramures',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Mehedinti',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Mures',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Neamt',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Olt',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Prahova',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Salaj',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Satu Mare',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Sibiu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Suceava',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Teleorman',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Timis',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Tulcea',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Vaslui',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Valcea',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Vrancea',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Calarasi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 192,
                'name' => 'Giurgiu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Rusia
            [
                'country' => 193,
                'name' => 'Altaiskii krai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Amurskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Arhangelskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Astrahanskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Bashkiriya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Belgorodskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Bryanskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Buryatiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Vladimirskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Volgogradskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Vologodskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Voronezhskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Nizhegorodskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Dagestan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Evreiskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Ivanovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Irkutskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Kabardino-Balkariya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Kaliningradskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Tverskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Kalmykiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Kaluzhskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Kamchatskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Kareliya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Kemerovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Kirovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Komi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Kostromskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Krasnodarskii krai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Krasnoyarskii krai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Kurganskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Kurskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Lipetskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Magadanskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Marii El',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Mordoviya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Moscow amp Moscow Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Murmanskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Novgorodskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Novosibirskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Omskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Orenburgskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Orlovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Penzenskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Permskiy krai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Primorskii krai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Pskovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Rostovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Ryazanskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Samarskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Saint-Petersburg and Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Saratovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Saha Yakutiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Sahalin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Sverdlovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Severnaya Osetiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Smolenskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Stavropolskii krai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Tambovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Tatarstan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Tomskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Tulskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Tyumenskaya obl. i Hanty-Mansiiskii AO',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Udmurtiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Ulyanovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Uralskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Habarovskii krai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Chelyabinskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Checheno-Ingushetiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Chitinskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Chuvashiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Yaroslavskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Adygeya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Hakasiya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Chukotskii AO',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Yamalo-Nenetskii AO',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Karachaeva-Cherkesskaya Respublica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Raimirskii Dolgano-Nenetskii AO',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 193,
                'name' => 'Respublica Tiva',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // San Cristobal y Nieves
            [
                'country' => 198,
                'name' => 'Basseterre',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // San Marino
            [
                'country' => 199,
                'name' => 'Acquaviva',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 199,
                'name' => 'Chiesanuova',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 199,
                'name' => 'Domagnano',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 199,
                'name' => 'Faetano',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 199,
                'name' => 'Fiorentino',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 199,
                'name' => 'Borgo Maggiore',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 199,
                'name' => 'San Marino',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 199,
                'name' => 'Monte Giardino',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 199,
                'name' => 'Serravalle',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // San Martín (Francia)
            [
                'country' => 200,
                'name' => 'Kingstown',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Santa Elena
            [
                'country' => 203,
                'name' => 'Jamestown',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Santa Lucía
            [
                'country' => 204,
                'name' => 'Castries',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Sierra Leona
            [
                'country' => 209,
                'name' => 'Freetown',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Siria
            [
                'country' => 212,
                'name' => 'Damascus',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Somalia
            [
                'country' => 213,
                'name' => 'Mogadishu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Sudáfrica
            [
                'country' => 215,
                'name' => 'Johannesburg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Suecia
            [
                'country' => 217,
                'name' => 'Blekinge Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Gavleborgs Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Gotlands Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Hallands Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Jamtlands Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Jonkopings Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Kalmar Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Dalarnas Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Kronobergs Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Norrbottens Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Orebro Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Ostergotlands Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Sodermanlands Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Uppsala Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Varmlands Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Vasterbottens Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Vasternorrlands Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Vastmanlands Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Stockholms Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Skane Lan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 217,
                'name' => 'Vastra Gotaland',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Suiza
            [
                'country' => 218,
                'name' => 'Appenzell Ausserrhoden',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Bern',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Basel-Landschaft',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Basel-Stadt',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Fribourg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Genegraveve',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Glarus',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Graubünden',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Jura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Luzern',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Neuchacirctel',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Nidwalden',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Obwalden',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Sankt Gallen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Schaffhausen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Solothurn',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Schwyz',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Thurgau',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Ticino',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Uri',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Vaud',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Valais',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Zug',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 218,
                'name' => 'Zürich',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Surinám
            [
                'country' => 219,
                'name' => 'Paramaribo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Tayikistán
            [
                'country' => 222,
                'name' => 'Gorno-Badakhshan Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 222,
                'name' => 'Kuljabsk Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 222,
                'name' => 'Kurgan-Tjube Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 222,
                'name' => 'Sughd Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 222,
                'name' => 'Tajikistan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Tailandia
            [
                'country' => 223,
                'name' => 'Amnat Charoen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Ang Thong',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Bangkok',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Buri Ram',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Chachoengsao',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Chai Nat',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Chaiyaphum',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Chanthaburi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Chiang Mai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Chiang Rai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Chon Buri',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Kalasin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Kanchanaburi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Khon Kaen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Krabi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Lampang',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Loei',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Lop Buri',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Mae Hong Son',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Maha Sarakham',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Nakhon Pathom',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Nakhon Ratchasima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Nakhon Sawan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Nakhon Si Thammarat',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Narathiwat',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Nong Bua Lam Phu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Nong Khai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Nonthaburi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Pathum Thani',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Pattani',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Phangnga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Phatthalung',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Phichit',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Phitsanulok',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Phra Nakhon Si Ayutthaya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Phrae',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Phuket',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Prachin Buri',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Prachuap Khiri Khan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Ratchaburi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Rayong',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Roi Et',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Sa Kaeo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Sakon Nakhon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Samut Prakan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Samut Sakhon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Samut Songkhran',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Saraburi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Si Sa Ket',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Sing Buri',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Songkhla',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Sukhothai',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Suphan Buri',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Surat Thani',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Surin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Trang',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Ubon Ratchathani',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Udon Thani',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Uthai Thani',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Uttaradit',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Yala',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 223,
                'name' => 'Yasothon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Tokelau
            [
                'country' => 230,
                'name' => 'Fakaofo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Trinidad y Tobago
            [
                'country' => 232,
                'name' => 'Port of Spain',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Turkmenistán
            [
                'country' => 234,
                'name' => 'Ashgabat Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 234,
                'name' => 'Krasnovodsk Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 234,
                'name' => 'Mary Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 234,
                'name' => 'Tashauz Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 234,
                'name' => 'Chardzhou Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Turquía
            [
                'country' => 235,
                'name' => 'Bartin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Bayburt',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Karabuk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Adana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Aydin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Amasya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Ankara',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Antalya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Artvin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Afion',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Balikesir',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Bilecik',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Bursa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Gaziantep',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Denizli',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Izmir',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Isparta',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Icel',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Kayseri',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Kars',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Kodjaeli',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Konya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Kirklareli',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Kutahya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Malatya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Manisa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Sakarya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Samsun',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Sivas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Istanbul',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Trabzon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Corum',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Edirne',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Elazig',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Erzincan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Erzurum',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 235,
                'name' => 'Eskisehir',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Ucrania
            [
                'country' => 237,
                'name' => 'Vinnitskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Volynskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Dnepropetrovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Donetskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Zhitomirskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Zakarpatskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Zaporozhskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Ivano-Frankovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Kievskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Kirovogradskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Krymskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Luganskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Lvovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Nikolaevskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Odesskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Poltavskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Rovenskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Sumskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Ternopolskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Harkovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Hersonskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Hmelnitskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Cherkasskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Chernigovskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Chernovitskaya obl.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Estoniya',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Cheju',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Chollabuk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Chollanam',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Chungcheongbuk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Chungcheongnam',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Incheon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Kangweon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Kwangju',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Kyeonggi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Kyeongsangbuk',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Kyeongsangnam',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Pusan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Seoul',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Taegu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Taejeon',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Ulsan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Aichi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Akita',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Aomori',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Wakayama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Gifu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Gunma',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Ibaraki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Iwate',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Ishikawa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Kagawa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Kagoshima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Kanagawa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Kyoto',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Kochi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Kumamoto',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Mie',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Miyagi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Miyazaki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Nagano',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Nagasaki',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Nara',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Niigata',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Okayama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Okinawa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Osaka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Saga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Saitama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Shiga',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Shizuoka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Shimane',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Tiba',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Tokyo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Tokushima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Tochigi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Tottori',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Toyama',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Fukui',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Fukuoka',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Fukushima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Hiroshima',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Hokkaido',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Hyogo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Yoshimi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Yamagata',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Yamaguchi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Yamanashi',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Hong Kong',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Indonesia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Jordan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Malaysia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Singapore',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Taiwan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Kazahstan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 237,
                'name' => 'Ukraina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Uganda
            [
                'country' => 238,
                'name' => 'Jinja',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 238,
                'name' => 'Kampala',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Uruguay
            [
                'country' => 239,
                'name' => 'Artigas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Canelones',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Cerro Largo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Colonia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Durazno',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Flores',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Florida',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Lavalleja',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Maldonado',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Montevideo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Paysanduacute',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Río Negro',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Rivera',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Rocha',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Salto',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'San José',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Soriano',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Tacuarembó',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 239,
                'name' => 'Treinta y Tres',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Uzbekistán
            [
                'country' => 240,
                'name' => 'Andijon Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 240,
                'name' => 'Buxoro Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 240,
                'name' => 'Jizzac Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 240,
                'name' => 'Qaraqalpaqstan',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 240,
                'name' => 'Qashqadaryo Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 240,
                'name' => 'Navoiy Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 240,
                'name' => 'Namangan Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 240,
                'name' => 'Samarqand Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 240,
                'name' => 'Surxondaryo Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 240,
                'name' => 'Sirdaryo Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 240,
                'name' => 'Tashkent Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 240,
                'name' => 'Fergana Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 240,
                'name' => 'Xorazm Region',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Venezuela
            [
                'country' => 242,
                'name' => 'Amazonas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Anzoategui',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Apure',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Aragua',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Barinas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Bolívar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Carabobo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Cojedes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Delta Amacuro',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Falcón',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Guárico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Lara',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Mérida',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Miranda',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Monagas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Nueva Esparta',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Portuguesa',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Sucre',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Táchira',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Trujillo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Yaracuy',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Zulia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Dependencias Federales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Distrito Capital',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 242,
                'name' => 'Vargas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Vietnam
            [
                'country' => 243,
                'name' => 'Dong Bang Song Cuu Long',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 243,
                'name' => 'Dong Bang Song Hong',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 243,
                'name' => 'Dong Nam Bo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 243,
                'name' => 'Duyen Hai Mien Trung',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 243,
                'name' => 'Khu Bon Cu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 243,
                'name' => 'Mien Nui Va Trung Du',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'country' => 243,
                'name' => 'Thai Nguyen',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Wallis y Futuna
            [
                'country' => 244,
                'name' => 'Mata-Utu',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
        ]);
    }
}
