<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTypesSeeder::class);
        $this->call(LanguageLevelsSeeder::class);
        $this->call(AgeRangesSeeder::class);
        $this->call(ExperienceRangesSeeder::class);
        $this->call(CountriesSeeder::class);
        $this->call(CountryStatesSeeder::class);
        $this->call(StudyTypesSeeder::class);
        $this->call(SalaryExpectationsSeeder::class);
        $this->call(CompanyActivitiesSeeder::class);
        $this->call(DegreesSeeder::class);
        $this->call(SystemUserSeeder::class);
        $this->call(SystemPeopleSeeder::class);
        $this->call(PositionsSeeder::class);
        $this->call(SkillsSeeder::class);
        $this->call(LanguagesSeeder::class);
        /* $this->call(TalentsUsersSeeder::class);
        $this->call(ClientsSeeder::class);
        $this->call(PositionRequestsSeeder::class); */
    }
}
