<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DegreesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('degrees')->insert([
            // Primaria -> 1
            [
                'study_type_id' => 1,
                'degree' => 'Título de Educación Primaria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Secundaria -> 2
            [
                'study_type_id' => 2,
                'degree' => 'Bachiller',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 2,
                'degree' => 'Bachiller en Ciencias',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 2,
                'degree' => 'Bachiller en Humanidades',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 2,
                'degree' => 'Bachiller en Electrónica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 2,
                'degree' => 'Bachiller en Letras',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 2,
                'degree' => 'Bachiller en Artes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 2,
                'degree' => 'Bachiller en Leyes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 2,
                'degree' => 'Bachiller Tecnico en Computacion',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 2,
                'degree' => 'Bachiller en Administración de Empresas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 2,
                'degree' => 'Bachillerato Internacional',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Universitario -> 3
            [
                'study_type_id' => 3,
                'degree' => 'Administración',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Administración de Empresas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Administración de hostelería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Administración de Sistemas Informáticos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Administración en Salud',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Administración Pública',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Agricultura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Agronomía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Antropología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Archivología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Arquitectura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Artes ',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Artes Escénicas y Audiovisuales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Artes Liberales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Artes Plásticas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Bellas Artes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Bibliotecología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Bioanálisis',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Bioinformática',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Biología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Biología Humana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Bioquímica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Biotecnología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ciencia de Datos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ciencia de Materiales Sustentables',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ciencias Actuariales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ciencias de Datos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ciencias de la Tierra',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ciencias Estadísticas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ciencias Forense',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ciencias Policiales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ciencias Políticas     ',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ciencias Políticas y Administrativas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ciencias Sociales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Cinematografía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Citotecnología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Comercio Internacional',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Computación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Comunicación Social',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Contabilidad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Contabilidad y gestión de empresas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Contaduría Internacional',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Contaduría Pública',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Criminología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Derecho',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Diseño Gráfico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ecología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Economía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Educación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Educación Biología y Química',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Educación Ciencias Pedagógicas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Educación Ciencias Sociales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Educación Filosofía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Educación Integral y Preescolar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Enfermería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Enfermería (Técnica)',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Estudios Internacionales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Estudios Latinoamericanos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Estudios Liberales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Farmacia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Filosofía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Física',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Física Biomédica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Fisioterapia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Geografía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Gestión de la Hospitalidad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Gobierno Y Asuntos Públicos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Historia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Idiomas Modernos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Información de la Salud',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería Aeroespacial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería Agronómica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería Civil',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería de Mantenimiento',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería de Minas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería de Petróleo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería de Producción',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería Eléctrica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería en Energías Renovables',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería en Informática',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería en Materiales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería en Organización Industrial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería en Procesos Industriales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería en Sistemas Biomédicos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería en Telecomunicaciones',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería Forestal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería Geodésica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería Geofísica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería Hidrometeorológica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería Industrial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería Mecánica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería Mecatrónica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ingeniería Química',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Inspección de Salud Pública',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Lengua y Literatura Modernas Alemanas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Lengua y Literatura Modernas Francesas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Lengua y Literatura Modernas Inglesas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Lengua y Literatura Modernas Italianas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Lengua y Literatura Modernas Portuguesas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Letras',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Marketing',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Matemáticas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Matemáticas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Matemáticas Aplicadas y Computación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Medicina',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Medicina Pre Hospitalaria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Medicina Veterinaria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Museología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Música',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Nanotecnología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Neurociencias',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Nutrición y Dietética',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Odontología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Optometrista',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Ortesis y Prótesis',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Psicología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Psicopedagogía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Publicidad y Mercadeo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Química',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Química de Alimentos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Química Industrial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Radiología e Imagenología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Relaciones Internacionales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Relaciones Laborales y Recursos Humanos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Sociología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Tecnología ',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Tecnología Cardiopulmonar',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Teología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Terapia Ocupacional',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Trabajo Social',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Traducción',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Turismo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 3,
                'degree' => 'Urbanismo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Especializaciones -> 4
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Arquitectura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialidad en Ginecología y Obstetricia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Arquitectura - Diseño Urbano',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Arquitectura - Museología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Arquitectura Planificación de Transporte',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Aseguramiento de la Calidad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Botánica Agrícola',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Ciencia y Tecnología Cosmética',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Ciencias Contables',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Ciencias del Suelo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Ciencias Penales y Criminológicas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Cirugía Bucal y Maxilofacial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Comunicaciones Integradas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Derecho Administrativo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Derecho Corporativo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Derecho de Menores',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Derecho Internacional Económico',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Derecho Mercantil',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Derecho Tributario',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Derecho y el Comercio Exterior',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Derecho y Política Internacional',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Derechos Humanos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Dermatología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Economía Urbana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Estadística',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Estadística',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Finanzas de Empresas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Gerencia Tributaria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Gobierno y Política Pública',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Hematología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Medicina Interna',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Mercadeo para Empresas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Organización de Empresas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Política y Comercio Petrolero Internacional',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Producción Animal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Publicidad',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Zoología Agrícola',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Toxicología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista Farmacia Hospitalaria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista Financiero',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Administración en Recursos Humanos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Análisis de Datos en Ciencias Sociales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Auditoria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Derecho Procesal',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Farmacia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Gerencia de Sistemas de Calidad y Control de Estados Producción',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Mercadeo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Pediatría',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Moneda e Instituciones Financieras',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Procesos y Sistemas Electorales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Neurología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Relaciones Internacionales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 4,
                'degree' => 'Especialista en Responsabilidad Social Empresarial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Maestrías -> 5
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Administración',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Administración de Empresas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Agronomía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Arquitectura ',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Arquitectura - Diseño Urbano',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Botánica Agrícola',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Cirugía General',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Cirugía Oncológica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Cirugía Pediátrica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Conservación y restauración de Monumentos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Derecho Internacional',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Desarrollo Rural',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Economía  Aplicada',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Economía  Internacional',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Entomología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Estadística',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Estudios de la Mujer',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Estudios Políticos y de Gobierno',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Farmacología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Finanzas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Gerencia Empresarial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Gerencia Pública',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Gestión de Investigación y Desarrollo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Idiomas Modernos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Ingeniería Agrícola',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Ingeniería Gerencial',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Literatura',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Mercadeo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Moneda e Instituciones Financieras',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Nefrología de Adultos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Planificación Urbana',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Química de Medicamentos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en relaciones Internacionales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Teoría y Política Económica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Toxicología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 5,
                'degree' => 'Maestría en Zoología Agrícola',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            // Doctorados -> 6
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Arquitectura ',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Ciencias Agrícolas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Ciencias del Suelo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado Zoología Agrícola',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Ciencias',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Ciencias Farmacéuticas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Ciencias Sociales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Derecho y Ciencias Políticas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Economía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Estadística',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Farmacología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Gerencia',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Gestión de Investigación y Desarrollo',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Nanotecnología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Química de Medicamentos',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Seguridad Social',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado Ciencia de la Computación',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado Biología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado Física',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado Teología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado Atención a la Salud Mental',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado Economía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado Psicología',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado Matemáticas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado Química',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado Ingeniería Mecánica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado Ingeniería',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado Filosofía',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado Ingeniería Eléctrica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado Ciencia de los Materiales',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Salud Pública',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Investigación Clínica',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'study_type_id' => 6,
                'degree' => 'Doctorado en Ciencias Biomédicas',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
        ]);
    }
}
