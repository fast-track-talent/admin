<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExperienceRangesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('experience_ranges')->insert([
            [
                'exp_range' => 'Menos de 1',
                'min' => null,
                'max' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'exp_range' => 'De 1 a 3',
                'min' => 1,
                'max' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'exp_range' => 'De 3 a 5',
                'min' => 3,
                'max' => 5,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'exp_range' => 'Más de 5',
                'min' => 5,
                'max' => null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ]
        ]);
    }
}
