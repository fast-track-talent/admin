<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PositionRequest;

class PositionRequestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PositionRequest::factory()->count(950)->create();
    }
}
