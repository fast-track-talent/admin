<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SalaryExpectationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('salary_expectations')->insert([
            [
                'range' => 'Menos de $500',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'range' => 'Entre $500 y $1000',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'range' => 'Entre $1000 y $1500',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'range' => 'Entre $1500 y $2000',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'range' => 'Más de $2000',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
        ]);
    }
}
