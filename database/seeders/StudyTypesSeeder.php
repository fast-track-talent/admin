<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudyTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('study_types')->insert([
            [
                'name' => 'Primaria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'name' => 'Secundaria',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'name' => 'Universitario',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'name' => 'Especializaciones',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'name' => 'Maestrías',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'name' => 'Doctorados',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ]
        ]);
    }
}