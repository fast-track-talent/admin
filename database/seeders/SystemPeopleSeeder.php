<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SystemPeopleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('people')->insert([
            [
                'user_id' => 1,
                'firstnames' => 'Francisco',
                'lastnames' => 'Rivera',
                'gender' => 'M',
                'birthdate' => '1975-05-15',
                'country' => 1
            ],
            [
                'user_id' => 2,
                'firstnames' => 'Ernesto',
                'lastnames' => 'Murrugarra',
                'gender' => 'M',
                'birthdate' => '1990-02-22',
                'country' => 1
            ]
        ]);
    }
}
