<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SystemUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'user_type_id' => 1,
                'email' => 'francisco@fttalent.work',
                'password' => bcrypt('123456789'),
                'country_code' => 1,
                'phone_number' => '4145552258'
            ],
            [
                'user_type_id' => 1,
                'email' => 'ekodevs.admins@fttalent.work',
                'password' => bcrypt('123456789'),
                'country_code' => 1,
                'phone_number' => '4125447874'
            ]
        ]);
    }
}
