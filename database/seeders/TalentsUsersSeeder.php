<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class TalentsUsersSeeder extends Seeder
{
   

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::newFactory('talent')->count(4500)->create();
    }
}
