<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_types')->insert([
            [
                'name' => 'Admins',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'name' => 'Client',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ],
            [
                'name' => 'Talent',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ]
        ]);
    }
}
