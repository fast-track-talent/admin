require('./bootstrap');

// Import modules...
window.Vue = require('vue').default;

import Vue from 'vue';
import { App as InertiaApp, plugin as InertiaPlugin } from '@inertiajs/inertia-vue';
import Vuetify from 'vuetify';
import Es from 'vuetify/es5/locale/es';

import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
Vue.use(VueSweetalert2);

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';

Vue.use(VueToast);

import Vuex from 'vuex';

import storeData from "./store/index";

import vueDebounce from 'vue-debounce';
Vue.use(vueDebounce);

const el = document.getElementById('app');

Vue.use(Vuex);

const store = new Vuex.Store(
    storeData
);

Vue.mixin({ methods: { route }});
Vue.use(InertiaPlugin)
Vue.use(Vuetify)

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

new Vue({
  vuetify: new Vuetify({
    lang: {
      locales: { Es },
      current: 'Es',
    },
    theme: {
      themes: {
        light: {
          primary   : '#2fe3b4',
          secondary : '#23272f'

        },
        dark: {
          primary   : '#2fe3b4',
          secondary : '#23272f'
        },
      },
    },
  }),
  store,
  render: h => h(InertiaApp, {
  props: {
    initialPage: JSON.parse(el.dataset.page),
    resolveComponent: name => require(`./Pages/${name}`).default,
  },
  }),
}).$mount(el)

function onReady(callback) {
  var intervalId = window.setInterval(function() {
    if (document.getElementsByTagName('body')[0] !== undefined) {
      window.clearInterval(intervalId);
      callback.call(this);
    }
  }, 1000);
}

function setVisible(selector, visible) {
  document.querySelector(selector).style.display = visible ? 'block' : 'none';
}

onReady(function() {
  setVisible('#loader-wrapper', false);
});
