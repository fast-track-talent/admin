<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="icon" type="image/png" href="{!! asset('img/icon.png') !!}"/>
      <title>FTT Admin</title>

      {{-- Icons --}}
      <link href="https://cdn.jsdelivr.net/npm/@mdi/font@6.x/css/materialdesignicons.min.css" rel="stylesheet">

      <!-- Fonts -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

      <!-- Styles -->
      <link rel="stylesheet" href="{{ mix('css/app.css') }}">

      <!-- Scripts -->
      @routes
      <script src="{{ mix('js/app.js') }}" defer></script>
  </head>
  <body class="font-sans antialiased">
    <div id="loader-wrapper">
      <div id="loader"></div>
    </div>
    @inertia

    @env ('local')
      <script src="http://localhost:3000/browser-sync/browser-sync-client.js"></script>
    @endenv
  </body>
</html>
