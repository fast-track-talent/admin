@component('mail::layout')
    {{-- Header --}}
    @slot ('header')
        @component('mail::header', ['url' => 'https://ftt-web.ekodevs.com'])
            <!-- header -->
            <img class="logo" src="{{ env('APP_URL') }}/img/logo.png" width="350" alt="Fast Track Talent Logo" />
        @endcomponent
    @endslot
    {{-- Content here --}}
    <h1>{{ $details['title'] }}</h1>
    @if ($details['forAdmins'])
        <p>El usuario <b>{{ $details['personalData']->lastnames }}, {{ $details['personalData']->firstnames }}</b> con el correo <b>{{ $details['userData']->email }}</b>, quien es el contacto de la empresa <b>{{ $details['client']->name }}</b> ha culminado el asistente de petición de talentos, donde solicitó los siguientes cargos:</p>
        <ul>
            @foreach ($details['positionRequests'] as $posReq)
                <li>{{ $posReq->position->position }}: {{ $posReq->title }}</li>
            @endforeach
        </ul>
        <p>Puedes ver más información al respecto en la <b>aplicación administrativa de Fast Track Talent</b></p>
    @elseif (!$details['forAdmins'])
        <p>¡Bienvenido a Fast Track Talent!</p>
        <p>Hola estimado/a Sr/Sra {{ $details['personalData']->firstnames }} {{ $details['personalData']->lastnames }}, gracias por confiar en nosotros. Un reclutador estará revisando a detalle su solicitud de los siguientes de cargos:</p>
        <ul>
            @foreach ($details['positionRequests'] as $posReq)
                <li>{{ $posReq->position->position }}: {{ $posReq->title }}</li>
            @endforeach
        </ul>
        <p>En las próximas 48 horas, un agente de Fast Track Talent se pondrá en contacto con usted para iniciar formalmente el proceso de selección.</p>

    @endif
    {{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            <!-- subcopy -->
            Este correo ha sido enviado automáticamente, por favor no responda ni reenvíe mensajes a esta dirección. Si desea ponerse en contacto con nosotros, puede escribirnos a contact@fttalent.work
        @endcomponent
    @endslot

    {{-- Footer --}}
    @slot ('footer')
        @component('mail::footer')
            <!-- footer -->
            Fast Track Talent, C.A.
            Rif: J 29875567-3
        @endcomponent
    @endslot
@endcomponent