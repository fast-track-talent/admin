@component('mail::layout')
    {{-- Header --}}
    @slot ('header')
        @component('mail::header', ['url' => 'https://ftt-web.ekodevs.com'])
            <!-- header -->
            <img class="logo" src="{{ env('APP_URL') }}/img/logo.png" width="350" alt="Fast Track Talent Logo" />
        @endcomponent
    @endslot
    {{-- Content here --}}
    <h1>{{ $details['title'] }}</h1>
    @if ($details['forAdmins'])
        <p>El usuario <b>{{ $details['personalData']->lastnames }}, {{ $details['personalData']->firstnames }}</b> con el correo <b>{{ $details['userData']->email }}</b>, ha culminado la entrevista para optar por un empleo.</p>
        <p>Ya puedes ver sus datos en la <b>aplicación administrativa de Fast Track Talent</b></p>
    @elseif (!$details['forAdmins'])
        <p>¡Bienvenido a Fast Track Talent!</p>
        <p>Hola {{ $details['personalData']->firstnames }}, gracias por confiar en nosotros. En los próximos días, un reclutador estará revisando a detalle tu perfil profesional. Cuando tengamos una oferta hecha a tu medida, un agente se pondrá en contacto contigo.</p>
    @endif
    {{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            <!-- subcopy -->
            Este correo ha sido enviado automáticamente, por favor no respondas ni reenvíes mensajes a esta dirección. Si deseas ponerte en contacto con nosotros, escríbenos a contact@fttalent.work
        @endcomponent
    @endslot

    {{-- Footer --}}
    @slot ('footer')
        @component('mail::footer')
            <!-- footer -->
            Fast Track Talent, C.A.
            Rif: J 29875567-3
        @endcomponent
    @endslot
@endcomponent