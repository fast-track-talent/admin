@component('mail::layout')
    {{-- Header --}}
    @slot ('header')
        @component('mail::header', ['url' => 'https://ftt-web.ekodevs.com'])
            <!-- header -->
            <img class="logo" src="{{ env('APP_URL') }}/img/logo.png" width="350" alt="Fast Track Talent Logo" />
        @endcomponent
    @endslot

    {{-- Content here --}}

    <h1>{{ $details['title'] }}</h1>
    <p>Detalles:</p>
    <ul>
        <li><b>Nombre:</b> {{ $details['fullname'] }}</li>
        <li><b>Correo:</b> {{ $details['email'] }}</li>
        <li><b>Asunto:</b> {{ $details['category'] }}</li>
        <li><b>Mensaje:</b> {{ $details['message'] }}</li>
    </ul>
    <br>
    <p>¡Respóndele lo más rápido que puedas!</p>

    {{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            <!-- subcopy -->
            Esta es una respuesta automática, por ende, no revisaremos el mensaje si respondes a esta dirección.
        @endcomponent
    @endslot

    {{-- Footer --}}
    @slot ('footer')
        @component('mail::footer')
            <!-- footer -->
            Fast Track Talent, C.A.
        @endcomponent
    @endslot
@endcomponent