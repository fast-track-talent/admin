<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ApplicationController;
use App\Http\Controllers\Admin\ConfigController;
use App\Http\Controllers\Admin\ClientController;
use App\Http\Controllers\Admin\PositionRequestController;
use App\Http\Controllers\Admin\TestimonyController;


/*
|--------------------------------------------------------------------------
| Web Routes Admin
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('auth')->group(function () {


    Route::get('/', [DashboardController::Class,'index'])->name('dashboard');
    // Talents module
    Route::prefix('application')->group(function () {
      Route::get('/',[ApplicationController::Class,'index'])->name('application');
      Route::post('/get-applications',[ApplicationController::Class,'getApplications' ])->name('application.getApplications');
      Route::get('/detail/{id}', [ApplicationController::Class,'showDetail'])->name('application.detail');
      // Route::get('/get-country-states/{id}',[ApplicationController::Class,'getStates'])->name('application.getCountryStates');
      Route::put('/update-reputation/{id}', [ApplicationController::Class, 'updateReputation'])->name('application.updateReputation');
      Route::get('/query-clients/{value}',[ApplicationController::Class,'searchClients'])->name('application.queryClients');
      Route::get('/query-position-requests/{id}',[ApplicationController::Class,'searchPositionRequests'])->name('application.queryPosReqs');
      Route::post('/nominate',[ApplicationController::Class,'nominateTalent'])->name('application.nominateTalent');
      Route::get('/states/{code}', [ApplicationController::class, 'getStates']);
      // Comments
      Route::post('/save-comment',[ApplicationController::Class,'saveComment'])->name('application.saveComment');
      Route::delete('/delete-comment/{id}',[ApplicationController::Class,'deleteComment'])->name('application.deleteComment');

      // Old routes
      // guardar / editar talentos
      Route::post('/store', [ApplicationController::class, 'storeTalent'])->name('application.save');
      // render de agregar talento
      Route::get('/new', [ApplicationController::class, 'addTalent'])->name('application.new');
      // render de editar talento
      Route::get('/edit/{id}', [ApplicationController::class, 'addTalent'])->name('application.edit');

      //Route::get('create',[ApplicationController::Class,'form'])->name('application.store');
      Route::post('get',[ApplicationController::Class,'get'])->name('application.get');
      Route::delete('/form/delete/{id}',[ApplicationController::Class,'deleteApplication'])->name('form.delete');
      Route::get('cv/download/{id}', [ApplicationController::Class,'getDownload'])->name('application.cv.download');
      Route::get('getAll',[ApplicationController::Class,'getAll' ])->name('application.getAll');
      Route::post('saveNominar', [ApplicationController::Class, 'saveNominar'])->name('application.saveNominar');
      Route::post('/avatar/upload', [ApplicationController::class, 'upload'])->name('application.avatar.upload');
      Route::post('/extras', [ApplicationController::class, 'saveExtras'])->name('application.extras');
      Route::post('/email', [ApplicationController::class, 'validEmail'])->name('application.validEmail');
      Route::get('/getPosition',[ApplicationController::Class,'positionData'])->name('application.positionData');
      Route::get('/getCountry',[ApplicationController::Class,'countryData'])->name('application.countryData');
      Route::get('/getLanguages',[ApplicationController::Class,'languageData'])->name('application.languageData');
      
    });

    // RUTAS PARA MÓDULO CLIENTES - FABIOLA
    Route::prefix('client')->group(function () {
      Route::get('/',[ClientController::Class,'index' ])->name('client');
      Route::get('/detail/{id}', [ClientController::Class,'showDetail'])->name('client.detail');

      Route::get('/new', [ClientController::class, 'addClient'])->name('client.new');
      Route::get('/edit/{id}', [ClientController::class, 'addClient'])->name('client.edit');
      Route::post('/store', [ClientController::class, 'storeClient'])->name('client.store');
      
      Route::post('get',[ClientController::Class,'get' ])->name('client.get');
      Route::get('getAll',[ClientController::Class,'getAll' ])->name('client.getAll');
      Route::post('/avatar/upload', [ClientController::Class,'uploadAvatar'])->name('client.avatar.uploadAvatar');
      Route::delete('/delete/{id}',[ClientController::Class,'deleteClient'])->name('client.delete');
      Route::post('/save', [ClientController::class, 'save'])->name('client.save');
      // Comments
      Route::post('/save-comment',[ClientController::Class,'saveComment'])->name('client.saveComment');
      Route::delete('/delete-comment/{id}',[ClientController::Class,'deleteComment'])->name('client.deleteComment');
    });

    // Position request modules
    Route::prefix('position-requests')->group(function () {
      Route::get('/',[PositionRequestController::Class,'index' ])->name('position.requests');
      Route::get('/detail/{id}', [PositionRequestController::Class,'showDetail'])->name('position.request.detail');
      // Route::post('/filter-position-requests',[ApplicationController::Class,'filterPositionRequests' ])->name('application.filterPositionRequests');
      Route::post('/query-talents',[PositionRequestController::Class,'searchTalents'])->name('position.request.queryTalents');
      Route::post('/nominate',[PositionRequestController::Class,'nominateTalents'])->name('position.request.nominateTalents');
      // Comments
      Route::post('/save-comment',[PositionRequestController::Class,'saveComment'])->name('position.request.saveComment');
      Route::delete('/delete-comment/{id}',[PositionRequestController::Class,'deleteComment'])->name('position.request.deleteComment');
      // Route::post('get',[ClientController::Class,'get' ])->name('client.get');
      // Route::get('getAll',[ClientController::Class,'getAll' ])->name('client.getAll');
      // Route::post('/avatar/upload', [ClientController::Class,'uploadAvatar'])->name('client.avatar.uploadAvatar');
      // Route::delete('/delete/{id}',[ClientController::Class,'deleteClient'])->name('client.delete');
      // Route::post('/save', [ClientController::class, 'save'])->name('client.save');
    });

    Route::prefix('testimony')->group(function () {

      Route::get('/',[TestimonyController::Class,'index'])->name('testimony');
      Route::post('storeTestimony',[TestimonyController::Class,'storeTestimony'])->name('testimony.storeTestimony');
      Route::post('get',[TestimonyController::Class,'get' ])->name('testimony.get');
      Route::delete('/delete/{id}',[TestimonyController::Class,'delete'])->name('testimony.delete');
    });

    Route::prefix('config')->group(function () {

      //Countries
      Route::get('/countries',[ConfigController::Class,'countries'])->name('config.countries');
      Route::post('save-country',[ConfigController::Class,'saveCountry'])->name('config.saveCountry');
      Route::delete('/countries/delete/{id}',[ConfigController::Class,'deleteCountry'])->name('config.deleteCountries');
      Route::post('/countries/restore',[ConfigController::Class,'restoreCountry'])->name('config.restoreCountries');

      // States
      Route::get('/states',[ConfigController::Class,'states'])->name('config.states');
      Route::get('/get-states/{id}',[ConfigController::Class,'getStates'])->name('config.getStates');
      Route::post('/save-state', [ConfigController::Class,'saveState'])->name('config.saveState');
      Route::delete('/state/delete/{id}',[ConfigController::Class,'deleteState'])->name('config.deleteState');
      Route::post('/state/restore',[ConfigController::Class,'restoreState'])->name('config.restoreState');

      //Study Types
      Route::get('/study',[ConfigController::Class,'studyTypes'])->name('config.study');
      Route::post('save-study',[ConfigController::Class,'saveStudyType'])->name('config.saveStudyType');
      Route::delete('/study/delete/{id}',[ConfigController::Class,'deleteStudy' ])->name('config.deleteStudy');
      Route::post('/study/restore',[ConfigController::Class,'restoreStudy'])->name('config.restoreStudy');

      //Degrees
      Route::get('/degree',[ConfigController::Class,'degree'])->name('config.degree');
      Route::get('get-degrees/{id}', [ConfigController::Class,'getDegrees'])->name('config.getDegrees');
      Route::post('save-degree',[ConfigController::Class,'saveDegree'])->name('config.saveDegree');
      Route::delete('/degree/delete/{id}',[ConfigController::Class,'deleteDegree'])->name('config.deleteDegree');
      Route::post('/degree/restore',[ConfigController::Class,'restoreDegree'])->name('config.restoreDegree');
      
      
      //Position (Charges)
      Route::get('/positions',[ConfigController::Class,'position'])->name('config.position');
      Route::post('save-position',[ConfigController::Class,'savePosition'])->name('config.savePosition');
      Route::delete('/position/delete/{id}',[ConfigController::Class,'deletePosition'])->name('config.deletePosition');
      Route::post('/position/restore',[ConfigController::Class,'restorePosition'])->name('config.restorePosition');
      
      // Company Activities
      Route::get('/company-activities',[ConfigController::Class,'companyActivity'])->name('config.activityCompany');
      Route::post('save-company-activity',[ConfigController::Class,'saveCompanyActivity'])->name('config.saveCompanyActivity');
      Route::delete('/company-activity/delete/{id}',[ConfigController::Class,'deleteCompanyActivity'])->name('config.deleteCompanyActivity');      
      Route::post('/company-activity/restore',[ConfigController::Class,'restoreCompanyActivity'])->name('config.restoreCompanyActivity');
      
      // Skills
      Route::get('/skills',[ConfigController::Class,'skills'])->name('config.skills');
      Route::post('/skills/save',[ConfigController::Class,'saveSkill'])->name('config.saveSkill');
      Route::delete('/skills/delete/{id}',[ConfigController::Class,'deleteSkill'])->name('config.deleteSkill');
      Route::post('/skills/restore',[ConfigController::Class,'restoreSkill'])->name('config.restoreSkill');

      // Languages
      Route::get('/languages',[ConfigController::Class,'languages'])->name('config.languages');
      Route::post('/languages/save',[ConfigController::Class,'saveLanguage'])->name('config.saveLanguage');
      Route::delete('/languages/delete/{id}',[ConfigController::Class,'deleteLanguage'])->name('config.deleteLanguage');
      Route::post('/languages/restore',[ConfigController::Class,'restoreLanguage'])->name('config.restoreLanguage');
    });

  });
