<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Interview\ContactController as InterviewContact;
use App\Http\Controllers\Interview\PersonalDataController;
use App\Http\Controllers\Interview\SalaryExpectationController;
use App\Http\Controllers\Interview\StudiesController;
use App\Http\Controllers\Interview\WorkExperienceController;
use App\Http\Controllers\Interview\ExtraDataController;
use App\Http\Controllers\Interview\SkillsController;
use App\Http\Controllers\Admin\ApplicationController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\Profiles\IdentificationController;
use App\Http\Controllers\Profiles\ContactController as ProfileContact;
use App\Http\Controllers\Profiles\IndustrySectorController;
use App\Http\Controllers\Profiles\ProductsController;
use App\Http\Controllers\Profiles\ProfileRequestsController;
use App\Http\Controllers\Profiles\ProfileLocationsController;
use App\Http\Controllers\Profiles\ProfileStudiesController;
use App\Http\Controllers\Profiles\ProfileSkillsController;
use App\Http\Controllers\Profiles\ProfileYearsOfExperienceController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('website')->group(function() {
    
    // Get testimonials.
    // Send contact email.
    Route::post('send-contact-email', [MailController::class, 'sendContactEmail']);
});

// These are the routes for the frontend assistants
Route::prefix('assistants')->group(function () {

    // These are the routes for the talents frontend assistant
    Route::prefix('interview')->group(function () {
        // Contact logic
        Route::get('/contact/get-data', [InterviewContact::class, 'getStepData']);
        Route::post('/contact/save-user', [InterviewContact::class, 'registerUser']);
        Route::put('/contact/save-user/{id}', [InterviewContact::class, 'updateUser']);
        // Personal data logic
        Route::get('/personal-data/get-data', [PersonalDataController::class, 'getStepData']);
        Route::get('/personal-data/states/{code}', [PersonalDataController::class, 'getStates']);
        Route::get('/personal-data/get-state/{code}', [PersonalDataController::class, 'getState']);
        Route::post('/personal-data/save', [PersonalDataController::class, 'registerPersonData']);
        Route::put('/personal-data/update/{id}', [PersonalDataController::class, 'updatePersonData']);
        // Salary expectation Logic
        Route::get('/salary-expectation/get-data', [SalaryExpectationController::class, 'getSalaryExpectations']);
        Route::put('/salary-expectation/save/{id}', [SalaryExpectationController::class, 'saveApplicationSalary']);
        // Studies logic
        Route::get('/studies/get-data', [StudiesController::class, 'getStepData']);
        Route::post('/studies/save', [StudiesController::class, 'saveStudies']);
        // Work experience logic
        Route::get('/work-experience/get-data', [WorkExperienceController::class, 'getStepData']);
        Route::post('/work-experience/save', [WorkExperienceController::class, 'syncExperiences']);
        // Skills Logic
        Route::post('/skills/get-skills', [SkillsController::class, 'getSkills']);
        Route::post('/skills/save-skills', [SkillsController::class, 'saveSkills']);

        // Extras logic
        Route::post('/extra-data/save/{id}', [ExtraDataController::class, 'syncExtras']);
        // Finish Assistant Logic
        Route::get('complete-assistant/{id}', [MailController::class, 'sendCompletedAssesmentEmail']);
    });

    // These are the routes for the clients/companies frontend assistant
    Route::prefix('profiles')->group(function () {
        // Identification logic
        Route::post('/identification/save', [IdentificationController::class, 'saveClient']);
        Route::put('/identification/update/{id}', [IdentificationController::class, 'updateClient']);
        // Contact logic
        Route::get('/contact/get-data', [ProfileContact::class, 'getStepData']);
        Route::post('/contact/save-contact', [ProfileContact::class, 'saveClientContact']);
        // Step Three Logic
        Route::get('/industry-sector/get-data', [IndustrySectorController::class, 'getStepData']);
        Route::post('/industry-sector/save-industries', [IndustrySectorController::class, 'saveClientIndustries']);
        // Step Four Logic
        Route::post('/products/save-products', [ProductsController::class, 'saveClientProducts']);
        // Step Five Logic
        Route::get('/profile-requests/get-data', [ProfileRequestsController::class, 'getStepData']);
        Route::post('/profile-requests/save-position-requests', [ProfileRequestsController::class, 'savePositionRequests']);
        // Step Six Logic
        Route::get('/profile-locations/get-data', [ProfileLocationsController::class, 'getStepData']);
        Route::post('/profile-locations/save-position', [ProfileLocationsController::class, 'savePositionRequestsLocations']);
        Route::get('/profile-locations/states/{code}', [ProfileLocationsController::class, 'getStates']);
        // Step Seven Logic
        Route::get('/profile-studies/get-data', [ProfileStudiesController::class, 'getStepData']);
        Route::post('/profile-studies/save-position', [ProfileStudiesController::class, 'savePositionRequestsStudies']);
        // Step Eight Logic
        Route::get('/profile-skills/get-data', [ProfileSkillsController::class, 'getStepData']);
        Route::post('/profile-skills/get-skills', [ProfileSkillsController::class, 'getSkills']);
        Route::post('/profile-skills/save-position', [ProfileSkillsController::class, 'savePositionRequestsSkills']);
        // Step Nine Logic
        Route::get('/experience-years/get-data', [ProfileYearsOfExperienceController::class, 'getStepData']);
        Route::post('/experience-years/save-position', [ProfileYearsOfExperienceController::class, 'savePositionRequestsAgeExperiences']);
        Route::get('/experience-years/get-stats-data', [ProfileYearsOfExperienceController::class, 'getDataForStats']);
        // Finish Assistant Logic
        Route::post('complete-assistant', [MailController::class, 'sendCompletedCompanyAssesmentEmail']);
        
    });
});
